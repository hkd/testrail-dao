<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php $GI->load->view('report_plugins/charts/defaults') ?>

<div class="chartContainer">
	<div id="chart0" style="height: 250px;"></div>
</div>

<script type="text/javascript">
var chart_bar;

function beforePrint()
{
	chart_bar.setSize(900, 250, false);
	$('#chart0').css('width', '900px');
}

function afterPrint()
{
	$('#chart0').css('width', '');
	var options = chart_bar.options;
	chart_bar.destroy();
	chart_bar = new Highcharts.Chart(options);
}

$(function () {
	$(document).ready(function() {
		chart_bar = new Highcharts.Chart({
			chart: {
				renderTo: 'chart0',
				<?php if ($show_comparison): ?>
				type: 'column'
				<?php else: ?>
				type: 'bar'
				<?php endif ?>
            },
            title: {
            	<?php if ($show_comparison && $show_summary): ?>
               	text: '<?php echo  lang('reports_rds_charts_bar_title_full') ?>'
               	<?php elseif ($show_comparison): ?>
                	text: '<?php echo  lang('reports_rds_charts_bar_title_comparison') ?>'
            	<?php else: ?>
                text: '<?php echo  lang('reports_rds_charts_bar_title_summary') ?>'
                <?php endif ?>
            },
            xAxis: {
                categories: [
                	<?php $is_first = true ?>
                	<?php if ($show_comparison): ?>
	                <?php foreach ($runs_reversed as $run): ?>
	                	<?php if (!$is_first): ?>
	                	,
	                	<?php endif ?>
	                	<?php $category = h($run->name) ?>
	                	<?php if ($run->config): ?>
	                		<?php $category .= '<br /><span style=\"color: #337933\">(' .
	                			h($run->config) . ')</span>' ?>
	                	<?php endif ?>
	                	<?php echo  js::encode_string($category)?>
	                	<?php $is_first = false ?>
		          	<?php endforeach ?>
		          	<?php endif ?>
		          	<?php if ($show_summary): ?>
		          		<?php if (!$is_first): ?>
		          		,
		          		<?php endif ?>
		          		<?php echo  js::encode_string(
			          			lang(
				          			'reports_rds_defects_summary'
				          		)
				          	)?>
		          	<?php endif ?>
                ],
                tickmarkPlacement: 'on',
                title: {
                    enabled: false
                }
            },
            legend: {
            	enabled: true
            },
            <?php if (!$show_comparison): ?>
            xAxis: {
				categories: ['', ''],
				title: {
					text: null
				}
			},
            <?php endif ?>
            yAxis: {
                title: {
                    text: ''
                },
                allowDecimals: false
            },
            plotOptions: {
            	<?php if ($show_comparison): ?>
                series: {                	
                	stacking: 'normal',
                },
                <?php endif ?>
                column: {
					dataLabels: {
						enabled: true,
						align: 'left',
						x: 30
					}
				}
            },
            <?php if (!$show_comparison): ?>
            tooltip: {
				formatter: function() {
					return this.series.name +': '+ this.y;
				}
			},
			<?php endif ?>
            series: [
            		<?php if ($show_comparison): ?>
						<?php $data = array() ?>
						<?php foreach ($runs_reversed as $run): ?>
							<?php $data[] = $run->defect_count ?>
						<?php endforeach ?>
						<?php if ($show_summary): ?>
							<?php $data[] = $summary->defect_count ?>
						<?php endif ?>
						{
							name: <?php echo  js::encode_string( lang('reports_rds_defects') ) ?>,
							data: <?php echo  json::encode( $data ) ?>,
							pointWidth: 30
						}
					<?php else: ?>
						{
							name: <?php echo  js::encode_string( lang('reports_rds_defects') ) ?>,
							data: [<?php echo  $summary->defect_count ?>],
							pointWidth: 20
						},
						{
							name: <?php echo  js::encode_string( lang('reports_rds_ref_references') ) ?>,
							data: [<?php echo  $references->reference_count ?>],
							color: Highcharts.getOptions().colors[2],
							pointWidth: 20
						},
					<?php endif ?>
            ]
        });
	});
});
</script>
