<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php $GI->load->view('report_plugins/charts/defaults') ?>

<?php if ($show_new && $show_updated): ?>
	<?php $bars_per_group = 2 ?>
<?php else: ?>
	<?php $bars_per_group = 1 ?>
<?php endif ?>
<?php $chart_height = max(count($case_groups) * 20 * $bars_per_group + 100, 150) ?>

<div class="chartContainer">
	<div id="chart0" style="height: <?php echo  $chart_height ?>px;"></div>
</div>

<script type="text/javascript">
var chart_bar;

function beforePrint()
{
	chart_bar.setSize(900, <?php echo  $chart_height ?>, false);
	$('#chart0').css('width', '900px');
}

function afterPrint()
{
	$('#chart0').css('width', '');
	var options = chart_bar.options;
	chart_bar.destroy();
	chart_bar = new Highcharts.Chart(options);
}

$(function () {
	$(document).ready(function() {
		chart_bar = new Highcharts.Chart({
			chart: {
				renderTo: 'chart0',
				type: 'bar'
			},
			title: {
				<?php if ($changes_from && $changes_to): ?>
				text: "<?php echo  langf(
					'reports_cas_charts_bar_title_from_to',
					h($case_groupby_name),
					date::format_short_date($changes_from),
					date::format_short_date($changes_to)
				) ?>"
				<?php elseif ($changes_from): ?>
				text: "<?php echo  langf(
					'reports_cas_charts_bar_title_from',
					h($case_groupby_name),
					date::format_short_date($changes_from)
				) ?>"
				<?php elseif ($changes_to): ?>
				text: "<?php echo  langf(
					'reports_cas_charts_bar_title_to',
					h($case_groupby_name),
					date::format_short_date($changes_to)
				) ?>"
				<?php else: ?>
				text: "<?php echo  langf('reports_cas_charts_bar_title',
					h($case_groupby_name)) ?>"
				<?php endif ?>
			},
			xAxis: {
				categories: [
				<?php $is_first = true ?>
				<?php foreach ($case_groups as $group_id => $group_name): ?>
					<?php if (!$is_first): ?>
					,
					<?php endif ?>
					<?php $category = h($group_name) ?>
					<?php echo  js::encode_string($category)?>
					<?php $is_first = false ?>
					<?php endforeach ?>
				],
				tickmarkPlacement: 'on',
				title: {
					enabled: false
				}
			},
			tooltip: {
				enabled: true
			},
			legend: {
				enabled: true
			},
			yAxis: {
				title: {
					text: ''
				},
				allowDecimals: false,
				labels: {
					overflow: 'justify'
				}
			},
			plotOptions: {
				bar: {
					groupPadding: 0.15,
					pointPadding: 0.1,
					dataLabels: {
						enabled: true
					}
				}
			},
			series: [
				<?php if ($show_updated): ?>
				<?php $data = array() ?>
				<?php foreach ($case_groups as $group_id => $group_name): ?>
					<?php $data[] = arr::get($cases_updated->case_counts, $group_id, 0) ?>
				<?php endforeach ?>
				{
					name: <?php echo  js::encode_string(lang('reports_cas_charts_legend_updated')) ?>,
					data: <?php echo  json::encode( $data ) ?>,
					color: '#8bbc21',
					legendIndex: 2
				}
				<?php endif ?>
				<?php if ($show_new): ?>
				<?php if ($show_updated): ?>
				,
				<?php endif ?>
				<?php $data = array() ?>
				<?php foreach ($case_groups as $group_id => $group_name): ?>
					<?php $data[] = arr::get($cases_created->case_counts, $group_id, 0) ?>
				<?php endforeach ?>
				{
					name: <?php echo  js::encode_string(lang('reports_cas_charts_legend_created')) ?>,
					data: <?php echo  json::encode( $data ) ?>,
					color: '#2f7ed8',
					legendIndex: 1
				}
				<?php endif ?>
			]
		});
	});
});

</script>
