<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php $GI->load->view('report_plugins/charts/defaults') ?>

<?php $users_to_include = array() ?>
<?php foreach ($users as $user): ?>
	<?php $estimate = arr::get($user_estimates, $user->id) ?>
	<?php if ($estimate && $estimate->test_count): ?>
		<?php $users_to_include[] = $user ?>
	<?php endif ?>
<?php endforeach ?>

<?php $chart_height = max(count($users_to_include) * 50 + 100, 150) ?>

<div class="chartContainer">
	<div id="chart0" style="height: <?php echo  $chart_height ?>px;"></div>
</div>

<script type="text/javascript">
var chart_bar;

function beforePrint()
{
	chart_bar.setSize(900, <?php echo  $chart_height ?>, false);
	$('#chart0').css('width', '900px');
}

function afterPrint()
{
	$('#chart0').css('width', '');
	var options = chart_bar.options;
	chart_bar.destroy();
	chart_bar = new Highcharts.Chart(options);
}

$(function () {
	$(document).ready(function() {
		chart_bar = new Highcharts.Chart({
			chart: {
				renderTo: 'chart0',
				type: 'bar'
			},
			title: {
				text: '<?php echo  lang('reports_uws_charts_bar_title') ?>'
			},
			xAxis: {
				categories: [
				<?php $is_first = true ?>
				<?php foreach ($users_to_include as $user): ?>
					<?php if (!$is_first): ?>
					,
					<?php endif ?>
					<?php $category = h(names::shorten($user->name)) ?>
					<?php echo  js::encode_string($category)?>
					<?php $is_first = false ?>
					<?php endforeach ?>
				],
				tickmarkPlacement: 'on',
				title: {
					enabled: false
				}
			},
			tooltip: {
				enabled: true,
				formatter: function() {
					var s = this.series.name + ': ' + this.y;

					if (this.series.index != 0)
					{
						s += ' ' + <?php echo  js::encode_string(
							lang('reports_uws_charts_bar_hours')) ?>;
					}

					return s;
				}
			},
			legend: {
				reversed: true
			},
			yAxis: {
				title: {
					text: ''
				},
				allowDecimals: false
			},
			plotOptions: {
				bar: {
					pointPadding: 0.01,
					groupPadding: 0.1,
					dataLabels: {
						enabled: false
					}
				}
			},
			series: [
				<?php $data_tests = array() ?>
				<?php $data_estimate = array() ?>
				<?php $data_forecast = array() ?>
				<?php foreach ($users_to_include as $user): ?>
				<?php $estimate = arr::get($user_estimates, $user->id) ?>
				<?php if ($estimate): ?>
					<?php $data_tests[] = $estimate->test_count ?>
					<?php $data_estimate[] = round($estimate->total_estimate / 3600) ?>
					<?php $data_forecast[] = round($estimate->total_forecast / 3600) ?>
				<?php else: ?>
					<?php $data_tests[] = 0 ?>
					<?php $data_estimate[] = 0 ?>
					<?php $data_forecast[] = 0 ?>
				<?php endif ?>
				<?php endforeach ?>
				{
					name: <?php echo  js::encode_string(lang('reports_uws_users_tests')) ?>,
					data: <?php echo  json::encode( $data_tests ) ?>
				},
				{
					name: <?php echo  js::encode_string(lang('reports_uws_users_estimate')) ?>,
					data: <?php echo  json::encode( $data_estimate ) ?>
				},
				{
					name: <?php echo  js::encode_string(lang('reports_uws_users_forecast')) ?>,
					data: <?php echo  json::encode( $data_forecast ) ?>
				}
			]
		});
	});
});

</script>
