<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php if ($references && $references->reference_count > 0): ?>
<div class="chartContainer">
	<div id="chart0" style="float: left; width: 250px; height: 250px"></div>
	<div id="chart1" style="margin: 0 30px 0 280px; height: 250px;"></div>
</div>

<?php $GI->load->view('report_plugins/charts/defaults') ?>

<script type="text/javascript">
var chart_pie, chart_bar;

function beforePrint()
{
	chart_bar.setSize(600, 250, false);
	$('#chart1').css('width', '600px');
}

function afterPrint()
{
	$('#chart1').css('width', '');
	var options = chart_bar.options;
	chart_bar.destroy();
	chart_bar = new Highcharts.Chart(options);
}

$(function () {
	$(document).ready(function() {
		chart_pie = new Highcharts.Chart({
			chart: {
				renderTo: 'chart0'
			},
			title: {
				text: '<?php echo  lang('reports_rcc_charts_pie_title') ?>'
			},
			tooltip: {
				pointFormat: '<b>{point.percentage}%</b> ({point.y})',
				percentageDecimals: 0
			},
			plotOptions: {
				pie: {
					size: '75%'
				}
			},
			series: [{
				type: 'pie',
				data: [
					[
						'<?php echo  lang('reports_rcc_charts_pie_covered') ?>',
						<?php echo  $references->reference_count_covered ?>
					],
					[
						'<?php echo  lang('reports_rcc_charts_pie_notcovered') ?>',
						<?php echo  $references->reference_count_notcovered ?>
					]
				]
			}]
		});

		chart_bar = new Highcharts.Chart({
			chart: {
				renderTo: 'chart1',
				type: 'bar'
			},
			title: {
				text: '<?php echo  lang('reports_rcc_charts_bar_title') ?>'
			},
			xAxis: {
				categories: [''],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: null,
				labels: {
					overflow: 'justify'
				},
				allowDecimals: false
			},
			tooltip: {
				formatter: function() {
					return this.series.name +': '+ this.y;
				}
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					}
				}
			},
			series: [
			{
				name: '<?php echo  lang('reports_rcc_charts_bar_references') ?>',
				data: [<?php echo  $references->reference_count ?>],
				color: Highcharts.getOptions().colors[2],
				pointWidth: 20
			},
			{
				name: '<?php echo  lang('reports_rcc_charts_bar_cases_refs') ?>',
				data: [<?php echo  $references->case_count ?>],
				color: Highcharts.getOptions().colors[3],
				pointWidth: 20
			}
			<?php if ($noreferences): ?>
			,{
				name: '<?php echo  lang('reports_rcc_charts_bar_cases_norefs') ?>',
				data: [<?php echo  $noreferences->case_count ?>],
				color: Highcharts.getOptions().colors[4],
				pointWidth: 20
			}
			<?php endif ?>
			]
		});
	});
});
</script>
<?php endif ?>