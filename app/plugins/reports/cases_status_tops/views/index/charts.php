<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php $GI->load->view('report_plugins/charts/defaults') ?>

<div class="chartContainer">
	<div id="chart0" style="height: 250px;"></div>
</div>

<?php $statuses_reversed = array_reverse($statuses) ?>

<script type="text/javascript">
var chart_bar;

function beforePrint()
{
	chart_bar.setSize(900, 250, false);
	$('#chart0').css('width', '900px');
}

function afterPrint()
{
	$('#chart0').css('width', '');
	var options = chart_bar.options;
	chart_bar.destroy();
	chart_bar = new Highcharts.Chart(options);
}

$(function () {
	$(document).ready(function() {
		chart_bar = new Highcharts.Chart({
			chart: {
				renderTo: 'chart0',
				type: 'bar'
			},
			title: {
				text: '<?php echo  lang('reports_cst_charts_bar_title') ?>'
			},
			xAxis: {
				categories: [''],
				title: {
					text: null
				}
			},
			yAxis: {
				min: 0,
				title: null,
				labels: {
					overflow: 'justify'
				},
				allowDecimals: false
			},
			tooltip: {
				formatter: function() {
					return this.series.name +': '+ this.y;
				}
			},
			legend: {
				reversed: true	
			},
			plotOptions: {
				bar: {
					dataLabels: {
						enabled: true
					},
					<?php if (count($statuses) > 5): ?>
					groupPadding: 0.05
					<?php endif ?>
				}
			},
			<?php $point_width = null ?>
			<?php if (count($statuses) < 4): ?>
				<?php $point_width = 25 ?>
			<?php endif ?>
			series: [
				<?php foreach ($statuses_reversed as $status): ?>
			{
				name: <?php echo  js::encode_string(h( $status->label ))?>,
				data: [<?php echo  arr::get($status_totals, $status->id, 0) ?>],
				color: '#<?php echo  color::format($status->color_dark) ?>',
				<?php if ($point_width): ?>
				pointWidth: <?php echo  $point_width ?>
				<?php endif ?>
			},
				<?php endforeach ?>
			]
		});
	});
});
</script>
