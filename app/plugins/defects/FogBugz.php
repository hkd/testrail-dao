<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * FogBugz Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Fogcreek FogBugz. Please
 * see http://docs.gurock.com/testrail-integration/defects-plugins
 * for more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

define('GI_DEFECTS_FOGBUGZ_API_VERSION', 6);

class FogBugz_defect_plugin extends Defect_plugin
{
	private $_api;
	
	private $_address;
	private $_user;
	private $_password;
	
	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'FogBugz defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your FogBugz connection below
[connection]
address=https://<your-server>.fogbugz.com/
user=testrail
password=secret'
	);

	public function get_meta()
	{
		return self::$_meta;
	}
		
	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************

	public function __construct()
	{
	}
	
	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
				// Intentionally left out because FogBugz' API is
				// quite slow and this saves one request.
				// $api->logout(); 
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}
	
	// *********************************************************
	// CONFIGURATION
	// *********************************************************

	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}
		
		$keys = array('address', 'user', 'password');
		
		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}
		
		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}
	
	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************

	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}
		
		$this->_api = new FogBugz_api($this->_address);
		$this->_api->login($this->_user, $this->_password);		
		return $this->_api;
	}

	// *********************************************************
	// PUSH
	// *********************************************************
		
	public function prepare_push($context)
	{
		// Return a form with the following fields/properties
		return array(
			'fields' => array(
				'title' => array(
					'type' => 'string',
					'label' => 'Title',
					'required' => true,
					'size' => 'full'
				),
				'category' => array(
					'type' => 'dropdown',
					'label' => 'Category',
					'required' => true,
					'remember' => true,
					'size' => 'compact'
				),
				'project' => array(
					'type' => 'dropdown',
					'label' => 'Project',
					'required' => true,					
					'remember' => true,
					'cascading' => true,
					'size' => 'compact'
				),
				'area' => array(
					'type' => 'dropdown',
					'label' => 'Area',
					'required' => true,
					'remember' => true,
					'depends_on' => 'project',
					'size' => 'compact'
				),
				'assignee' => array(
					'type' => 'dropdown',
					'label' => 'Assigned To',
					'required' => false,
					'remember' => true,
					'size' => 'compact'
				),
				'comment' => array(
					'type' => 'text',
					'label' => 'Comment',
					'rows' => 10
				)
			)
		);
	}

	private function _get_title_default($context)
	{
		$test = current($context['tests']);
		$title = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$title .= ' (+others)';
		}
		
		return $title;
	}
	
	private function _get_comment_default($context)
	{
		return $context['test_change']->description;
	}
	
	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}
	
	public function prepare_field($context, $input, $field)
	{
		$data = array();
		
		// Process those fields that do not need a connection to the
		// FogBugz installation.
		if ($field == 'title' || $field == 'comment')
		{
			switch ($field)
			{
				case 'title':
					$data['default'] = $this->_get_title_default(
						$context);
					break;
					
				case 'comment':
					$data['default'] = $this->_get_comment_default(
						$context);
					break;
			}
			
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();
		
		switch ($field)
		{
			case 'category':
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_categories()
				);
				
				// Select the stored preference or the first item in
				// the list otherwise.
				$default = arr::get($prefs, 'category');
				if ($default)
				{
					$data['default'] = $default;
				}
				else
				{
					if ($data['options'])
					{
						$data['default'] = key($data['options']);
					}
				}
				break;

			case 'project':
				$data['default'] = arr::get($prefs, 'project');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_projects()
				);
				break;
				
			case 'area':
				if (isset($input['project']))
				{
					$data['default'] = arr::get($prefs, 'area');
					$data['options'] = $this->_to_id_name_lookup(
						$api->get_areas($input['project'])
					);
				}
				break;

			case 'assignee':
				$data['default'] = arr::get($prefs, 'assignee');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_users()
				);
				break;
		}
		
		return $data;
	}
	
	public function validate_push($context, $input)
	{
	}
	
	public function push($context, $input)
	{
		$api = $this->_get_api();
		
		return $api->add_case(
			array(
				'sTitle' => $input['title'],
				'ixCategory' => $input['category'],
				'ixProject' => $input['project'],
				'ixArea' => $input['area'],
				'ixPersonAssignedTo' => $input['assignee'],
				'sEvent' => $input['comment']
			)
		);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();

		$case = $api->get_case(
			$defect_id,
			array(
				'sTitle',
				'ixStatus',
				'sStatus',
				'sProject',
				'ixCategory',
				'sCategory',
				'fOpen',
				'sLatestTextSummary'
			)
		);
		
		// Decide which status to return to TestRail based on the
		// fOpen flag of the case and possibly the resolved flag of
		// the status.
		if ($case['fOpen'] == 'false')
		{
			$status_id = GI_DEFECTS_STATUS_CLOSED;
		}
		else 
		{
			// The case is still open. In order to find out whether
			// the case has been resolved or not, we need to get
			// the statuses and check the resolved flag of the case
			// status.
			$statuses = obj::get_lookup(
				$api->get_statuses($case['ixCategory'])
			);
			
			$status = arr::get($statuses, $case['ixStatus']);
		
			if (!$status)
			{
				throw new FogBugzException(
					'Invalid status with ID ' . $case['ixStatus']
				);
			}
			
			if ($status->resolved)
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}
			else 
			{
				$status_id = GI_DEFECTS_STATUS_OPEN;
			}
		}
		
		// Format the description of the case (we use a monospace
		// font).		
		if ($case['sLatestTextSummary'])
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($case['sLatestTextSummary'])
					)
				)
			);
		}
		else
		{
			$description = null;
		}
		
		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}default.asp?{1}',
				$this->_address,
				$defect_id
			),
			'title' => $case['sTitle'],
			'status_id' => $status_id,
			'status' => $case['sStatus'],
			'description' => $description,
			'attributes' => array(
				'Category' => h($case['sCategory']),
				'Status' => h($case['sStatus']),
				'Project' => h($case['sProject'])
			)
		);
	}
}

/**
 * FogBugz API
 *
 * Wrapper class for the FogBugz API with login/logout and functions
 * for retrieving projects etc. from a FogBugz installation.
 */
class FogBugz_api
{
	private $_version;
	private $_token;
	private $_address;
	private $_curl;
	
	/**
	 * Construct
	 *
	 * Initializes a new FogBugz API object. Checks the min version
	 * the FogBugz installation supports and raises an exception if
	 * its greater than the version we support. Expects the web
	 * address of the FogBugz installation including http or https
	 * prefix.
	 */
	public function __construct($address)
	{
		$this->_address = str::slash($address);
		$this->_version = $this->_check_version();
	}
	
	private function _check_version()
	{
		$url = $this->_address . 'api.xml';
		$version = $this->_send_request('GET', $url);
		
		// Check for minimum version
		$min = (int)$version->minversion;
		if ($min > GI_DEFECTS_FOGBUGZ_API_VERSION)
		{
			$this->_throw_error(
				'Unsupported FogBugz API version: {0}/{1}',
				$min,
				GI_DEFECTS_FOGBUGZ_API_VERSION
			);
		}
		
		return $version;
	}
	
	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new FogBugzException($message);
	}
		
	private function _parse_response($response)
	{
		$dom = xml::parse_string($response);
		
		// FogBugz API:
		// "If the first child node is <error>, something went wrong."
		if (isset($dom->error))
		{
			$this->_throw_error((string) $dom->error);
		}
		
		return $dom;
	}
	
	private function _send_command($command, $data = null)
	{
		$url = $this->_address;
		$url .= $this->_version->url . 'cmd=' . $command;
		
		if ($this->_token)
		{
			$url .= '&token=' . $this->_token;
		}
		
		return $this->_send_request('POST', $url, $data);
	}
	
	private function _send_request($method, $url, $data = null)
	{
		$options['data'] = $data;

		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		$response = http::request_ex(
			$this->_curl,
			$method,
			$url,
			$options
		);
		
		// In case debug logging is enabled, we append the data
		// we've sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debugr('$data', $data);
			logger::debugr('$response', $response);
		}
		
		if ($response->code != 200)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0})', $response->code
			);
		}
		
		return $this->_parse_response($response->content);
	}
	
	/**
	 * Login
	 *
	 * Logs in to the FogBugz installation using the provided user/
	 * email address and password.
	 */
	public function login($user, $password)
	{
		$data['email'] = $user;
		$data['password'] = $password;
		$response = $this->_send_command('logon', $data);
		$this->_token = (string)$response->token;
	}
	
	/**
	 * Logout
	 *
	 * Logs the user out. You can use login() to log in again.
	 */
	public function logout()
	{
		$this->_send_command('logoff');
		$this->_token = null;
	}
	
	/**
	 * Get Projects
	 *
	 * Returns a list of projects for the FogBugz installation. The
	 * projects are returned as array of objects, each with its ID
	 * and name.
	 */
	public function get_projects()
	{
		$response = $this->_send_command('listProjects');
		
		if (!isset($response->projects))
		{
			return array();
		}
		
		$projects = $response->projects; // <projects>
		
		$result = array();
		foreach ($projects->project as $project)
		{
			if (isset($project->fInbox))
			{
				if ($project->fInbox == 'true')
				{
					continue; // Skip the 'Inbox' project
				}
			}
			
			$p = obj::create();
			$p->id = (int) $project->ixProject;
			$p->name = (string) $project->sProject;
			$result[] = $p;
		}
		
		return $result;
	}

	/**
	 * Get Areas
	 *
	 * Returns a list of areas for the given project for the FogBugz
	 * installation. Areas are returned as array of objects, each	 
	 * with its ID, name and project ID.
	 */
	public function get_areas($project_id)
	{
		$data['ixProject'] = $project_id;
		$response = $this->_send_command('listAreas', $data);

		if (!isset($response->areas))
		{
			return array();
		}
		
		$areas = $response->areas; // <areas>
		
		$result = array();
		foreach ($areas->area as $area)
		{
			$a = obj::create();
			$a->id = (int) $area->ixArea;
			$a->name = (string) $area->sArea;
			$a->project_id = (int) $area->ixProject;
			$result[] = $a;
		}
		
		return $result;
	}

	/**
	 * Get Users
	 *
	 * Returns a list of users for the FogBugz installation. Users are
	 * returned as array of objects, each with its ID and name.
	 */
	public function get_users()
	{
		$response = $this->_send_command('listPeople');

		if (!isset($response->people))
		{
			return array();
		}
		
		$users = $response->people; // <people>
		
		$result = array();
		foreach ($users->person as $user)
		{
			$u = obj::create();
			$u->id = (int) $user->ixPerson;
			$u->name = (string) $user->sFullName;
			$result[] = $u;
		}
		
		return $result;
	}
	
	/**
	 * Get Categories
	 *
	 * Returns a list of categories for the FogBugz installation.
	 * Categories are returned as array of objects, each with its ID,
	 * and name.
	 */
	public function get_categories()
	{
		$response = $this->_send_command('listCategories');

		if (!isset($response->categories))
		{
			return array();
		}
		
		$categories = $response->categories; // <categories>
		
		$result = array();
		foreach ($categories->category as $category)
		{
			if (isset($category->fDeleted))
			{
				if ($category->fDeleted == 'true')
				{
					continue; // Skip this deleted category
				}
			}
			
			$c = obj::create();
			$c->id = (int) $category->ixCategory;
			$c->name = (string) $category->sCategory;
			$result[] = $c;
		}

		return $result;
	}
	
	/**
	 * Get Statuses
	 *
	 * Returns a list of statuses for the given category for the
	 * FogBugz installation. Statuses are returned as array of
	 * objects, each with its ID, and name.
	 */
	public function get_statuses($category_id)
	{
		$data['ixCategory'] = $category_id;
		$response = $this->_send_command('listStatuses', $data);

		if (!isset($response->statuses))
		{
			return array();
		}
		
		$statuses = $response->statuses; // <statuses>
		
		$result = array();
		foreach ($statuses->status as $status)
		{
			if (isset($status->fDeleted))
			{
				if ($status->fDeleted == 'true')
				{
					continue; // Skip this deleted status
				}
			}
			
			$s = obj::create();
			$s->id = (int) $status->ixStatus;
			$s->name = (string) $status->sStatus;
			$s->resolved = $status->fResolved == 'true';
			$result[] = $s;
		}

		return $result;
	}
	
	/**
	 * Get Case
	 *
	 * Gets an existing case from the FogBugz installation with the
	 * given ID and requested columns (sTitle, ixStatus etc.). The
	 * columns must be named according to the FogBugz API format.
	 */	 
	public function get_case($case_id, $columns)
	{
		$data['q'] = $case_id;
		$data['cols'] = str::join($columns, ',');
		$response = $this->_send_command('search', $data);

		if (!$response->cases)
		{
			return array();
		}
		
		$cases = $response->cases; // <cases>
		
		if (!isset($cases->case))
		{
			$this->_throw_error('The requested case does not exist');
		}
		
		$case = $cases->case;		
		
		$result = array();		
		foreach ($columns as $column)
		{
			if (!isset($case->$column))
			{
				$this->_throw_error('Missing column "{0}"', $column);
			}
			
			$result[$column] = (string) $case->$column;
		}
		
		return $result;
	}
	
	/**
	 * Add Case
	 *
	 * Adds a new case to the FogBugz installation with the given
	 * parameters (title, project etc.) and returns its ID. The
	 * parameters must be named according to the FogBugz API format,
	 * e.g.:
	 *
	 * sTitle:      The title of the new case
	 * ixCategory:  The ID of the category of the new case (bug,
	 *              feature request etc.)
	 * ixProject:   The ID of the project the case should be added
	 *              to
	 * ixArea:      The ID of the area the case should be added
	 *              to
	 * sEvent:      The description of the new case
	 */
	public function add_case($options)
	{
		$response = $this->_send_command('new', $options);
		
		$case = $response->case;	
		if (!isset($case['ixBug']))
		{
			$this->_throw_error('No case ID received (ixBug)');
		}
		
		return (string) $case['ixBug'];
	}
}

class FogBugzException extends Exception
{
}
