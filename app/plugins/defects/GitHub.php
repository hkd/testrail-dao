<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * GitHub Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for GitHub. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class GitHub_defect_plugin extends Defect_plugin
{
	private $_api;

	private $_address;
	private $_user;
	private $_password;

	private $_field_defaults = array(
		'summary' => array(
			'type' => 'string',
			'label' => 'Summary',
			'size' => 'full',
			'required' => true
		),
		'milestone' => array(
			'type' => 'dropdown',
			'label' => 'Milestone',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'assignee' => array(
			'type' => 'dropdown',
			'label' => 'Assignee',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'label' => array(
			'type' => 'multiselect',
			'label' => 'Labels',
			'required' => false,
			'size' => 'full'
		),
		'description' => array(
			'type' => 'text',
			'label' => 'Description',
			'required' => false,
			'rows' => 10
		)
	);

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'GitHub defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your GitHub connection below.
; Note: requires GitHub API v3 or later. You
; can alternatively specify a GitHub Enterprise
; server address.
[connection]
address=https://api.github.com/
user=testrail
password=secret

[repository]
owner=<repository-owner>
name=<repository-name>
');

	public function get_meta()
	{
		return self::$_meta;
	}
	
	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************
	
	public function __construct()
	{
	}
	
	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		$groups = array(
			'connection' => array(
				'address',
				'user',
				'password'
			),
			'repository' => array(
				'owner',
				'name'
			)
		);

		foreach ($groups as $group => $keys)
		{
			if (!isset($ini[$group]))
			{
				throw new ValidationException(
					"Missing [$group] group"
				);
			}
			
			// Check required values for existance
			foreach ($keys as $key)
			{
				if (!isset($ini[$group][$key]) ||
					!$ini[$group][$key])
				{
					throw new ValidationException(
						"Missing configuration for key '$key'"
					);
				}
			}
		}		

		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException(
				'Address is not a valid url'
			);
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
		$this->_repo = $ini['repository']['owner'] . '/' .
			$ini['repository']['name'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}

		$this->_api = new GitHub_api(
			$this->_address,
			$this->_user,
			$this->_password,
			$this->_repo
		);

		return $this->_api;
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************
		
	public function prepare_push($context)
	{
		return array('fields' => $this->_field_defaults);
	}

	private function _get_summary_default($context)
	{
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}

		return $summary;
	}

	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}
	
	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// GitHub installation.	
		if ($field == 'summary' || $field == 'description')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;
			}
		
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'label':
				$data['default'] = arr::get($prefs, 'label');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_labels($this->_repo)
				);
				break;

			case 'assignee':
				$data['default'] = arr::get($prefs, 'assignee');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_assignees($this->_repo)
				);
				break;

			case 'milestone':
				$data['default'] = arr::get($prefs, 'milestone');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_milestones($this->_repo)
				);
				break;
		}
		
		return $data;
	}
	
	public function validate_push($context, $input)
	{
	}
	
	public function push($context, $input)
	{
		$api = $this->_get_api();		
		return $api->add_issue($input);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();
		$issue = $api->get_issue($defect_id);

		$attributes = array();

		// Add some important attributes for the issue such as the
		// issue type, current status and repo. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.

		if (isset($issue->url))
		{
			// Add a link to the repo.
			$attributes['Repository'] = str::format(
				'<a target="_blank" href="{0}">{1}</a>',
				a("http://github.com/$this->_repo"),
				h($this->_repo)
			);
		}

		$status = '';
		if (isset($issue->state))
		{
			$attributes['Status'] = h($issue->state);
			$status = $issue->state;
		}
		
		if (isset($issue->milestone))
		{
			$attributes['Milestone'] = h($issue->milestone->title);
		}
		else
		{
			$attributes['Milestone'] = "&ndash;";
		}

		if (isset($issue->assignee->login))
		{
			$attributes['Assignee'] = 
				h($issue->assignee->login);
		}
		else
		{
			$attributes['Assignee'] = "&ndash;";
		}

		$label_count = count($issue->labels);
		if ($label_count > 0)
		{
			$i = 1;
			$attributes['Labels'] = '';
			foreach ($issue->labels as $label)
			{
				if ($i < $label_count)
				{
					$attributes['Labels'] .= h($label->name) . ', ';
				}
				else
				{
					$attributes['Labels'] .= h($label->name);
				}
				$i++;
			}
		}
		else
		{
			$attributes['Labels'] = "&ndash;";
		}

		// Decide which status to return to TestRail based on the
		// resolution property of the issue (whether the issue was
		// resolved or not). The issue or statuses don't have any
		// additional meta information so that's unfortunately the
		// only distinction we can make for the status.
		$status_id = GI_DEFECTS_STATUS_OPEN;

		if (isset($issue->state))
		{
			if ($issue->state == 'closed')
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}
		}

		// Format the description of the issue (we use a monospace
		// font).
		if (isset($issue->body) && $issue->body)
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($issue->body)
					)
				)
			);
		}
		else
		{
			$description = null;
		}

		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}',
				$issue->html_url
			),
			'title' => $issue->title,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * GitHub REST API
 *
 * Wrapper class for the GitHub REST API with functions for
 * retrieving repos, issues etc.
 */
class GitHub_api
{
	private $_address;
	private $_user;
	private $_password;
	private $_repo;
	private $_curl;

	/**
	 * Construct
	 *
	 * Initializes a new GitHub API object. Expects the web address
	 * of the GitHub installation including http or https prefix.
	 */	
	public function __construct($address, $user, $password, $repo)
	{
		$this->_address = str::slash($address);
		$this->_user = $user;
		$this->_password = $password;
		$this->_repo = $repo;
	}
	
	/**
	 * Get Labels
	 *
	 * Returns a list of labels for the given repo. The labels are
	 * returned as array of objects, each with its ID and name.
	 */	
	public function get_labels($repo)
	{
		$labels = $this->_send_command(
			'GET',
			"repos/$repo/labels"
		);

		$result = array();
		foreach ($labels as $label)
		{
			$p = obj::create();
			$p->id = (string) $label->name;
			$p->name = (string) $label->name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get Assignees
	 * 
	 * Returns a list of Assignees. The Assignees are returned as
	 * array of objects, each with its ID and name.
	 */
	public function get_assignees($repo)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				"repos/{0}/assignees",
				$repo
			)
		);
		
		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $assignee)
		{
			$a = obj::create();
			$a->id = (string) $assignee->login;
			$a->name = (string) $assignee->login;
			$result[] = $a;
		}

		return $result;
	}

	/**
	 * Get Milestones
	 *
	 * Returns a list of milestones for the given repo for the GitHub
	 * installation. Milestones are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_milestones($repo)
	{
		$response = $this->_send_command(
			'GET',
			"repos/$repo/milestones"
		);

		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $milestone)
		{
			$m = obj::create();
			$m->id = (string) $milestone->number;
			$m->name = (string) $milestone->title;
			$result[] = $m;
		}
		
		return $result;
	}

	/**
	 * Get Issue
	 *
	 * Gets an existing case from the GitHub installation and returns
	 * it. The resulting issue object has various properties such as
	 * the summary, description, repo etc.
	 */	
	public function get_issue($issue_id)
	{
		return $this->_send_command(
			'GET',
			"repos/$this->_repo/issues/" . urlencode($issue_id)
		);
	}

	public function _send_command($method, $command, $data = null)
	{
		$page = 0;
		$limit = 100;
		$response = array();

		do
		{
			$page++;

			$r = $this->_send_command_batch(
				$method, 
				$command,
				$page,
				$limit,
				$data
			);

			if (!is_array($r))
			{
				return $r;		
			}

			$count = count($r);
			if ($count > 0)
			{
				$response = array_merge($response, $r);
			}
		} while ($count == $limit);

		return $response;
	}

	private function _send_command_batch($method, $command, $page,
		$limit, $data = null)
	{
		return $this->_send_request(
			$method,
			str::format(
				'{0}{1}?page={2}&per_page={3}',
				$this->_address,
				$command,
				$page,
				$limit
			),
			$data
		);
	}
	
	private function _send_request($method, $url, $data = null)
	{
		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Issuing GitHub HTTP request');
			logger::debugr(
				'$request',
				array(
					'method' => $method,
					'url' => $url,
					'data' => $data
				)
			);
		}

		$response = http::request_ex(
			$this->_curl,
			$method, 
			$url, 
			array(
				'user' => $this->_user,
				'password' => $this->_password,
				'data' => json::encode($data),
				'headers' => array(
					'User-Agent' => 'testrail-git',
					'Content-Type' => 'application/json'
				)
			)
		);

		// In case debug logging is enabled, we append the data we've
		// sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Got the following response');
			logger::debugr('$response', $response);
		}

		$obj = json::decode($response->content);

		if ($response->code != 200 && $response->code != 201)
		{
			switch ($response->code)
			{
				case '403':
				case '401':
					$this->_throw_error(
						'Invalid HTTP code ({0}). Please check your user/' .
						'password.',
						$response->code
					);
					break;

				case '404':
					$this->_throw_error(
						'Invalid HTTP code ({0}). Please check your ' .
						'[repository] configuration and that the issue ' .
						'exists in GitHub.',
						$response->code
					);
					break;

				default:
					$this->_throw_error(
						'The request to GitHub failed with an invalid ' .
						'HTTP code ({0}).',
						$response->code
					);
				break;
			}
		}

		return $obj;
	}

	/**
	 * Add Issue
	 *
	 * Adds a new issue to the GitHub installation with the given
	 * parameters (title, repo etc.) and returns its identifier.
	 * The parameters must be named according to the GitHub API
	 * format,
	 * e.g.:
	 *
	 * summary:     The summary of the new issue
	 * repo:     	The name of the repository
	 * milestone:   The ID (number) of the milestone the issue
	 *				should be added to
	 * description: The description of the new issue
	 * assignee:	The login the issue should be assigned to
	 */	
	public function add_issue($options)
	{
		$fields = array();

		foreach ($options as $field_name => $field_value)
		{
			if (!$field_value)
			{
				continue;
			}

			$field = $this->_format_field(
				$field_name,
				$field_value);

			if (isset($field['name']) && isset($field['value']))
			{
				$fields[$field['name']] = $field['value'];
			}
		}

		$response = $this->_send_command(
			'POST',
			"repos/$this->_repo/issues",
			$fields
		);

		return "$response->number";
	}

	private function _format_field($field_name, $field_value)
	{
		$data = array();
		$data['name'] = $field_name;

		switch ($field_name)
		{
			case 'summary':
				$data['name'] = 'title';
				$data['value'] = $field_value;
				break;

			case 'description':
				$data['name'] = 'body';
				$data['value'] = $field_value;
				break;
			
			case 'assignee':
			case 'milestone':
				$data['value'] = $field_value;
				break;

			case 'label':
				$data['name'] = 'labels';
				$data['value'] = $field_value;
				break;
		}

		return $data;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new GitHubException($message);
	}
}

class GitHubException extends Exception
{
}
