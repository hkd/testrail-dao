<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Bitbucket Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Bitbucket. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class Bitbucket_defect_plugin extends Defect_plugin
{
	private $_api;

	private $_address;
	private $_user;
	private $_password;

	private $_field_defaults = array(
		'summary' => array(
			'type' => 'string',
			'label' => 'Summary',
			'size' => 'full',
			'required' => true
		),
		'kind' => array(
			'type' => 'dropdown',
			'label' => 'Kind',
			'required' => true,
			'remember' => true,
			'size' => 'compact'
		),
		'priority' => array(
			'type' => 'dropdown',
			'label' => 'Priority',
			'required' => true,
			'remember' => true,
			'size' => 'compact'
		),
		'status' => array(
			'type' => 'dropdown',
			'label' => 'Status',
			'remember' => true,
			'size' => 'compact'
		),
		// The assignee is disabled as it requires owner permissions.
		// It only works if the configured user configured has owner
		// privileges for the repository. This feature is planned for
		// the future:
		// https://bitbucket.org/site/master/issue/7329/users-privilege-a-given-user-should-be
		/*
		'assignee' => array(
			'type' => 'dropdown',
			'label' => 'Assignee',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		*/
		'description' => array(
			'type' => 'text',
			'label' => 'Description',
			'required' => false,
			'rows' => 10
		)
	);

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Bitbucket defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' =>
			'; Please configure your Bitbucket connection below.
[connection]
address=https://bitbucket.org/
user=testrail
password=secret

[repository]
owner=<repository-owner>
name=<repository-name>
'
);

	public function get_meta()
	{
		return self::$_meta;
	}

	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************

	public function __construct()
	{
	}

	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************

	public function validate_config($config)
	{
		$ini = ini::parse($config);

		$groups = array(
			'connection' => array(
				'address',
				'user',
				'password'
			),
			'repository' => array(
				'owner',
				'name'
			)
		);

		foreach ($groups as $group => $keys)
		{
			if (!isset($ini[$group]))
			{
				throw new ValidationException(
					"Missing [$group] group"
				);
			}

			// Check required values for existance
			foreach ($keys as $key)
			{
				if (!isset($ini[$group][$key]) ||
					!$ini[$group][$key])
				{
					throw new ValidationException(
						"Missing configuration for key '$key'"
					);
				}
			}
		}

		$address = $ini['connection']['address'];

		// Check whether the address is a valid url (syntax only).
		if (!check::url($address))
		{
			throw new ValidationException(
				'Address is not a valid url'
			);
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
		$this->_repo = $ini['repository']['owner'] . '/' .
			$ini['repository']['name'];
	}

	// *********************************************************
	// API / CONNECTION
	// *********************************************************

	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}

		$this->_api = new Bitbucket_api(
			$this->_address,
			$this->_user,
			$this->_password,
			$this->_repo
		);

		return $this->_api;
	}

	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		return array('fields' => $this->_field_defaults);
	}

	private function _get_summary_default($context)
	{
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;

		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}

		return $summary;
	}

	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// Bitbucket installation.
		if ($field == 'summary' || $field == 'description')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;

				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;
			}

			return $data;
		}

		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}

		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'status':
				$data['default'] = arr::get($prefs, 'status');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_status()
				);
				break;

			case 'kind':
				$data['default'] = arr::get($prefs, 'kind');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_kinds()
				);
				break;

			case 'priority':
				$data['default'] = arr::get($prefs, 'priority');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_priorities()
				);
				break;

			case 'assignee':
				$data['default'] = arr::get($prefs, 'assignee');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_assignees($this->_repo)
				);
				break;

			case 'milestone':
				$data['default'] = arr::get($prefs, 'milestone');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_milestones()
				);
				break;
		}

		return $data;
	}

	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();
		return $api->add_issue($input);
	}

	// *********************************************************
	// LOOKUP
	// *********************************************************

	public function lookup($issue_id)
	{
		$api = $this->_get_api();
		$issue = $api->get_issue($issue_id);

		$attributes = array();

		// Add some important attributes for the issue such as the
		// issue type, current status and repo. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.

		if (isset($issue->status))
		{
			// Add a link to the repo.
			$attributes['Repository'] = str::format(
				'<a target="_blank" href="{0}">{1}</a>',
				a("http://bitbucket.com/$this->_repo"),
				h($this->_repo)
			);
		}

		$status = '';
		if (isset($issue->status))
		{
			$attributes['Status'] = h($issue->status);
			$status = $issue->status;
		}

		if (isset($issue->priority))
		{
			$attributes['Priority'] = h($issue->priority);
		}
		else
		{
			$attributes['Priority'] = "&ndash;";
		}

		if (isset($issue->metadata->kind))
		{
			$attributes['Kind'] = h($issue->metadata->kind);
		}
		else
		{
			$attributes['Kind'] = "&ndash;";
		}

		if (isset($issue->reported_by->username))
		{
			$attributes['Reported By'] =
				h($issue->reported_by->username);
		}
		else
		{
			$attributes['Reported By'] = "&ndash;";
		}

		if (isset($issue->responsible->username))
		{
			$attributes['Assignee'] =
				h($issue->responsible->username);
		}
		else
		{
			$attributes['Assignee'] = "&ndash;";
		}

		// Decide which status to return to TestRail based on the
		// resolution property of the issue (whether the issue was
		// resolved or not). The issue or statuses don't have any
		// additional meta information so that's unfortunately the
		// only distinction we can make for the status.
		$status_id = GI_DEFECTS_STATUS_OPEN;

		if (isset($issue->state))
		{
			if ($issue->state == 'resolved')
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}
		}

		// Format the description of the issue (we use a monospace
		// font).
		if (isset($issue->content) && $issue->content)
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($issue->content)
					)
				)
			);
		}
		else
		{
			$description = null;
		}

		return array(
			'id' => $issue_id,
			'url' => str::format(
				'{0}{1}/issue/{2}',
				a($this->_address),
				a($this->_repo),
				h($issue_id)
			),
			'title' => $issue->title,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Bitbucket REST API
 *
 * Wrapper class for the Bitbucket REST API with functions for
 * retrieving repos, issues etc.
 */
class Bitbucket_api
{
	private $_address;
	private $_user;
	private $_password;
	private $_repo;
	private $_curl;

	/**
	 * Construct
	 *
	 * Initializes a new Bitbucket API object. Expects the full web
	 * address of the Bitbucket installation including http or https
	 * prefix.
	 */
	public function __construct($address, $user, $password, $repo)
	{
		$this->_address = str::slash($address);
		$this->_user = $user;
		$this->_password = $password;
		$this->_repo = $repo;
		$this->_check_issue_tracker();
	}

	private function _check_issue_tracker()
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				"repositories/{0}",
				$this->_repo
			)
		);

		if (!$response)
		{
			return null;
		}

		if (!$response->has_issues)
		{
			$this->_throw_error(
				'The repository {0} was not configured with an issue ' .
				'tracker. You cannot push issues to this repository.',
				$this->_repo
			);
		}
	}

	/**
	 * Get Kinds
	 *
	 * Returns a list of kinds (issue types) for the configured repo.
	 * The kinds are returned as array of objects, each with its ID
	 * and name. Kinds are static in Bitbucket and cannot be configured
	 * nor be fetched via the API.
	 */
	public function get_kinds()
	{
		$kinds = array(
			'bug' => 'Bug',
			'enhancement' => 'Enhancement',
			'proposal' => 'Proposal',
			'task' => 'Task',
		);

		$result = array();
		foreach ($kinds as $id => $name)
		{
			$p = obj::create();
			$p->id = $id;
			$p->name = $name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get Status
	 *
	 * Returns a list of statuses for the configured repo. The statuses
	 * are returned as array of objects, each with its ID and name.
	 * Statuses are static in Bitbucket and cannot be configured nor be
	 * fetched via the API.
	 */
	public function get_status()
	{
		$statuses = array(
			'new' => 'New',
			'open' => 'Open',
			'resolved' => 'Resolved',
			'on hold' => 'On Hold',
			'invalid' => 'Invalid',
			'duplicate' => 'Duplicate',
			'wontfix' => 'Won\'t Fix'
		);

		$result = array();
		foreach ($statuses as $id => $name)
		{
			$p = obj::create();
			$p->id = $id;
			$p->name = $name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get Priorities
	 *
	 * Returns a list of priorities for the configured repo. The
	 * priorities are returned as array of objects, each with its ID
	 * and name. Priorities are static in Bitbucket and cannot be
	 * configured nor be fetched via the API.
	 */
	public function get_priorities()
	{
		$priorities = array(
			'trivial' => 'Trivial',
			'minor' => 'Minor',
			'major' => 'Major',
			'critical' => 'Critical',
			'blocker' => 'Blocker'
		);

		$result = array();
		foreach ($priorities as $id => $name)
		{
			$p = obj::create();
			$p->id = $id;
			$p->name = $name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get Privileges
	 *
	 * Returns a list of users that have privileges to access the give
	 * repository. The users are returned as array of objects, each
	 * with its username and full name.
	 */
	private function _get_privileges($repo)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				"privileges/{0}",
				$repo
			)
		);

		if (!$response)
		{
			return array();
		}

		$result = array();
		foreach ($response as $user)
		{
			$a =  obj::create();
			$a->id = (string) $user->user->username;
			$a->name = (string) $user->user->first_name . 
				$user->user->last_name;
			$result[] = $a;
		}

		return $result;
	}

	/**
	 * Get Group Privileges
	 *
	 * Returns a list of users that have privileges to access the repo
	 * via a group. The users are returned as array of objects, each
	 * with its username and full name.
	 */
	private function _get_group_privileges($repo)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				"group-privileges/{0}",
				$repo
			)
		);

		if (!$response)
		{
			return array();
		}

		$result = array();
		foreach ($response as $group)
		{
			foreach ($group->group->members as $user)
			{
				$a =  obj::create();
				$a->id = (string) $user->username;
				$a->name = (string) $user->first_name . 
					$user->last_name;
				$result[] = $a;
			}
		}

		return $result;
	}

	/**
	 * Get Assignees
	 *
	 * Returns a list of assignees. The assignees are returned as array
	 * of objects, each with its username and full name.
	 */
	public function get_assignees($repo)
	{
		// Not yet implemented because the assignee is not supported
		// at this point.
		return array();
	}

	/**
	 * Get Issue
	 *
	 * Gets an existing issue/ticket from the Bitbucket installation
	 * and returns it. The resulting object has various properties
	 * such as the summary, description, repo etc.
	 */
	public function get_issue($issue_id)
	{
		return $this->_send_command(
			'GET',
			"repositories/$this->_repo/issues/$issue_id"
		);
	}

	private function _send_command($method, $command, $data = null)
	{
		$url = $this->_address . 'api/1.0/' . $command;
		return $this->_send_request($method, $url, $data);
	}

	private function _send_request($method, $url, $data = null)
	{
		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Issuing Bitbucket HTTP request');
			logger::debugr(
				'$request',
				array(
					'method' => $method,
					'url' => $url,
					'data' => $data
				)
			);
		}

		$response = http::request_ex(
			$this->_curl,
			$method,
			$url,
			array(
				'user' => $this->_user,
				'password' => $this->_password,
				'data' => $data,
				'headers' => array(
					'Content-Type' => 'application/x-www-form-urlencoded'
				)
			)
		);

		// In case debug logging is enabled, we append the data we've
		// sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Got the following response');
			logger::debugr('$response', $response);
		}

		$obj = json::decode($response->content);

		if ($response->code != 200 && $response->code != 201)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0}). Please check your user/' .
				'password and the [repository] configuration.',
				$response->code
			);
		}

		return $obj;
	}

	/**
	 * Add Issue
	 *
	 * Adds a new issue to the Bitbucket installation with the given
	 * parameters (summary, kinds etc.) and returns its identifier.
	 * The parameters must be named according to the Bitbucket API
	 * format,
	 * e.g.:
	 *
	 * summary:     The summary of the new issue
	 * kinds:       The kind the issue should be created with
	 * priority:    The priority the issue should be created with
	 * status:      The status the issue should be created with
	 * description: The description of the new issue
	 */
	public function add_issue($options)
	{
		$fields = array();

		foreach ($options as $field_name => $field_value)
		{
			if (!$field_value)
			{
				continue;
			}

			$field = $this->_format_field(
				$field_name,
				$field_value);

			if (isset($field['name']) && isset($field['value']))
			{
				$fields[$field['name']] = $field['value'];
			}
		}

		$response = $this->_send_command(
			'POST',
			"repositories/$this->_repo/issues/",
			$fields
		);

		return "$response->local_id";
	}

	private function _format_field($field_name, $field_value)
	{
		$data = array();
		$data['name'] = $field_name;

		switch ($field_name)
		{
			case 'summary':
				$data['name'] = 'title';
				$data['value'] = $field_value;
				break;

			case 'description':
				$data['name'] = 'content';
				$data['value'] = $field_value;
				break;

			case 'assignee':
				$data['name'] = 'responsible';
				$data['value'] = $field_value;
				break;

			default:
				$data['name'] = $field_name;
				$data['value'] = $field_value;
		}

		return $data;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);

		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else
		{
			$message = $format;
		}

		throw new BitbucketException($message);
	}
}

class BitbucketException extends Exception
{
}
