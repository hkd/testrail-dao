<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Assembla Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Assembla. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class Assembla_defect_plugin extends defect_plugin
{
	private $_api;

	private $_address;
	private $_key;
	private $_secret;

	private $_field_defaults = array(
		'summary' => array(
			'type' => 'string',
			'label' => 'Summary',
			'size' => 'full',
			'required' => true
		),
		'priority' => array(
			'type' => 'dropdown',
			'label' => 'Priority',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'assignedto' => array(
			'type' => 'dropdown',
			'label' => 'Assigned to',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'milestone' => array(
			'type' => 'dropdown',
			'label' => 'Milestones',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'description' => array(
			'type' => 'text',
			'label' => 'Description',
			'required' => false,
			'rows' => 10
		)
	);

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Assembla defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' =>
			'; Please configure your Assembla connection and API
; settings below. The API key and secret is per user
; and can be found in the Assembla user profiles.
[connection]
address=https://api.assembla.com/
key=testrail
secret=secret
space=name
');

	public function get_meta()
	{
		return self::$_meta;
	}

	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************

	public function __construct()
	{
	}

	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************

	public function validate_config($config)
	{
		$ini = ini::parse($config);

		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}

		$keys = array('address', 'key', 'secret', 'space');

		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}

		$address = $ini['connection']['address'];

		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_key = $ini['connection']['key'];
		$this->_secret = $ini['connection']['secret'];
		$this->_space = $ini['connection']['space'];
	}

	// *********************************************************
	// API / CONNECTION
	// *********************************************************

	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}

		$this->_api = new Assembla_api(
			$this->_address,
			$this->_key,
			$this->_secret,
			$this->_space
		);

		return $this->_api;
	}

	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		return array('fields' => $this->_field_defaults);
	}

	private function _get_summary_default($context)
	{
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;

		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}

		return $summary;
	}

	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// Assembla installation.
		if ($field == 'summary' || $field == 'description')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;

				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;
			}

			return $data;
		}

		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}

		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'priority':
				$data['default'] = arr::get($prefs, 'priority');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_priorities($this->_space)
				);
				break;

			case 'assignedto':
				$data['default'] = arr::get($prefs, 'assignedto');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_users($this->_space)
				);
				break;

			case 'milestone':
				$data['default'] = arr::get($prefs, 'milestone');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_milestones($this->_space)
				);
				break;
		}

		return $data;
	}

	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();
		return $api->add_ticket($input);
	}

	// *********************************************************
	// LOOKUP
	// *********************************************************

	public function lookup($ticket_number)
	{
		$api = $this->_get_api();
		$ticket = $api->get_ticket($ticket_number);

		$attributes = array();

		// Add some important attributes for the ticket such as the
		// type, current status and project. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.

		if (isset($this->_space))
		{
			// Add a link to the project.
			$attributes['Space'] = str::format(
				'<a target="_blank"
					href="https://www.assembla.com/spaces/{0}">{1}</a>',
				a($ticket->space_id),
				h($this->_space)
			);
		}

		$status = '';
		if (isset($ticket->status))
		{
			$attributes['Status'] = h($ticket->status);
			$status = $ticket->status;
		}
		else
		{
			$attributes['Status'] = "&ndash;";
		}

		$attributes['Owner'] = "&ndash;";
		if (isset($ticket->assigned_to_id))
		{
			$owner = $api->get_user($ticket->assigned_to_id);
			if ($owner)
			{
				$attributes['Owner'] = h($owner->name);
			}
		}

		$attributes['Reported by'] = "&ndash;";
		if (isset($ticket->reporter_id))
		{
			$reporter = $api->get_user($ticket->reporter_id);
			if ($reporter)
			{
				$attributes['Reported by'] = h($reporter->name);
			}
		}

		$attributes['Milestone'] = "&ndash;";
		if (isset($ticket->milestone_id))
		{
			$milestone = $api->get_milestone($ticket->milestone_id);
			if ($milestone)
			{
				$attributes['Milestone'] = h($milestone->title);
			}
		}

		if (isset($ticket->priority))
		{
			$priority = $api->get_priority($ticket->priority);
			if ($priority)
			{
				$attributes['Priority'] = h($priority->name);
			}
		}
		else
		{
			$attributes['Priority'] = "&ndash;";
		}

		// Decide which status to return to TestRail based on the
		// completion date and status name of the ticket (whether
		// the ticket was resolved or not).
		$status_id = GI_DEFECTS_STATUS_OPEN;

		if (isset($ticket->completed_date))
		{
			// Fixed is one of the default closed states
			if ($ticket->status == 'Fixed')
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}
			else
			{
				$status_id = GI_DEFECTS_STATUS_CLOSED;
			}
		}

		// Format the description of the ticket (we use a monospace
		// font).
		if (isset($ticket->description))
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($ticket->description)
					)
				)
			);
		}
		else
		{
			$description = null;
		}

		return array(
			'id' => $ticket_number,
			'url' => str::format(
				'https://www.assembla.com/spaces/{0}/tickets/{1}',
				a($this->_space),
				h($ticket_number)
			),
			'title' => $ticket->summary,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Assembla REST API
 *
 * Wrapper class for the Assembla API with functions for retrieving
 * projects, getting and adding tickets etc.
 */
class Assembla_api
{
	private static $_priorities = array(
		'1' => 'Highest',
		'2' => 'High',
		'3' => 'Normal',
		'4' => 'Low',
		'5' => 'Lowest',
	);

	private $_address;
	private $_key;
	private $_secret;
	private $_space;
	private $_curl;

	/**
	 * Construct
	 *
	 * Initializes a new Assembla API object. Expects the web address
	 * of the Assembla API endpoint (e.g. api.assembla.com) including
	 * http or https prefix.
	 */
	public function __construct($address, $key, $secret, $space)
	{
		$this->_address = str::slash($address);
		$this->_key = $key;
		$this->_secret = $secret;
		$this->_space = $space;
	}

	/**
	 * Get Priorities
	 *
	 * Returns a list of priorities for the given project. Priorities
	 * are returned as array of objects, each with its ID and name.
	 */
	public function get_priorities()
	{
		// The priorities are hard-coded since there's no API for
		// this as it appears.
		$result = array();
		foreach (self::$_priorities as $prioritiy => $name)
		{
			$p = obj::create();
			$p->id = $prioritiy;
			$p->name = $name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get Priority
	 *
	 * Returns the priority for a given user priority ID.
	 */
	public function get_priority($priority_id)
	{
		$name = arr::get(self::$_priorities, $priority_id);
		if (!$name)
		{
			return null;
		}

		$p = obj::create();
		$p->id = $priority_id;
		$p->name = $name;
		return $p;
	}

	/**
	 * Get Users
	 *
	 * Returns a list of users for the given project for the Assembla
	 * installation. Users are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_users($space)
	{
		$response = $this->_send_command(
			'GET', 
			"spaces/$space/users"
		);

		if (!$response)
		{
			return array();
		}

		$result = array();
		foreach ($response as $user)
		{
			$p = obj::create();
			$p->id = (string) $user->id;
			$p->name = (string) $user->name;
			$result[] = $p;
		}

		return $result;
	}

	/**
	 * Get User
	 *
	 * Returns the user for a given user ID.
	 */
	public function get_user($user_id)
	{
		return $this->_send_command('GET', "users/$user_id");
	}

	/**
	 * Get Milestones
	 *
	 * Returns a list of milestones for the given project for the
	 * Assembla installation. Milestones are returned as array of
	 * objects, each with its ID and title.
	 */
	public function get_milestones($space)
	{
		$response = $this->_send_command(
			'GET', 
			"spaces/$space/milestones?per_page=100"
		);

		if (!$response)
		{
			return array();
		}

		$result = array();
		foreach ($response as $milestone)
		{
			$m = obj::create();
			$m->id = (string) $milestone->id;
			$m->name = (string) $milestone->title;
			$result[] = $m;
		}

		return $result;
	}

	/**
	 * Get Milestone
	 *
	 * Returns the milestone for a given milestone ID.
	 */
	public function get_milestone($milestone_id)
	{
		return $this->_send_command(
			'GET',
			"spaces/$this->_space/milestones/$milestone_id"
		);
	}

	/**
	 * Get Ticket
	 *
	 * Gets an existing ticket from the Assembla installation and
	 * returns it. The resulting ticket object has various properties
	 * such as the summary, description etc.
	 */
	public function get_ticket($ticket_id)
	{
		return $this->_send_command(
			'GET',
			"spaces/$this->_space/tickets/$ticket_id"
		);
	}

	private function _send_command($method, $command, $data = null)
	{
		$url = $this->_address . 'v1/' . $command;
		return $this->_send_request($method, $url, $data);
	}

	private function _send_request($method, $url, $data = null)
	{
		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Issuing Assembla HTTP REST request');
			logger::debugr(
				'$request',
				array(
					'method' => $method,
					'url' => $url,
					'data' => $data
				)
			);
		}

		$response = http::request_ex(
			$this->_curl,
			$method,
			$url,
			array(
				'user' => $this->_key,
				'password' => $this->_secret,
				'data' => json::encode($data),
				'headers' => array(
					'Content-Type' => 'application/json',
					'X-Api-Key' => $this->_key,
					'X-Api-Secret' => $this->_secret
				)
			)
		);

		// In case debug logging is enabled, we append the data we've
		// sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Got the following response');
			logger::debugr('$response', $response);
		}

		$obj = json::decode($response->content);

		if ($response->code != 200 && $response->code != 201 &&
			$response->code != 204)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0}). Please check your API key, ' .
				'secret and configured space.',
				$response->code
			);
		}

		return $obj;
	}

	/**
	 * Add Ticket
	 *
	 * Adds a new ticket to the Assembla installation with the given
	 * parameters (summary, priority etc.) and returns its ID. The
	 * parameters must be named according to the Assembla API format,
	 * e.g.:
	 *
	 * summary:		The summary of the new ticket
	 * priority:	The ID of the priority the ticket should be
	 *				added with
	 * assignedto:	The ID of the user the ticket should be
	 *				assigned to
	 * milestone:	The ID of the milestone the ticket should be
	 *				added with
	 * description: The description of the new ticket
	 */
	public function add_ticket($options)
	{
		$fields = array();

		foreach ($options as $field_name => $field_value)
		{
			if (!$field_value)
			{
				continue;
			}

			$field = $this->_format_field(
				$field_name,
				$field_value);

			if (isset($field['name']) && isset($field['value']))
			{
				$fields[$field['name']] = $field['value'];
			}
		}

		$data = array('ticket' => $fields);
		$response = $this->_send_command(
			'POST',
			"spaces/$this->_space/tickets.json",
			$data
		);

		return $response->number;
	}

	private function _format_field($field_name, $field_value)
	{
		$data = array();
		$data['name'] = $field_name;

		switch ($field_name)
		{
			case 'milestone':
				$data['name'] = 'milestone_id';
				$data['value'] = $field_value;
				break;

			case 'assignedto':
				$data['name'] = 'assigned_to_id';
				$data['value'] = $field_value;
				break;

			default:
				$data['value'] = $field_value;
				break;
		}

		return $data;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);

		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else
		{
			$message = $format;
		}

		throw new AssemblaException($message);
	}
}

class AssemblaException extends Exception
{
}
