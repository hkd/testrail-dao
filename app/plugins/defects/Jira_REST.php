<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Jira Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Atlassian Jira. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class Jira_REST_defect_plugin extends Defect_plugin
{
	private $_api;

	private $_address;
	private $_user;
	private $_password;
	private $_subtasks_enabled = false;
	private $_subtasks_autofill = false;
	private $_links_autofill = false;

	private $_default_fields = array(
		'summary' => 'on',
		'project' => 'on',
		'issuetype' => 'on',
		'component' => 'on',
		'assignee' => 'on',
		'priority' => 'on',
		'affects_version' => 'on',
		'fix_version' => 'off',
		'estimate' => 'off',
		'labels' => 'off',
		'environment' => 'off',
		'parent' => 'off',
		'linktype' => 'off',
		'links' => 'off',
		'description' => 'on'
	);

	private $_field_defaults = array(
		'summary' => array(
			'type' => 'string',
			'label' => 'Summary',
			'size' => 'full',
			'required' => true
		),
		'project' => array(
			'type' => 'dropdown',
			'label' => 'Project',
			'required' => true,
			'remember' => true,
			'cascading' => true,
			'size' => 'compact'
		),
		'issuetype' => array(
			'type' => 'dropdown',
			'label' => 'Issue Type',
			'required' => true,
			'remember' => true,
			'cascading' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'component' => array(
			'type' => 'dropdown',
			'label' => 'Component',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'assignee' => array(
			'type' => 'dropdown',
			'label' => 'Assignee',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'priority' => array(
			'type' => 'dropdown',
			'label' => 'Priority',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),
		'affects_version' => array(
			'type' => 'dropdown',
			'label' => 'Affects Version',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'fix_version' => array(
			'type' => 'dropdown',
			'label' => 'Fix Version',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'environment' => array(
			'type' => 'text',
			'label' => 'Environment',
			'rows' => 4,
			'required' => false
		),
		'estimate' => array(
			'type' => 'string',
			'label' => 'Estimate',
			'required' => false,
			'size' => 'compact'
		),
		'labels' => array(
			'type' => 'string',
			'label' => 'Labels',
			'description' => 'A comma separated list of labels.',
			'required' => false,
			'size' => 'full'
		),
		'description' => array(
			'type' => 'text',
			'label' => 'Description',
			'required' => false,
			'rows' => 10
		),
		'parent' => array(
			'type' => 'string',
			'label' => 'Parent Task',
			'required' => false,
			'size' => 'compact'
		),
		'linktype' => array(
			'type' => 'dropdown',
			'label' => 'Link Type',
			'required' => false,
			'size' => 'compact'
		),
		'links' => array(
			'type' => 'string',
			'label' => 'Issue Links',
			'required' => false,
			'size' => 'compact'
		)
	);

	private static $_meta_defects = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'JIRA defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your JIRA connection below.
;
; Note: requires JIRA 5 or later or JIRA OnDemand/Cloud. You
; can use the \'JIRA SOAP\' defect plugin for older versions.
[connection]
address=https://<your-server>/
user=testrail
password=secret

[push.fields]
summary=on
project=on
issuetype=on
component=on
assignee=on
priority=on
affects_version=on
fix_version=off
estimate=off
labels=off
environment=off
parent=off
linktype=off
links=off
description=on
');

	private static $_meta_references = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'JIRA reference plugin for TestRail',
		'can_push' => false, // Lookup only
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your JIRA connection below.
;
; Note: requires JIRA 5 or later or JIRA OnDemand/Cloud. You
; can use the \'JIRA SOAP\' defect plugin for older versions.
[connection]
address=https://<your-server>/
user=testrail
password=secret
');

	public function get_meta()
	{
		if ($this->get_type() == GI_INTEGRATION_TYPE_REFERENCES)
		{
			return self::$_meta_references;
		}
		else 
		{
			return self::$_meta_defects;
		}
	}
	
	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************
	
	public function __construct()
	{
	}
	
	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}
		
		$keys = array('address', 'user', 'password');
		
		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}

		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}

		if (isset($ini['push.fields']))
		{
			// Rules to verify custom fields
			foreach ($ini['push.fields'] as $field => $option)
			{
				if ($option != 'on')
				{
					continue;
				}

				$this->_validate_field($ini, $field);
			}
		}
	}

	private function _validate_field($ini, $field)
	{
		static $valid_types = array(
			'dropdown' => true,
			'multiselect' => true,
			'text' => true,
			'string' => true
		);

		$category = arr::get($ini, "push.field.$field");

		// Custom fields must always have a separate category.
		if (str::starts_with($field, 'customfield_'))
		{
			if (!$category)
			{
				throw new ValidationException(
					str::format(
						'Field "{0}" is enabled but configuration ' .
						'section [push.field.{0}] is missing',
					 	$field
					 )
				);
			}

			$keys = array('label', 'type');
			foreach ($keys as $key)
			{
				if (!isset($category[$key]))
				{
					throw new ValidationException(
						str::format(
							'Missing configuration for key "{0}" in ' . 
							'section [push.field.{1}]',
							$key,
							$field
						)
					);
				}
			}
		}

		// The specified type must be well-known.
		$type = arr::get($category, 'type');
		if ($type)
		{
			if (!isset($valid_types[str::to_lower($type)]))
			{
				throw new ValidationException(
					str::format(
						'Invalid field type specified in section ' .
						'[push.field.{0}]',
						$field
					)
				);
			}
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = $this->_sanetize_address(
			$ini['connection']['address']
		);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
		$this->_config = $ini;
	}

	private function _sanetize_address($address)
	{
		// We work around common configuration issues with the JIRA
		// address by removing often used but invalid paths/addresses.

		$replace = array(
			'\\/secure\\/Dashboard\\.jspa\\??$' => '',
			'\\/secure\\/MyJiraHome\\.jspa\\??$' => '',
			'\\/secure\\/CreateIssue!default\\.jspa\\??$' => '',
			'\\/login\\?dest-url=.*$' => '',
			'\\/login\\??$' => '',
		);

		foreach ($replace as $re => $value)
		{
			$address = preg_replace("/$re/i", $value, $address);
		}

		return str::slash($address);
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}

		$this->_api = new Jira_REST_api(
			$this->_address,
			$this->_user,
			$this->_password
		);
		return $this->_api;
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************
		
	public function prepare_push($context)
	{
		$fields = array();

		if (isset($this->_config['push.fields']))
		{
			$fields_config = $this->_config['push.fields'];
		}
		else
		{
			$fields_config = $this->_default_fields;
		}

		// Return a form with a dynamic list of fields/properties,
		// based on the configuration of the defect plugin.
		foreach ($fields_config as $field_name => $option)
		{
			if ($option != 'on')
			{
				continue;
			}

			if (isset($this->_field_defaults[$field_name]))
			{
				$field = $this->_field_defaults[$field_name];
			}
			else
			{
				$field = array();
			}

			$category_name = "push.field.$field_name";
			$category = arr::get($this->_config, $category_name);

			if ($category)
			{
				foreach ($category as $property => $value)
				{
					$property = str::to_lower($property);

					if ($property != 'label' && 
						$property != 'description')
					{
						$value = str::to_lower($value);
						if ($value == 'true')
						{
							$value = true;
						}
						elseif ($value == 'false')
						{
							$value = false;
						}
					}

					if ($property == 'rows')
					{
						if (check::integer($value))
						{
							$value = (int) $value;
						}
					}

					// This may override the default value from above.
					$field[$property] = $value;
				}
			}

			if (str::starts_with($field_name, 'customfield_'))
			{
				// All custom fields depend on the issue type (and
				// indirectly on the project).
				$field['depends_on'] = 'issuetype';
			}

			$fields[$field_name] = $field;
		}

		$result = array('fields' => $fields);

		// The user can also customize the width of the dialog.
		if (isset($this->_config['push.dialog']['width']))
		{
			$result['width'] = 
				(int) $this->_config['push.dialog']['width'];
		}

		if (isset($fields['parent']))
		{
			// Set the sub-task related fields
			$this->_subtasks_enabled = true;
			$autofill = arr::get($fields['parent'], 'autofill');
			if ($autofill)
			{
				$this->_subtasks_autofill = 
					str::to_lower($autofill) == 'on';
			}
		}

		if (isset($fields['links']))
		{
			$autofill = arr::get($fields['links'], 'autofill');
			if ($autofill)
			{
				$this->_links_autofill = 
					str::to_lower($autofill) == 'on';
			}
		}

		// Save the form for later use in prepare_field().
		$this->_form = $result;
		return $result;
	}

	private function _get_summary_default($context)
	{
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}

		return $summary;
	}

	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	private function _get_parent_default($context)
	{
		if (!$this->_subtasks_autofill)
		{
			return null;
		}
		
		foreach ($context['tests'] as $test)
		{
			$case = $test->case;
			if ($case->refs)
			{
				$refs = str::split($case->refs, ',');
				if ($refs)
				{
					// We can only use one reference as the parent.
					return $refs[0];
				}
			}
		}

		return null;
	}
	
	private function _get_links_default($context)
	{
		if (!$this->_links_autofill)
		{
			return null;
		}
		
		$refs = '';
		foreach ($context['tests'] as $test)
		{
			$case = $test->case;
			if ($case->refs)
			{
				if ($refs)
				{
					$refs .= ', ';
				}

				$refs .= $case->refs;
			}
		}

		return $refs;
	}
	
	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// Jira installation.	
		if ($field == 'summary' || $field == 'description' ||
			$field == 'parent' || $field == 'links')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;

				case 'parent':
					$data['default'] = $this->_get_parent_default(
						$context);
					break;

				case 'links':
					$data['default'] = $this->_get_links_default(
						$context);
					break;
			}
		
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'project':
				$data['default'] = arr::get($prefs, 'project');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_projects()
				);
				break;

			case 'issuetype':
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_types(
						$input['project'],
						$this->_subtasks_enabled
					)
				);
				
				// Select the stored preference or the first item in
				// the list otherwise.
				$default = arr::get($prefs, 'issuetype');
				if ($default)
				{
					$data['default'] = $default;
				}
				else
				{
					if ($data['options'])
					{
						$data['default'] = key($data['options']);
					}
				}
				break;

			case 'assignee':
				$data['default'] = arr::get($prefs, 'assignee');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_assignees($input['project'])
				);
				break;

			case 'component':
				$data['default'] = arr::get($prefs, 'component');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_components($input['project'])
				);
				break;

			case 'affects_version':
			case 'fix_version':
				$show_archived = true;
				$show_released = true;
				$show_overdue = true;

				$category = arr::get(
					$this->_config, "push.field.$field"
				);

				if ($category)
				{
					if (isset($category['show_archived']))
					{
						if ($category['show_archived'] == 'false')
						{
							$show_archived = false;
						}
					}

					if (isset($category['show_released']))
					{
						if ($category['show_released'] == 'false')
						{
							$show_released = false;
						}
					}

					if (isset($category['show_overdue']))
					{
						if ($category['show_overdue'] == 'false')
						{
							$show_overdue = false;
						}
					}
				}

				$data['default'] = arr::get($prefs, $field);
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_versions(
						$input['project'],
						$show_archived,
						$show_released,
						$show_overdue
					)
				);
				break;

			case 'priority':
				$data['default'] = arr::get($prefs, 'priority');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_priorities()
				);
				break;

			case 'linktype':
				$data['default'] = arr::get($prefs, 'linktype');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_link_types()
				);
				break;
		}

		if (str::starts_with($field, 'customfield_'))
		{
			// All custom fields depend on both the project and issue
			// type.
			$data['default'] = arr::get($prefs, $field);
			$data['options'] = $this->_to_id_name_lookup(
				$api->get_customfield_values(
					$input['project'],
					$input['issuetype'],
					$field
				)
			);
		}

		// Prevent an error message in case the field was a multi-
		// select and was changed back to dropdown. The preferences
		// would be an array in this case and the defects controller
		// may raise an error. We need to clear the default value
		// in this case.

		$fields = arr::get($this->_form, 'fields');
		if ($fields[$field]['type'] == 'dropdown')
		{
			$default = arr::get($data, 'default');
			if (is_array($default))
			{
				$data['default'] = '';
			}
		}

		return $data;
	}
	
	public function validate_push($context, $input)
	{
	}	
	
	public function push($context, $input)
	{
		$api = $this->_get_api();		
		return $api->add_issue($input);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();
		$issue = $api->get_issue($defect_id);

		$attributes = array();

		// Add some important attributes for the issue such as the
		// issue type, current status and project. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.

		if (isset($issue->fields->project->name))
		{
			// Add a link to the project.
			$attributes['Project'] = str::format(
				'<a target="_blank" href="{0}browse/{1}">{2}</a>',
				a($this->_address),
				a($issue->fields->project->key),
				h($issue->fields->project->name)
			);
		}

		if (isset($issue->fields->issuetype->name))
		{
			$attributes['Issue Type'] = 
				h($issue->fields->issuetype->name);
		}

		$status = '';
		if (isset($issue->fields->status->name))
		{
			$attributes['Status'] = h($issue->fields->status->name);
			$status = $issue->fields->status->name;
		}
		
		if (isset($issue->fields->components) &&
			count($issue->fields->components) > 0)
		{
			$attributes['Component'] = '';
			foreach ($issue->fields->components as $component)
			{
				$attributes['Component'] .= h($component->name) . ' ';
			}
		}
		else
		{
			$attributes['Component'] = "&ndash;";
		}

		if (isset($issue->fields->assignee->displayName))
		{
			$attributes['Assignee'] = 
				h($issue->fields->assignee->displayName);
		}
		else
		{
			$attributes['Assignee'] = "&ndash;";
		}

		if (isset($issue->fields->priority->name))
		{
			$attributes['Priority'] = 
				h($issue->fields->priority->name);
		}
		else
		{
			$attributes['Priority'] = "&ndash;";
		}

		// Decide which status to return to TestRail based on the
		// resolution property of the issue (whether the issue was
		// resolved or not). The issue or statuses don't have any
		// additional meta information so that's unfortunately the
		// only distinction we can make for the status.
		$status_id = GI_DEFECTS_STATUS_OPEN;

		if (isset($issue->fields->resolution))
		{
			if ($issue->fields->resolution)
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}
		}

		// Format the description of the issue (we use a monospace
		// font).
		if (isset($issue->fields->description) && 
			$issue->fields->description)
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($issue->fields->description)
					)
				)
			);
		}
		else
		{
			$description = null;
		}

		if (!isset($issue->fields->summary))
		{
			throw new Jira_RESTException(
				'Invalid response from JIRA (issue is missing the Summary field).'
			);
		}

		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}browse/{1}',
				$this->_address,
				$defect_id
			),
			'title' => $issue->fields->summary,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Jira REST API
 *
 * Wrapper class for the Jira REST API with functions for retrieving
 * projects, getting and issues etc.
 */
class Jira_REST_api
{
	private $_address;
	private $_user;
	private $_password;
	private $_curl;

	private $_createmeta = array();

	/**
	 * Construct
	 *
	 * Initializes a new Jira API object. Expects the web address
	 * of the Jira installation including http or https prefix.
	 */	
	public function __construct($address, $user, $password)
	{
		$this->_address = str::slash($address);
		$this->_user = $user;
		$this->_password = $password;
	}

	/**
	 * Get Types
	 *
	 * Returns a list of issue types for the Jira installation. The
	 * issue types are returned as array of objects, each with its ID
	 * and name.
	 */	
	public function get_types($project_key, $allow_subtasks = true)
	{
		$response = $this->_send_command(
			'GET', 
			"project/$project_key"
		);
		
		if (!$response->issueTypes)
		{
			return array();
		}
		
		$result = array();
		foreach ($response->issueTypes as $type)
		{
			if (!$allow_subtasks)
			{
				if (isset($type->subtask) && $type->subtask)
				{
					continue; // A sub-task issue type
				}
			}

			$t = obj::create();
			$t->id = (string) $type->id;
			$t->name = (string) $type->name;
			$result[] = $t;
		}
		
		return $result;
	}
	
	/**
	 * Get Projects
	 *
	 * Returns a list of projects for the Jira installation. The
	 * projects are returned as array of objects, each with its ID
	 * and name.
	 */	
	public function get_projects()
	{
		$response = $this->_send_command('GET', 'project');
		
		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $project)
		{
			$p = obj::create();
			$p->id = (string) $project->key;
			$p->name = (string) $project->name;
			$result[] = $p;
		}
		
		return $result;
	}

	/** 
	 * Get Assignees in batches of $limit
	 * 
	 * Return a list of Assignees based on $offset and $limit, as the
	 * Jira API limits the number of results to 1000, 50 if no limit
	 * is specified.
	 */
	private function _get_assignee_batches($project_key, $offset, 
		$limit)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				"user/assignable/search?project={0}&startAt={1}&maxResults={2}",
				$project_key,
				$offset,
				$limit
			)
		);
		
		return $response;
	}

	/**
	 * Get Assignee
	 *
	 * Returns a list of assignees for the given project for the Jira
	 * installation. Assignees are returned as array of objects, each
	 * with its name (ID) and display name.
	 */
	public function get_assignees($project_key)
	{
		$offset = 0;
		$limit = 1000;

		$r = $this->_get_assignee_batches($project_key, $offset, $limit);
		$response = $r;

		$count = count($r);
		while ($count == $limit)
		{
			$offset += $limit;
			$r = $this->_get_assignee_batches($project_key, $offset,
				$limit);

			$count = count($r);
			if ($count > 0)
			{
				$response = array_merge($response, $r);
			}
		}

		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $assignee)
		{
			$a = obj::create();
			$a->id = (string) $assignee->name;
			$a->name = (string) $assignee->displayName;
			$result[] = $a;
		}
		
		return $result;
	}

	/**
	 * Get Components
	 *
	 * Returns a list of components for the given project for the Jira
	 * installation. Components are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_components($project_key)
	{
		$response = $this->_send_command(
			'GET',
			"project/$project_key/components"
		);

		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $component)
		{
			$c = obj::create();
			$c->id = (string) $component->id;
			$c->name = (string) $component->name;
			$result[] = $c;
		}
		
		return $result;
	}

	/**
	 * Get Versions
	 *
	 * Returns a list of versions for the given project for the Jira
	 * installation. Versions are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_versions($project_key, $show_archived,
		$show_released, $show_overdue)
	{
		$response = $this->_send_command(
			'GET',
			"project/$project_key/versions"
		);

		if (!$response)
		{
			return array();
		}

		$result = array();
		foreach ($response as $version)
		{
			// Don't show Archived versions if they are disabled in the
			// configuration.
			if (!$show_archived && $version->archived)
			{
				continue;
			}

			// Don't show Released versions if they are disabled in the
			// configuration.
			if (!$show_released && $version->released)
			{
				continue;
			}

			// Don't show Overdue versions if they are disabled in the
			// configuration.
			if (isset($version->overdue))
			{
				if (!$show_overdue && $version->overdue)
				{
					continue;
				}
			}

			$v = obj::create();
			$v->id = (string) $version->id;
			$v->name = (string) $version->name;
			$result[] = $v;
		}
		
		return $result;
	}

	/**
	 * Get Link Types
	 *
	 * Returns a list of link types for the given project for the Jira
	 * installation. Link types are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_link_types()
	{
		$response = $this->_send_command('GET', 'issueLinkType');

		if (!$response)
		{
			return array();
		}

		if (!isset($response->issueLinkTypes))
		{
			return array();
		}

		$result = array();
		foreach ($response->issueLinkTypes as $type)
		{
			$t = obj::create();
			$t->id = (string) $type->id;
			$t->name = (string) $type->name;
			$result[] = $t;
		}
		
		return $result;
	}	

	/**
	 * Get Priorities
	 *
	 * Returns a list of priorities for the given project for the Jira
	 * installation. Priorities are returned as array of objects, each
	 * with its ID and name.
	 */
	public function get_priorities()
	{
		$response = $this->_send_command('GET', 'priority');

		if (!$response)
		{
			return array();
		}
		
		$result = array();
		foreach ($response as $priorities)
		{
			$p = obj::create();
			$p->id = (string) $priorities->id;
			$p->name = (string) $priorities->name;
			$result[] = $p;
		}
		
		return $result;
	}

	private function _get_createmeta($project_key, $issuetype)
	{
		if (isset($this->_createmeta[$project_key][$issuetype]))
		{
			return $this->_createmeta;
		}

		$meta = $this->_send_command(
			'GET',
			str::format(
				'issue/createmeta?projectKeys={0}&issuetypeIds={1}&expand=projects.issuetypes.fields',
				$project_key,
				$issuetype
			)
		);

		foreach ($meta->projects as $project)
		{
			foreach ($project->issuetypes as $issuetype)
			{
				$this->_createmeta[$project->key][$issuetype->id] = 
					$issuetype->fields;
			}
		}

		return $this->_createmeta;
	}

	/**
	 * Get Customfield Values
	 *
	 * Returns a list of customfield values for the given project, 
	 * issue type and custom field name for the Jira installation.
	 * Priorities are returned as array of objects, each with its ID
	 * and value.
	 */
	public function get_customfield_values($project_key, $issuetype,
		$field_name)
	{
		$meta = $this->_get_createmeta($project_key, $issuetype);

		if (!isset($meta[$project_key][$issuetype]))
		{
			return array();
		}
		
		$fields = $meta[$project_key][$issuetype];

		if (!isset($fields->$field_name) || 
			!isset($fields->$field_name->schema->custom))
		{
			return array();
		}

		$field_meta = $fields->$field_name;
				
		switch ($field_meta->schema->custom)
		{
			case 'com.atlassian.jira.plugin.system.customfieldtypes:userpicker':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker':
				return $this->_get_customfield_values_users(
					$project_key,
					$field_meta
				);

			case 'com.atlassian.jira.plugin.system.customfieldtypes:grouppicker':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker':
				return $this->_get_customfield_values_groups(
					$project_key,
					$field_meta
				);

			default:
				return $this->_get_customfield_values_default(
					$project_key,
					$field_meta
				);
		}

		return $result;
	}

	private function _get_customfield_values_default($project_key,
		$field_meta)
	{
		if (!isset($field_meta->allowedValues))
		{
			return array();
		}

		$values = $field_meta->allowedValues;

		$result = array();
		foreach ($values as $field_id => $field_value)
		{
			// Handle the special case 'Cascading Select'
			if (isset($field_value->children))
			{
				foreach ($field_value->children as $child_value)
				{
					// Save the id of the parent and child separated
					// by ':' that way we can decode it when it's
					// selected in the dropdown, as we need to provide
					// parent and child id in add_issue().
					$c = obj::create();
					$c->id = (string) $field_value->id . ':' . 
						$child_value->id;
					$c->name = (string) $field_value->value . ' > ' .
						$child_value->value;
					$result[] = $c;
				}
			}
			else
			{
				$c = obj::create();
				$c->id = (string) $field_value->id;
				if (isset($field_value->value))
				{
					$c->name = (string) $field_value->value;
				}
				else 
				{
					$c->name = (string) $field_value->name;
				}
				$result[] = $c;
			}
		}

		return $result;
	}

	private function _get_customfield_values_users($project_key,
		$field_meta)
	{
		return $this->get_assignees($project_key);
	}

	private function _get_customfield_values_groups()
	{
		$response = $this->_send_command(
			'GET',
			'groups/picker?maxResults=1000'
		);

		$result = array();
		foreach ($response->groups as $group)
		{
			$a = obj::create();
			$a->id = (string) $group->name;
			$a->name = (string) $group->name;
			$result[] = $a;
		}

		return $result;
	}

	/**
	 * Get Customfield Type
	 *
	 * Gets the full type of a custom field and returns it as string,
	 * e.g.:
	 * com.atlassian.jira.plugin.system.customfieldtypes:textfield
	 */
	private function _get_customfield_type($project_key, $issuetype, 
		$field_name)
	{
		$meta = $this->_get_createmeta($project_key, $issuetype);

		if (!isset($meta[$project_key][$issuetype]))
		{
			return array();
		}
		
		$fields = $meta[$project_key][$issuetype];

		$result = '';
		if (isset($fields->$field_name->schema->custom))
		{
			$result = $fields->$field_name->schema->custom;
		}

		return $result;
	}

	/**
	 * Get Issue
	 *
	 * Gets an existing case from the Jira installation and returns
	 * it. The resulting issue object has various properties such
	 * as the summary, description, project etc.
	 */	 
	public function get_issue($issue_id)
	{
		return $this->_send_command(
			'GET',
			'issue/' . urlencode($issue_id)
		);
	}

	private function _send_command($method, $command, $data = null)
	{
		$url = $this->_address . 'rest/api/2/' . $command;
		return $this->_send_request($method, $url, $data);
	}
	
	private function _send_request($method, $url, $data = null)
	{
		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Issuing Jira HTTP REST request');
			logger::debugr(
				'$request',
				array(
					'method' => $method,
					'url' => $url,
					'data' => $data
				)
			);
		}
				
		$response = http::request_ex(
			$this->_curl,
			$method, 
			$url, 
			array(
				'user' => $this->_user,
				'password' => $this->_password,
				'data' => json::encode($data),
				'headers' => array(
					'Content-Type' => 'application/json'
				)
			)
		);

		// In case debug logging is enabled, we append the data we've
		// sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Got the following response');
			logger::debugr('$response', $response);
		}

		$obj = json::decode($response->content);

		if ($response->code != 200 && $response->code != 201)
		{
			if ($response->code == 401 || $response->code == 403)
			{
				$this->_throw_error(
					'Invalid user or password or insufficient permissions ' .
					'for the integration user (HTTP code {0}). Please ' .
					'make sure to use your actual JIRA username for the ' .
					'integration (not your email address, for example).',
					$response->code
				);
			}
			elseif ($response->code != 400 || !$obj)
			{
				$this->_throw_error(
					'Invalid HTTP code ({0}). Please verify the address ' .
					'of your JIRA installation in the configuration ' .
					'settings and that TestRail can reach your JIRA ' . 
					'server.',
					$response->code
				);
			}
			else
			{
				$errors = '';

				if (isset($obj->errorMessages))
				{
					foreach ($obj->errorMessages as $error)
					{
						$errors .= "\n$error";
					}
				}

				if (isset($obj->errors))
				{
					foreach ($obj->errors as $field => $error)
					{
						$errors .= "\n$field: $error";
					}
				}

				$this->_throw_error($errors);
			}
		}

		return $obj;
	}

	/**
	 * Add Issue
	 *
	 * Adds a new issue to the Jira installation with the given
	 * parameters (title, project etc.) and returns its ID. The
	 * parameters must be named according to the Jira API format,
	 * e.g.:
	 *
	 * summary:     The summary of the new issue
	 * issuetype:   The ID of the type of the new issue (bug,
	 *              feature request etc.)
	 * project:     The ID of the project the issue should be added
	 *              to
	 * component:   The ID of the component the issue should be
	 *              added to
	 * description: The description of the new issue
	 */	
	public function add_issue($options)
	{
		$fields = array();

		// Get the JIRA submit format for the fields to submit (system
		// and custom).
		foreach ($options as $field_name => $field_value)
		{
			if ($field_value === null || $field_value === '')
			{
				continue;
			}

			if (!str::starts_with($field_name, 'customfield_'))
			{
				if ($field_name == 'links' || $field_name == 'linktype')
				{
					continue;
				}

				$field = $this->_format_system_field(
					$field_name,
					$field_value);
			}
			else
			{
				$field = $this->_format_custom_field(
					$field_name,
					$field_value,
					$options['project'],
					$options['issuetype']
				);
			}

			if (isset($field['name']) && isset($field['value']))
			{
				$fields[$field['name']] = $field['value'];
			}
		}

		// Submit the JIRA issue.
		$data = array('fields' => $fields);
		$response = $this->_send_command('POST','issue', $data);
		$issue_id = $response->key;

		// Create links to other JIRA issues, if any.
		if (isset($options['links']) && isset($options['linktype']))
		{
			$links = str::split($options['links'], ',');
			foreach ($links as $linked_id)
			{
				$data = array(
					'type' => array(
						'id' => $options['linktype']
					),
					'inwardIssue' => array(
						'key' => $issue_id
					),
					'outwardIssue' => array(
						'key' => str::trim($linked_id)
					)
				);

				$this->_send_command('POST', 'issueLink', $data);
			}
		}

		return $issue_id;
	}

	private function _format_system_field($field_name, $field_value)
	{
		$data = array();
		$data['name'] = $field_name;

		switch ($field_name)
		{
			case 'parent':
				$data['value'] = array('key' => $field_value);
				break;

			case 'project':
				$data['value'] = array('key' => $field_value);
				break;

			case 'assignee':
				$data['value'] = array('name' => $field_value);
				break;

			case 'component':
			case 'affects_version':
			case 'fix_version':
				if ($field_name == 'fix_version')
				{
					$data['name'] = 'fixVersions';
				}
				elseif ($field_name == 'affects_version')
				{
					$data['name'] = 'versions';
				}
				else
				{
					$data['name'] = 'components';
				}

				if (is_array($field_value))
				{
					$tmp = array();
					foreach ($field_value as $value)
					{
						$tmp[]['id'] = $value;
					}
					$data['value'] = $tmp;
				}
				else
				{
					$data['value'] = array(array('id' =>
						$field_value));
				}
				break;

			case 'priority':
			case 'issuetype':
				$data['value'] = array('id' => $field_value);
				break;

			case 'estimate':
				$data['name'] = 'timetracking';
				$data['value'] = array('originalEstimate' =>
					$field_value);
				break;

			case 'labels':
				// Replace all whitespaces in a label as they are not
				// allowed and will return an error by Jira.
				$labels = preg_replace('/\s+/u', '', $field_value);
				$data['name'] = 'labels';
				$data['value'] = str::split($labels, ',');
				break;

			default:
				$data['value'] = $field_value;
				break;
		}

		return $data;
	}

	private function _format_custom_field($field_name, $field_value,
		$project_key, $issuetype)
	{
		$data = array();
		$data['name'] = $field_name;

		// Get the type of the custom field to decide which format to
		// use to submit the field to Jira.
		$field_type = $this->_get_customfield_type(
			$project_key,
			$issuetype,
			$field_name
		);

		switch ($field_type)
		{
			case 'com.atlassian.jira.plugin.system.customfieldtypes:textfield':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:textarea':
				$data['value'] = $field_value;
				break;
				
			case 'com.atlassian.jira.plugin.system.customfieldtypes:float':
				// Make it work with commas, too.
				$field_value = str::replace($field_value, ',', '.');
				$data['value'] = (double) $field_value;
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:select':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:radiobuttons':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:version':
				// These field types do not support multiple items.
				// This needs to be handled in case the user sets the
				// field to a multiselect in TestRail.
				if (is_array($field_value))
				{
					$this->_throw_error(
						'Multi-select is not supported for the ' .
						'custom field "{0}".',
						$field_name
					);
				}
							
				$data['value'] = array('id' => $field_value);
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:multiselect':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:multicheckboxes':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:multiversion':
				// In case the field is configured as dropdown (not
				// multiselect), we need to handle the single value
				// case as well.
				if (!is_array($field_value))
				{
					$field_value = array($field_value);
				}

				$tmp = array();
				foreach ($field_value as $value) 
				{
					$tmp[]['id'] = $value;
				}

				$data['value'] = $tmp;
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:cascadingselect':
				// This field type does not support multiple items.
				// This needs to be handled in case the user sets the
				// field to a multiselect in TestRail.
				if (is_array($field_value))
				{
					$this->_throw_error(
						'Multi-select is not supported for the ' .
						'custom field "{0}".',
						$field_name
					);
				}

				// Split parent and child id e.g. 111002:111003
				$ids = str::split($field_value, ':');
				$parent = obj::create();
				$parent->id = $ids[0];
				if (isset($ids[1]))
				{
					$parent->child = obj::create();
					$parent->child->id = $ids[1];
				}
				$data['value'] = $parent;
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:userpicker':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:grouppicker':
				// This field type does not support multiple items.
				// This needs to be handled in case the user sets the
				// field to a multiselect in TestRail.
				if (is_array($field_value))
				{
					$this->_throw_error(
						'Multi-select is not supported for the ' .
						'custom field "{0}".',
						$field_name
					);
				}

				$data['value'] = array('name' => $field_value);
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:multiuserpicker':
			case 'com.atlassian.jira.plugin.system.customfieldtypes:multigrouppicker':
				$result = array();

				// If $field_value is an array we got the result from a
				// multi-select field
				if (is_array($field_value))
				{
					foreach ($field_value as $user)
					{
						$u = obj::create();
						$u->name = (string) $user;
						$result[] = $u;
					}
					
				}
				else
				{
					// If $field_value is not an array we got it from
					// a field that cannot submit multiple values, e.g.
					// dropdown
					$u = obj::create();
					$u->name = (string) $field_value;
					$result[] = $u;
				}

				$data['value'] = $result;
				break;

			case 'com.atlassian.jira.plugin.system.customfieldtypes:labels':
				// Replace all whitespaces in a label as they are not
				// allowed and will return an error by Jira.
				$labels = preg_replace('/\s+/u', '', $field_value);
				$data['value'] = str::split($labels, ',');
				break;
		}

		return $data;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new Jira_RESTException($message);
	}
}

class Jira_RESTException extends Exception
{
}
