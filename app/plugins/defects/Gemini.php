<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Gemini Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Countersoft Gemini. Please
 * see http://docs.gurock.com/testrail-integration/defects-plugins
 * for more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */
 
class Gemini_defect_plugin extends Defect_plugin
{
	private $_api;
	
	private $_address;
	private $_user;
	private $_token;
	private $_reporter;
	
	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Gemini defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your Gemini connection below
[connection]
address=http://<your-server>.ongemini.com/
user=testrail
token=secret
reporter=1'
	);
		
	public function get_meta()
	{
		return self::$_meta;
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}
		
		$keys = array('address', 'user', 'token', 'reporter');
		
		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}
		
		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}
	
	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_token = $ini['connection']['token'];
		$this->_reporter = $ini['connection']['reporter'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}
		
		$this->_api = new Gemini_api(
			$this->_address,
			$this->_user,
			$this->_token);
			
		return $this->_api;
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		// Return a form with the following fields/properties
		return array(
			'fields' => array(
				'title' => array(
					'type' => 'string',
					'label' => 'Title',
					'required' => true,
					'size' => 'full'
				),
				'type' => array(
					'type' => 'dropdown',
					'label' => 'Type',
					'required' => true,
					'remember' => true,
					'size' => 'compact'
				),
				'project' => array(
					'type' => 'dropdown',
					'label' => 'Project',
					'required' => true,
					'remember' => true,
					'cascading' => true,
					'size' => 'compact'
				),
				'component' => array(
					'type' => 'dropdown',
					'label' => 'Component',
					'required' => true,
					'remember' => true,
					'depends_on' => 'project',
					'size' => 'compact'
				),
				'description' => array(
					'type' => 'text',
					'label' => 'Description',
					'rows' => 10
				)
			)
		);
	}
	
	private function _get_title_default($context)
	{
		$test = current($context['tests']);
		$title = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$title .= ' (+others)';
		}
		
		return $title;
	}
	
	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}
	
	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}
	
	public function prepare_field($context, $input, $field)
	{
		$data = array();
		
		// Process those fields that do not need a connection to the
		// Gemini installation.		
		if ($field == 'title' || $field == 'description')
		{
			switch ($field)
			{
				case 'title':
					$data['default'] = $this->_get_title_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;				
			}
		
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();
		
		switch ($field)
		{
			case 'type':
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_types()
				);
				
				// Select the stored preference or the first item in
				// the list otherwise.
				$default = arr::get($prefs, 'type');
				if ($default)
				{
					$data['default'] = $default;
				}
				else
				{
					if ($data['options'])
					{
						$data['default'] = key($data['options']);
					}
				}
				break;

			case 'project':
				$data['default'] = arr::get($prefs, 'project');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_projects()
				);
				break;

			case 'component':
				if (isset($input['project']))
				{
					$data['default'] = arr::get($prefs, 'component');
					$data['options'] = $this->_to_id_name_lookup(
						$api->get_components($input['project'])
					);
				}
				break;
		}
		
		return $data;
	}
	
	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();		
		$input['reporter'] = $this->_reporter;

		if ($input['description'])
		{
			// Format the description as HTML.
			$input['description'] =	nl2br(
				html::link_urls($input['description'])
			);
		}
		
		return $api->add_issue($input);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();
		$issue = $api->get_issue($defect_id);

		$attributes = array();
		
		// Add some important attributes for the issue such as the
		// issue type, current status and project. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.
		
		if (isset($issue['type']))
		{
			$attributes['Type'] = h($issue['type']);
		}

		$status = '';
		if (isset($issue['status']))
		{
			$attributes['Status'] = h($issue['status']);
			$status = $issue['status'];
		}

		if (isset($issue['project']))
		{
			// Add a link to the project.
			$attributes['Project'] = str::format(
				'<a target="_blank" href="{0}Project/Project.aspx?ProjID={1}">{2}</a>',
				a($this->_address),
				a($issue['project']),
				h($issue['project'])
			);
		}
		
		// Decide which status to return to TestRail based on the
		// final property of the issue's status.
		$status_id = GI_DEFECTS_STATUS_OPEN;
		
		if (isset($issue['status_id']))
		{
			$status_obj = arr::get(
				obj::get_lookup(
					$api->get_statuses()
				),
				$issue['status_id']
			);

			if ($status_obj)
			{
				if ($status_obj->final)
				{
					$status_id = GI_DEFECTS_STATUS_CLOSED;
				}
			}
		}
		
		$description = null;

		// Format the description of the issue (Gemini supports and
		// returns HTML).
		if (isset($issue['description']) && $issue['description'])
		{
			$description = $issue['description'];
		}

		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}issue/ViewIssue.aspx?id={1}',
				$this->_address,
				$defect_id
			),
			'title' => $issue['summary'],
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Gemini API
 *
 * Wrapper class for the Gemini API with functions for retrieving
 * projects etc. from a Gemini installation and adding issues.
 */
class Gemini_api
{
	private $_address;
	private $_headers;
	
	/**
	 * Construct
	 *
	 * Initializes a new Gemini API object. Expects the web address
	 * of the Gemini installation including http or https prefix.
	 */	
	public function __construct($address, $user, $token)
	{
		$this->_address = str::slash($address) . 'api/';
		$this->_headers = array(
			'Content-Type' => 'text/xml',
			'gemini-username-token' => base64::encode($user),
			'gemini-api-token' => base64::encode($token)
		);
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new GeminiException($message);
	}
	
	private function _send_command($method, $command, $data = null)
	{
		$url = $this->_address . $command;
		return $this->_send_request($method, $url, $data);
	}
	
	private function _send_request($method, $url, $data = null)
	{
		$options['data'] = $data;
		$options['headers'] = $this->_headers;
		$response = http::request($method, $url, $options);
		
		// In case debug logging is enabled, we append the data
		// we've sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debugr('$data', $data);
			logger::debugr('$response', $response);
		}
		
		// Parse the DOM before checking the HTTP code in order
		// to make use of the <RestError> tag which describes any
		// errors in detail.
		$dom = $this->_parse_response($response->content);
		
		if ($response->code != 200)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0})', $response->code
			);
		}
		
		return $dom;
	}
	
	private function _parse_response($response)
	{
		// Gemimi does not wrap its result in a <response> tag or
		// something similar. Since it is easier to work with our
		// XML api if we have such a tag, we add it here.
		$response = preg_replace(
			'/(<\?xml[^>]+\?>)(.*)/su',
			'\1<response>\2</response>',
			$response
		);
		
		$dom = xml::parse_string($response);
		
		if (isset($dom->RestError))
		{
			$this->_throw_error((string) $dom->RestError->Message);
		}

		return $dom;
	}
	
	/**
	 * Get Types
	 *
	 * Returns a list of types for the Gemini installation. Types
	 * are returned as array of objects, each with its ID and name.
	 */	
	public function get_types()
	{
		$response = $this->_send_command('GET', 'admin.ashx/issuetypes');
		
		if (!$response)
		{
			return array();
		}
		
		if (!isset($response->ArrayOfIssueTypeEN))
		{
			$this->_throw_error(
				'Invalid response (missing ArrayOfIssueTypeEN tag)'
			);
		}
		
		$types = $response->ArrayOfIssueTypeEN;
		if (!isset($types->IssueTypeEN))
		{
			$this->_throw_error(
				'Invalid response (missing IssueTypeEN tag)'
			);
		}
		
		$result = array();		
		foreach ($types->IssueTypeEN as $type)
		{
			$t = obj::create();
			$t->name = (string) $type->Description;
			$t->id = (string) $type->TypeID;
			$result[] = $t;
		}
		
		return $result;
	}
	
	/**
	 * Get Projects
	 *
	 * Returns a list of projects for the Gemini installation.
	 * Projects are returned as array of objects, each with its ID
	 * and name.
	 */
	public function get_projects()
	{
		$response = $this->_send_command('GET', 'projects.ashx/projects');
		
		if (!$response)
		{
			return array();
		}
		
		if (!isset($response->ArrayOfProjectEN))
		{
			$this->_throw_error(
				'Invalid response (missing ArrayOfProjectEN tag)'
			);
		}
		
		$projects = $response->ArrayOfProjectEN;
		if (!isset($projects->ProjectEN))
		{
			$this->_throw_error(
				'Invalid response (missing ProjectEN tag)'
			);
		}
		
		$result = array();		
		
		foreach ($projects->ProjectEN as $project)
		{
			if (isset($project->ProjectArchived))
			{
				if ($project->ProjectArchived == 'true')
				{
					continue;
				}
			}

			if (isset($project->ProjectReadOnly))
			{
				if ($project->ProjectReadOnly == 'true')
				{
					continue;
				}			
			}
			
			$p = obj::create();
			$p->name = (string) $project->ProjectName;
			$p->id = (string) $project->ProjectID;
			$result[] = $p;
		}
		
		return $result;
	}
	
	/**
	 * Get Components
	 *
	 * Returns a list of components for the given project for the
	 * Gemini installation. Components are returned as array of
	 * objects, each with its ID and name.
	 */
	public function get_components($project_id)
	{
		$response = $this->_send_command(
			'GET', 
			"projects.ashx/projects/$project_id/components"
		);
		
		if (!$response)
		{
			return array();
		}
		
		if (!isset($response->ArrayOfComponentEN))
		{
			$this->_throw_error(
				'Invalid response (missing ArrayOfComponentEN tag)'
			);
		}
		
		$components = $response->ArrayOfComponentEN;
		if (!isset($components->ComponentEN))
		{
			$this->_throw_error(
				'Invalid response (missing ComponentEN tag)'
			);
		}
		
		$result = array();		
		
		foreach ($components->ComponentEN as $component)
		{
			if (isset($component->ComponentReadOnly))
			{
				if ($component->ComponentReadOnly == 'true')
				{
					continue;
				}
			}
			
			$c = obj::create();
			$c->name = (string) $component->ComponentName;
			$c->id = (string) $component->ComponentID;
			$result[] = $c;
		}
		
		return $result;
	}
	
	/**
	 * Get Statuses
	 *
	 * Returns a list of statuses for the Gemini installation.
	 * Statuses are returned as array of objects, each with its ID,
	 * name and a resolved property.
	 */	
	public function get_statuses()
	{
		$response = $this->_send_command(
			'GET', 
			'admin.ashx/issuestatus'
		);
		
		if (!$response)
		{
			return array();
		}
		
		if (!isset($response->ArrayOfIssueStatusEN))
		{
			$this->_throw_error(
				'Invalid response (missing ArrayOfIssueStatusEN tag)'
			);
		}
		
		$statuses = $response->ArrayOfIssueStatusEN;
		if (!isset($statuses->IssueStatusEN))
		{
			$this->_throw_error(
				'Invalid response (missing IssueStatusEN tag)'
			);
		}		
		
		$result = array();		
		
		$states = $response->states;
		foreach ($statuses->IssueStatusEN as $status)
		{
			$s = obj::create();
			$s->name = (string) $status->Description;
			$s->id = (string) $status->StatusID;
			
			if (isset($status->IsFinalState))
			{
				$s->final = $status->IsFinalState == 'true';
			}
			
			$result[] = $s;
		}
		
		return $result;
	}
	
	/**
	 * Get Issue
	 *
	 * Gets an existing case from the Gemini installation and
	 * returns it. The resulting issue object has various properties
	 * such as the summary, description, project etc.
	 */	 
	public function get_issue($issue_id)
	{
		$response = $this->_send_command(
			'GET', 'issues.ashx/issues/' . $issue_id
		);
		
		if (!isset($response->IssueEN))
		{
			$this->_throw_error(
				'Invalid response (missing IssueEN tag)'
			);
		}
		
		$issue = $response->IssueEN;
		
		return array(
			'summary' => (string) $issue->IssueSummary,
			'type_id' => (string) $issue->IssueType,
			'type' => (string) $issue->IssueTypeDesc,
			'project_id' => (string) $issue->ProjectID,
			'project' => (string) $issue->ProjectCode,
			'status_id' => (string) $issue->IssueStatus,
			'status' => (string) $issue->IssueStatusDesc,
			'description' => (string) $issue->IssueLongDesc
		);
	}
		
	/**
	 * Add Issue
	 *
	 * Adds a new issue to the Gemini installation with the given
	 * parameters (title, project etc.) and returns its ID.
	 *
	 * title:       The title of the new issue
	 * type:        The ID of the type of the new issue (bug,
	 *              feature request etc.)
	 * project:     The ID of the project the issue should be added
	 *              to
	 * component:   The ID of the component the issue is added to
	 * description: The description of the new issue
	 */	
	public function add_issue($options)
	{
		$data = str::format(
			'<?xml version="1.0" encoding="UTF-8"?>
			<IssueEN>
				<IssueSummary>{0}</IssueSummary>
				<IssueType>{1}</IssueType>				
				<ProjectID>{2}</ProjectID>
				<Components>
					<IssueComponentEN>
						<ComponentID>{3}</ComponentID>
					</IssueComponentEN>
				</Components>
				<IssueLongDesc>{4}</IssueLongDesc>
				<ReportedBy>{5}</ReportedBy>
			</IssueEN>',
			xml::encode($options['title']),
			$options['type'],
			$options['project'],
			$options['component'],
			xml::encode($options['description']),
			$options['reporter']
		);
		
		$response = $this->_send_command(
			'POST', 
			'issues.ashx/issuespartial',
			$data
		);
		
		if (!isset($response->IssueEN))
		{
			$this->_throw_error(
				'Invalid response (missing IssueEN tag)'
			);
		}
		
		$issue = $response->IssueEN;
		return (string) $issue->IssueID;
	}
}

class GeminiException extends Exception
{
}
