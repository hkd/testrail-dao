<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Email Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for email integration. Please
 * see http://docs.gurock.com/testrail-integration/defects-plugins
 * for more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */
 
class Email_defect_plugin extends Defect_plugin
{
	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Email defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => false, // No lookup for this plugin
		'default_config' => 
			'; You can configure the email address of the
; recipient below. If you leave the recipient empty,
; the user can enter a custom email address in the
; defect dialog.
[email]
to=user@example.com'
	);
	
	public function get_meta()
	{
		return self::$_meta;
	}
	
	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['email']))
		{
			return;
		}
		
		$to = arr::get($ini['email'], 'to');
		if (!$to)
		{
			return;
		}
		
		if (!check::email($to) || $to == 'user@example.com')
		{
			throw new ValidationException(
				'Recipient is not a valid email address'
			);
		}
	}
	
	public function configure($config)
	{
		$ini = ini::parse($config);
		if (isset($ini['email']['to']))
		{
			$this->_to = $ini['email']['to'];
		}
		else 
		{
			$this->_to = null;
		}
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		if (!$this->_to)
		{
			$fields = array(
				'to' => array(
					'type' => 'string',
					'label' => 'Email Address',
					'required' => true,
					'remember' => true
				)
			);
		}
		else
		{
			$fields = array();
		}

		$fields['subject'] = array(
			'type' => 'string',
			'label' => 'Subject',
			'required' => true,
			'size' => 'full'
		);
		
		$fields['body'] = array(
			'type' => 'text',
			'label' => 'Body',
			'rows' => 10
		);

		return array('fields' => $fields);
	}
	
	private function _get_subject_default($context)
	{
		$test = current($context['tests']);
		$summary = '[TestRail] Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}
		
		return $summary;
	}
	
	private function _get_body_default($context)
	{
		return $context['test_change']->description;
	}
	
	public function prepare_field($context, $input, $field)
	{
		$data = array();		
		$prefs = arr::get($context, 'preferences');
		
		switch ($field)
		{
			case 'to':
				$data['default'] = arr::get($prefs, 'to');
				break;
				
			case 'subject':
				$data['default'] = $this->_get_subject_default(
					$context);
				break;
				
			case 'body':
				$data['default'] = $this->_get_body_default(
					$context);
				break;				
		}
		
		return $data;
	}
	
	public function validate_push($context, $input)
	{
		if (isset($input['to']))
		{
			if (!check::email($input['to']))
			{
				throw new ValidationException(
					'Recipient is not a valid email address'
				);
			}
		}
	}

	public function push($context, $input)
	{
		if (isset($input['to']))
		{
			$to = $input['to'];
		}
		else 
		{
			$to = $this->_to;
		}
		
		email::send($to, $input['subject'], $input['body']);
	}
}
