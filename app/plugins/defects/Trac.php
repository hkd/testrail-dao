<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Trac Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Trac. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */
 
define('GI_DEFECTS_TRAC_API_VERSION', '1');

class Trac_defect_plugin extends Defect_plugin
{
	private $_api;
	
	private $_address;
	private $_user;
	private $_password;
	
	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Trac defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your Trac connection below
[connection]
address=http://<your-server>/
user=testrail
password=secret'
	);
	
	public function get_meta()
	{
		return self::$_meta;
	}
	
	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}
		
		$keys = array('address', 'user', 'password');
		
		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}
		
		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}
	
	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}
		
		$this->_api = new Trac_api(
			$this->_address,
			$this->_user,
			$this->_password);
		
		return $this->_api;
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		// Return a form with the following fields/properties
		return array(
			'fields' => array(
				'summary' => array(
					'type' => 'string',
					'label' => 'Summary',
					'required' => true,
					'size' => 'full'
				),
				'type' => array(
					'type' => 'dropdown',
					'label' => 'Type',
					'required' => true,
					'remember' => true,
					'size' => 'compact'
				),
				'component' => array(
					'type' => 'dropdown',
					'label' => 'Component',
					'required' => true,
					'remember' => true,
					'size' => 'compact'
				),
				'description' => array(
					'type' => 'text',
					'label' => 'Description',
					'rows' => 10
				)
			)
		);
	}
	
	private function _get_summary_default($context)
	{		
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}
		
		return $summary;
	}
	
	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}
	
	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}
	
	public function prepare_field($context, $input, $field)
	{
		$data = array();
		
		// Process those fields that do not need a connection to the
		// Trac installation.		
		if ($field == 'summary' || $field == 'description')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;				
			}
		
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();
		
		switch ($field)
		{
			case 'type':
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_types()
				);
				
				// Select the stored preference or the first item in
				// the list otherwise.
				$default = arr::get($prefs, 'type');
				if ($default)
				{
					$data['default'] = $default;
				}
				else
				{
					if ($data['options'])
					{
						$data['default'] = key($data['options']);
					}
				}				
				break;

			case 'component':
				$data['default'] = arr::get($prefs, 'component');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_components()
				);
				break;
		}
		
		return $data;
	}
	
	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();
		return $api->add_ticket($input);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();
		$ticket = $api->get_ticket($defect_id);

		// Decide which status to return to TestRail based on the
		// resolution property of the bug. We can't differentiate it
		// further, it seems.
		if (isset($ticket['resolution']) && $ticket['resolution'])
		{
			$status_id = GI_DEFECTS_STATUS_RESOLVED;
		}
		else 
		{
			$status_id = GI_DEFECTS_STATUS_OPEN;
		}
		
		// Add some important attributes for the ticket such as the
		// current status and component. Note that the attribute
		// values (and description) support HTML and we thus need
		// to escape possible HTML characters (with 'h') in this
		// plugin.

		$attributes = array();

		if (isset($ticket['type']))
		{
			$attributes['Type'] = h($ticket['type']);
		}
		
		if (isset($ticket['status']))
		{
			$attributes['Status'] = h($ticket['status']);
		}

		if (isset($ticket['component']))
		{
			$attributes['Component'] = h($ticket['component']);
		}
		
		// Format the description of the ticket (we use a monospace
		// font).
		if (isset($ticket['description']) && $ticket['description'])
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($ticket['description'])
					)
				)
			);
		}
		else
		{
			$description = null;
		}
		
		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}ticket/{1}',
				$this->_address,
				$defect_id
			),
			'title' => $ticket['summary'],
			'status_id' => $status_id,
			'status' => $ticket['status'],
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Trac API
 *
 * Wrapper class for the Trac API with functions for retrieving
 * tickets etc. from a Trac installation.
 */
class Trac_api
{
	private $_address;
	private $_user;
	private $_password;
	private $_version;
	private $_curl;
	
	/**
	 * Construct
	 *
	 * Initializes a new Trac API object. Expects the web address
	 * of the Trac installation including http or https prefix.
	 */	
	public function __construct($address, $user, $password)
	{
		$this->_address = str::slash($address) . 'login/rpc';
		$this->_user = $user;
		$this->_password = $password;
		$this->_version = $this->_check_version();
	}
	
	private function _check_version()
	{
		$response = $this->_send_command('system.getAPIVersion');
		
		// "Returns a list with three elements. First element is
		//  the epoch (0=Trac 0.10, 1=Trac 0.11 or higher). Second
		//  element is the major version number, third is the minor."
		if (!isset($response[1]))
		{
			$this->_throw_error(
				'Invalid response from version check (no major)'
			);
		}

		// Check if the Trac installation matches our version.
		$major = $response[1];
		if ($major != GI_DEFECTS_TRAC_API_VERSION)
		{
			$this->_throw_error(
				'Unsupported Trac API version: {0}/{1}',
				$major,
				GI_DEFECTS_TRAC_API_VERSION
			);
		}
		
		return $major;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new TracException($message);
	}
	
	private function _send_command($command, $data = array())
	{
		$request = xmlrpc::encode_request($command, $data);

		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		$response = http::request_ex(
			$this->_curl,
			'POST',
			$this->_address,
			array(
				'data' => $request,
				'headers' => array(
					'Content-Type' => 'text/xml',					
				),
				'user' => $this->_user,
				'password' => $this->_password
			)
		);
		
		// In case debug logging is enabled, we append the data
		// we've sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debugr('$request', $request);
			logger::debugr('$response', $response);
		}
		
		if ($response->code != 200)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0})', $response->code
			);
		}

		$dom = xmlrpc::decode_request($response->content);
		
		// Check response for faultCode and/or faultString for any
		// errors.
		if (isset($dom['faultCode']))
		{
			if (isset($dom['faultString']))
			{
				$this->_throw_error(
					'{0} (faultCode: {1})',
					$dom['faultString'],
					$dom['faultCode']
				);
			}
			else 
			{
				$this->_throw_error(
					'Request resulted in error (faultCode: {0})',
					$dom['faultCode']
				);
			}
		}
		else 
		{
			if (isset($dom['faultString']))
			{
				$this->_throw_error(
					$dom['faultString']
				);
			}
		}
		
		return $dom;
	}

	/**
	 * Get Components
	 *
	 * Returns a list of components for the Trac installation.
	 * Components are returned as array of objects, each with its
	 * ID and name.
	 */
	public function get_components()
	{
		$components = $this->_send_command('ticket.component.getAll');
		
		$result = array();
		foreach ($components as $component)
		{
			$c = obj::create();
			$c->name = (string)$component;
			$c->id = $c->name;
			$result[] = $c;
		}
		
		return $result;
	}

	/**
	 * Get Types
	 *
	 * Returns a list of ticket types for the Trac installation.
	 * The types are returned as array of objects, each with its
	 * ID and name.
	 */
	public function get_types()
	{
		$types = $this->_send_command('ticket.type.getAll');
		
		$result = array();
		foreach ($types as $type)
		{
			$t = obj::create();
			$t->name = (string)$type;
			$t->id = $t->name;
			$result[] = $t;
		}
		
		return $result;
	}	
	
	/**
	 * Get Ticket
	 *
	 * Gets an existing ticket from the Trac installation and
	 * returns it. The resulting ticket has various properties such
	 * as the summary, description etc.
	 */	 
	public function get_ticket($ticket_id)
	{
		$data = array($ticket_id);
		$ticket = $this->_send_command('ticket.get', $data);
		
		// "Fetch a ticket. Returns [id, time_created, time_changed,
		//  attributes]."
		if (!isset($ticket[3]) || !is_array($ticket[3]))
		{
			$this->_throw_error(
				'No attributes received for ticket'
			);
		}
		
		$attributes = $ticket[3];
		return array(
			'summary' => $attributes['summary'],
			'status' => $attributes['status'],
			'resolution' => arr::get($attributes, 'resolution'),
			'type' => $attributes['type'],
			'component' => $attributes['component'],
			'description' => $attributes['description']
		);
	}
		
	/**
	 * Add Ticket
	 *
	 * Adds a new ticket to the Trac installation with the given
	 * parameters (title, project etc.) and returns its ID.
	 *
	 * summary:     The summary of the new ticket
	 * type:        The ID of the type of the the new ticket
	 * component:   The ID of the component the ticket is added to
	 * description: The description of the new ticket
	 */	
	public function add_ticket($options)
	{
		$data = array(
			$options['summary'],
			$options['description'],
			array(
				'type' => $options['type'],
				'component' => $options['component'],
			)
		);
		
		$id = $this->_send_command('ticket.create', $data);
		return (string) $id;
	}
}

class TracException extends Exception
{
}

// Check for the xmlrpc PHP module/extensions that is required by
// this plugin.
if (!function_exists('xmlrpc_encode_request'))
{
	throw new TracException(
		'The Trac defect plugin requires the xmlrpc PHP
extension which has not yet been installed.'
	);
}
