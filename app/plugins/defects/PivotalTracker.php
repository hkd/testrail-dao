<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Pivotal Tracker Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Pivotal Tracker. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

define('GI_DEFECTS_PIVOTAL_API', 'services/v4/');
define('GI_DEFECTS_PIVOTAL_STORYPOINTS_FMT',
	'{0} {0?{points}:{point}}');
define('GI_DEFECTS_PIVOTAL_STORYPOINTS_UNESTIMATED', 'Unestimated');
define('GI_DEFECTS_PIVOTAL_STORY_TYPE_FEATURE', 'feature');

class PivotalTracker_defect_plugin extends Defect_plugin
{
	// *********************************************************
	// DEFINITIONS
	// *********************************************************

	private $_story_types = array(
		'feature' => 'Feature',
		'bug' => 'Bug',
		'chore' => 'Chore',
		'release' => 'Release'
	);

	private $_states = array(
		'finished' => 'resolved',
		'delivered' => 'resolved',
		'accepted' => 'closed'
	);

	private $_api;
	private $_user;
	private $_password;

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Pivotal Tracker defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your Pivotal Tracker connection
[connection]
address=https://www.pivotaltracker.com/
user=testrail
password=secret'
	);
		
	public function get_meta()
	{
		return self::$_meta;
	}
	
	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}
		
		$keys = array('address', 'user', 'password');
		
		// Check required values for existence
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}

		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}
	
	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}
		
		$this->_api = new PivotalTracker_api($this->_address);
		$this->_api->login($this->_user, $this->_password);
		return $this->_api;
	}

	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		// Return a form with the following fields/properties
		return array(
			'fields' => array(
				'title' => array(
					'type' => 'string',
					'label' => 'Title',
					'required' => true,
					'size' => 'full'
				),
				'type' => array(
					'type' => 'dropdown',
					'label' => 'Story Type',
					'required' => true,
					'remember' => true,
					'size' => 'compact',
					'cascading' => true
				),
				'project' => array(
					'type' => 'dropdown',
					'label' => 'Project',
					'required' => true,
					'remember' => true,
					'size' => 'compact',
					'cascading' => true
				),
				'estimate' => array(
					'type' => 'dropdown',
					'label' => 'Estimate',
					'depends_on' => 'project|type',
					'size' => 'compact'
				),
				'labels' => array(
					'type' => 'string',
					'label' => 'Labels',
					'size' => 'full',
					'description' => 'A comma separated list of labels.'
				),
				'description' => array(
					'type' => 'text',
					'label' => 'Description',
					'rows' => 10
				)
			)
		);
	}

	private function _get_title_default($context)
	{
		$test = current($context['tests']);
		$title = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$title .= ' (+others)';
		}
		
		return $title;
	}
	
	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// Process those fields that do not need a connection to the
		// Pivotal Tracker installation.		
		if ($field == 'title' || $field == 'description' ||
			$field == 'type')
		{
			switch ($field)
			{
				case 'title':
					$data['default'] = $this->_get_title_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;

				case 'type':
					$data['default'] = arr::get($prefs, 'type');
					$data['options'] = $this->_story_types;
					break;
			}
		
			return $data;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();
		
		switch ($field)
		{
			case 'project':
				$data['default'] = arr::get($prefs, 'project');
				$data['options'] = $this->_to_id_name_lookup(
					$this->_api->get_projects()
				);				
				break;

			case 'estimate':
				$type = arr::get($input, 'type');

				if ($type == GI_DEFECTS_PIVOTAL_STORY_TYPE_FEATURE)
				{
					$project = null;

					if (isset($input['project']))
					{
						$project = $this->_api->get_project(
							$input['project']
						);					
					}

					if (isset($project->point_scale))
					{
						$data['default'] = arr::get($prefs, 'estimate', -1);
						$data['options'] = $this->_get_estimate_scale(
							$project->point_scale);
					}
				}
				else
				{
					$data['disabled'] = true;
				}

				break;
		}
		
		return $data;
	}

	private function _get_estimate_scale($point_scale)
	{
		$result = array(
			-1 => GI_DEFECTS_PIVOTAL_STORYPOINTS_UNESTIMATED
		);

		$points = str::split($point_scale, ',');
		foreach ($points as $p)
		{
			$result[(int)$p] = str::format(
				GI_DEFECTS_PIVOTAL_STORYPOINTS_FMT,
				$p
			);
		}

		return $result;
	}

	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();		
		return $api->add_story($input);
	}

	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($story_id)
	{
		$api = $this->_get_api();
		$story = $api->get_story($story_id);

		$attributes = array();
		
		// Add some important attributes for the issue such as the
		// story type, status etc. Note that the attribute values
		// (and description) support HTML and we thus need to escape
		// possible HTML characters (with 'h') in this plugin.
		
		$status = '';
		if (isset($story->current_state))
		{
			$status = $story->current_state;
		}

		$status_id = GI_DEFECTS_STATUS_OPEN;

		if ($status)
		{
			$status_type = arr::get($this->_states, $status);
			if ($status_type)
			{
				if ($status_type == 'resolved')
				{
					$status_id = GI_DEFECTS_STATUS_RESOLVED;
				}
				else 
				{
					$status_id = GI_DEFECTS_STATUS_CLOSED;					
				}
			}
		}

		if (isset($story->story_type))
		{
			$story_type = arr::get($this->_story_types, 
				$story->story_type);

			if ($story_type)
			{
				$attributes['Story Type'] = h($story_type);
			}
		}

		if ($status)
		{
			$attributes['Status'] = h($status);
		}

		if (isset($story->project_id))
		{
			$project = $api->get_project($story->project_id);
		
			if (isset($project->name))
			{
				$attributes['Project'] = str::format(
					'<a target="_blank" href="{0}projects/{1}">{2}</a>',
					$this->_address,
					a($story->project_id),
					h($project->name)					
				);
			}
		}

		if (isset($story->estimate))
		{
			$attributes['Estimate'] = $story->estimate == -1 ? 
				GI_DEFECTS_PIVOTAL_STORYPOINTS_UNESTIMATED:
				str::format(
					GI_DEFECTS_PIVOTAL_STORYPOINTS_FMT,
					$story->estimate
				);
		}
		else
		{
			$attributes['Estimate'] =
				GI_DEFECTS_PIVOTAL_STORYPOINTS_UNESTIMATED;
		}

		if (isset($story->labels) && $story->labels)
		{
			$labels = str::join(
				str::split($story->labels, ','), 
				', '
			);

			$attributes['Labels'] = h($labels);
		}
		
		// Format the description of the issue (we use a monospace
		// font).
		if (isset($story->description) && $story->description)
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				nl2br(
					html::link_urls(
						h($story->description)
					)
				)
			);
		}
		else
		{
			$description = null;
		}

		return array(
			'id' => $story_id,
			'url' => str::format(
				'{0}story/show/{1}',
				$this->_address,
				$story_id
			),
			'title' => $story->name,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * PivotalTracker API
 *
 * Wrapper class for the Pivotal Tracker Rest API for retrieving
 * stories etc. from a Pivotal Tracker installation. Uses the REST
 * API of Pivotal Tracker.
 */
class PivotalTracker_api
{
	private $_address;
	private $_token;
	private $_curl;

	/**
	 * Construct
	 *
	 * Initializes a new Pivotal Tracker API object. Expects the user
	 * and password to log on to the Pivotal Tracker installation.
	 */
	public function __construct($address)
	{
		$this->_address = str::slash($address);
	}

	/**
	 * Login
	 *
	 * Logs in to the Pivotal Tracker with user and password to
	 * retrieve a user token for subsequent calls.
	 */ 
	public function login($user, $password)
	{
		$auth['user'] = $user;
		$auth['password'] = $password;

		$response = $this->_send_command('GET', 'me', null, $auth);
		
		if (!isset($response->token->guid))
		{
			$this->_throw_error(
				'Unable to retrieve user token. Please check your
				Pivotal Tracker user and password.'
			);
		}

		$this->_token = $response->token->guid;
	}

	private function _send_command($method, $uri, $data = null,
		$auth = null)
	{
		if ($data)
		{
			$options['data'] = $data;
			$options['headers'] = array(
				'Content-type' => 'application/xml'
			);
		}

		if ($this->_token)
		{
			$options['headers']['X-TrackerToken'] = $this->_token;
		}
		else
		{
			// In this case, we have to send the credentials to
			// retrieve the access token.
			$options['user'] = $auth['user'];
			$options['password'] = $auth['password'];
		}

		$url = str::format(
			'{0}{1}{2}',
			$this->_address,
			GI_DEFECTS_PIVOTAL_API,
			$uri
		);

		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		$response = http::request_ex(
			$this->_curl,
			$method,
			$url,
			$options
		);

		// In case debug logging is enabled, we append the url we
		// tried to access and the response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debugr(
				'$rest',
				array(
					'url' => $url,
					'options' => $options,
					'response' => $response
				)
			);
		}

		// Error 401 (Unauthorized) has to be handled seperately
		// because it does not deliver an error message in XML format.
		if ($response->code == 401)
		{
			$this->_throw_error(
				'Access denied. Please check your Pivotal Tracker user
				and password.'
			);
		}

		$content = xml::parse_string($response->content);

		if ($response->code != 200)
		{
			if (isset($content->error))
			{
				$error = $content->error;
			}
			else
			{
				$error = (string) $content;
			}

			$this->_throw_error(
				'Invalid HTTP code ({0}). {1}',
				$response->code,
				$error
			);
		}

		return $content;
	}

	/**
	 * Get Story
	 *
	 * Gets an existing story from the Pivotal Tracker installation
	 * and returns it. The resulting story object has various 
	 * properties such as the id, the name etc. 
	 */
	public function get_story($story_id)
	{
		$response = $this->_send_command('GET', "stories/$story_id");

		$s = obj::create();
		$s->id = (int)$response->id;
		$s->name = (string)$response->name;
		$s->story_type = (string)$response->story_type;
		$s->description = (string)$response->description;
		$s->labels = (string)$response->labels;
		$s->current_state = (string)$response->current_state;
		$s->project_id = (int)$response->project_id;

		if (isset($response->estimate))
		{
			$s->estimate = (float)$response->estimate;
		}

		return $s;
	}

	/**
	 * Add Story
	 *
	 * Adds a new story to the Pivotal Tracker installation with
	 * the given parameters and returns its ID.
	 *
	 * project:		The ID of an existing project to add the story to.
	 * type:		The name of the type of the new story 
	 *				(i.e. feature, bug, etc.)
	 * title:		The title of the new story.
	 * description: The description of the new story.
	 * labels:		A comma separated list of labels for the new story.
	 */
	public function add_story($input)
	{
		$data = str::format(
			'<?xml version="1.0" encoding="UTF-8"?>
			<story>
				<story_type>{0}</story_type>
				<name>{1}</name>
				<description>{2}</description>
				<labels>{3}</labels>
				<estimate>{4}</estimate>
			</story>',
			$input['type'],
			xml::encode($input['title']),
			xml::encode($input['description']),
			xml::encode($input['labels']),
			$input['estimate']
		);

		$response = $this->_send_command(
			'POST',
			str::format(
				'projects/{0}/stories',
				$input['project']
			),
			$data
		);	
		
		return (int)$response->id;
	}

	/**
	 * Get Project
	 *
	 * Gets an existing project from the Pivotal Tracker installation
	 * and returns it. The resulting project object has various 
	 * properties such as the id, the name etc. 
	 */
	public function get_project($project_id)
	{
		$response = $this->_send_command('GET', "projects/$project_id");

		$p = obj::create();
		$p->id = (int)$response->id;
		$p->name = (string)$response->name;
		$p->point_scale = (string)$response->point_scale;

		return $p;
	}

	/**
	 * Get Projects
	 *
	 * Gets a list of all projects accessible from the user whose
	 * credentials were given to the constructor.
	 */
	public function get_projects()
	{
		$response = $this->_send_command('GET', 'projects');
		$result = array();

		foreach ($response as $project)
		{
			$p = obj::create();
			$p->id = (int)$project->id;
			$p->name = (string)$project->name;
			$p->point_scale = (string)$project->point_scale;
			$result[] = $p;
		}

		return $result;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new PivotalTrackerException($message);
	}
}

class PivotalTrackerException extends Exception
{
}
