<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Rally Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Rally. Please see
 * http://docs.gurock.com/testrail-integration/defects-plugins for
 * more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class Rally_defect_plugin extends Defect_plugin
{
	private $_api;

	private $_address;
	private $_user;
	private $_password;

	private $_field_defaults = array(
		'summary' => array(
			'type' => 'string',
			'label' => 'Summary',
			'size' => 'full',
			'required' => true
		),
		'project' => array(
			'type' => 'dropdown',
			'label' => 'Project',
			'required' => true,
			'remember' => true,
			'cascading' => true,
			'size' => 'compact',
		),
		'state' => array(
			'type' => 'dropdown',
			'label' => 'State',
			'required' => true,
			'remember' => true,
			'size' => 'compact',
		),
		'schedulestate' => array(
			'type' => 'dropdown',
			'label' => 'Schedule State',
			'required' => true,
			'remember' => true,
			'size' => 'compact',
		),
		'priority' => array(
			'type' => 'dropdown',
			'label' => 'Priority',
			'required' => false,
			'remember' => true,
			'size' => 'compact',
		),
		'severity' => array(
			'type' => 'dropdown',
			'label' => 'Severity',
			'required' => false,
			'remember' => true,
			'size' => 'compact',
		),
		'owner' => array(
			'type' => 'dropdown',
			'label' => 'Owner',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'release' => array(
			'type' => 'dropdown',
			'label' => 'Release',
			'required' => false,
			'remember' => true,
			'depends_on' => 'project',
			'size' => 'compact'
		),
		'environment' => array(
			'type' => 'dropdown',
			'label' => 'Environment',
			'required' => false,
			'remember' => true,
			'size' => 'compact'
		),	
		'foundinbuild' => array(
			'type' => 'string',
			'label' => 'Found In Build',
			'required' => false,
			'remember' => false,
			'size' => 'compact'
		),
		'description' => array(
			'type' => 'text',
			'label' => 'Description',
			'required' => false,
			'rows' => 10
		)
	);

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Rally defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your Rally connection below.
; Note: requires Rally API v1.40 or later.
[connection]
address=https://<your-server>/
user=testrail
password=secret
workspace=Acme
');

	public function get_meta()
	{
		return self::$_meta;
	}
	
	// *********************************************************
	// CONSTRUCT / DESTRUCT
	// *********************************************************
	
	public function __construct()
	{
	}
	
	public function __destruct()
	{
		if ($this->_api)
		{
			try
			{
				$api = $this->_api;
				$this->_api = null;
			}
			catch (Exception $e)
			{
				// Possible exceptions are ignored here.
			}
		}
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************
	
	public function validate_config($config)
	{
		$ini = ini::parse($config);
		
		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}

		$keys = array('address', 'user', 'password', 'workspace');
		
		// Check required values for existance
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}

		$address = $ini['connection']['address'];
		
		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];
		$this->_workspace = $ini['connection']['workspace'];
	}
	
	// *********************************************************
	// API / CONNECTION
	// *********************************************************
	
	private function _get_api()
	{
		if ($this->_api)
		{
			return $this->_api;
		}

		$this->_api = new Rally_api(
			$this->_address,
			$this->_user,
			$this->_password
		);
	
		return $this->_api;
	}
	
	// *********************************************************
	// PUSH
	// *********************************************************
		
	public function prepare_push($context)
	{
		return array('fields' => $this->_field_defaults);
	}

	private function _get_summary_default($context)
	{
		$test = current($context['tests']);
		$summary = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$summary .= ' (+others)';
		}

		return $summary;
	}

	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	private function _to_id_name_lookup($items)
	{
		$result = array();
		foreach ($items as $item)
		{
			$result[$item->id] = $item->name;
		}
		return $result;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// Rally installation.	
		if ($field == 'summary' || $field == 'description')
		{
			switch ($field)
			{
				case 'summary':
					$data['default'] = $this->_get_summary_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;
			}
		
			return $data;
		}
		
		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}
		
		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'release':
				$data['default'] = arr::get($prefs, 'release');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_releases(
						$this->_get_workspace_or_fail($api),
						$input['project']
					)
				);
				break;

			case 'environment':
				$data['default'] = arr::get($prefs, 'environment');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_environments(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;

			case 'owner':
				$data['default'] = arr::get($prefs, 'owner');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_editors($input['project'])
				);
				break;

			case 'priority':
				$data['default'] = arr::get($prefs, 'priority');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_priorities(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;

			case 'project':
				$data['default'] = arr::get($prefs, 'project');
				$data['options'] =  $this->_to_id_name_lookup(
					$api->get_projects(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;

			case 'schedulestate':
				$data['default'] = arr::get($prefs, 'schedulestate');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_schedulestates(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;

			case 'severity':
				$data['default'] = arr::get($prefs, 'severity');
				$data['options'] =  $this->_to_id_name_lookup(
					$api->get_severities(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;

			case 'state':
				$data['default'] = arr::get($prefs, 'state');
				$data['options'] = $this->_to_id_name_lookup(
					$api->get_states(
						$this->_get_workspace_or_fail($api)
					)
				);
				break;
		}

		return $data;
	}

	private function _get_workspace_or_fail($api)
	{
		$workspace = $api->get_workspace($this->_workspace);
		if (!$workspace)
		{
			ex::raisef(
				'RallyException',
				'Workspace "{0}" not found. ' . 
				'Please enter a valid Rally workspace.',
				$this->_workspace
			);
		}

		return $workspace;
	}
	
	public function validate_push($context, $input)
	{
	}
	
	public function push($context, $input)
	{
		$api = $this->_get_api();		
		return $api->add_issue($input);
	}
	
	// *********************************************************
	// LOOKUP
	// *********************************************************
	
	public function lookup($defect_id)
	{
		$api = $this->_get_api();
		$issue = $api->get_issue($defect_id);

		// Add some important attributes for the issue such as the
		// issue type, current status and project. Note that the
		// attribute values (and description) support HTML and we
		// thus need to escape possible HTML characters (with 'h')
		// in this plugin.

		$attributes = array();
		if (isset($issue->Workspace))
		{
			$attributes['Workspace'] = 
				h($issue->Workspace->_refObjectName);
		}
		else
		{
			$attributes['Workspace'] = "&ndash;";
		}

		if (isset($issue->_refObjectName))
		{
			$project_url = $issue->Project->_ref;
			$project_id = preg_replace(
				'/.*\/([0-9]{1,})\.js$/', '\1',
				$project_url
			);

			// Add a link to the project.
			$attributes['Project'] = str::format(
				'<a target="_blank" href="{0}#/{1}/dashboard">{2}</a>',
				a($this->_address),
				a($project_id),
				h($issue->Project->_refObjectName)
			);
		}

		$status = '';
		if (isset($issue->State))
		{
			$attributes['Status'] = h($issue->State);
			$status = $issue->State;
		}
		
		if (isset($issue->ScheduleState))
		{
			$attributes['Schedule State'] = h($issue->ScheduleState);
		}
		else
		{
			$attributes['Schedule State'] = "&ndash;";
		}

		if (isset($issue->Priority))
		{
			$attributes['Priority'] = h($issue->Priority);
		}
		else
		{
			$attributes['Priority'] = "&ndash;";
		}

		if (isset($issue->Severity))
		{
			$attributes['Severity'] = h($issue->Severity);
		}
		else
		{
			$attributes['Severity'] = "&ndash;";
		}

		if (isset($issue->Owner->_refObjectName))
		{
			$attributes['Owner'] = 
				h($issue->Owner->_refObjectName);
		}
		else
		{
			$attributes['Owner'] = "&ndash;";
		}

		if (isset($issue->Environment))
		{
			$attributes['Environment'] = h($issue->Environment);
		}
		else
		{
			$attributes['Environment'] = "&ndash;";
		}

		if (isset($issue->FoundInBuild))
		{
			$attributes['Found In Build'] = h($issue->FoundInBuild);
		}
		else
		{
			$attributes['Found In Build'] = "&ndash;";
		}

		// Decide which status to return to TestRail based on the
		// resolution property of the issue (whether the issue was
		// resolved or not). The issue or statuses don't have any
		// additional meta information so that's unfortunately the
		// only distinction we can make for the status.
		$status_id = GI_DEFECTS_STATUS_OPEN;

		if (isset($issue->state))
		{
			if ($issue->state == 'Fixed')
			{
				$status_id = GI_DEFECTS_STATUS_RESOLVED;
			}

			if ($issue->state == 'Closed')
			{
				$status_id = GI_DEFECTS_STATUS_CLOSED;
			}
		}

		// Format the description of the issue (we use a monospace
		// font).
		if (isset($issue->Description))
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				$issue->Description
			);
		}
		else
		{
			$description = null;
		}

		return array(
			'id' => $defect_id,
			'url' => str::format(
				'{0}#/detail/defect/{1}',
				$this->_address,
				$issue->ObjectID
			),
			'title' => $issue->_refObjectName,
			'status_id' => $status_id,
			'status' => $status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

/**
 * Rally REST API
 *
 * Wrapper class for the Rally API with functions for retrieving
 * projects, getting and adding issues etc.
 */
class Rally_api
{
	private $_address;
	private $_curl;
	private $_user;
	private $_password;
	private $_workspace;

	private $_environment= array();
	private $_priorities = array();
	private $_project_object = array();
	private $_schedulestates = array();
	private $_severities = array();
	private $_states = array();

	/**
	 * Construct
	 *
	 * Initializes a new Rally API object. Expects the web address of
	 * the Rally installation including http or https prefix.
	 */	
	public function __construct($address, $user, $password)
	{
		$this->_address = str::slash($address);
		$this->_user = $user;
		$this->_password = $password;
	}
	
	/**
	 * Helper function to sort any array by name that contains objects
	 * with ID and name.
	 */	
	private function _sort_names($a, $b)
	{
		return strcmp($a->name, $b->name);
	}

	/**
	 * Get Projects
	 *
	 * Returns a list of projects for the Rally installation. The
	 * projects are returned as array of objects, each with its ID
	 * and name.
	 */	
	public function get_projects($workspace)
	{
		$response = $this->_send_command(
			'GET',
			'project.js',
			"?fetch=true&workspace=$workspace"
		);

		if (!$response)
		{
			return array();
		}

		// We need to get the user here to compare it to the editors
		// list of the project.
		$user = $this->_get_user($this->_workspace, $this->_user);

		$result = array();
		foreach ($response as $project)
		{
			foreach ($project->Editors as $editor)
			{
				// We only add those projects to the result set where
				// the current user is an editor.
				if ($editor->_ref == $user)
				{
					$p = obj::create();
					$p->id = (string) $project->_ref;
					$p->name = (string) $project->_refObjectName;
					$result[] = $p;
				}
			}
		}

		return $result;
	}

	/**
	 * Get Workspace
	 *
	 * Returns the workspace reference (ID) for the requested workspace
	 * name or null if no workspace can be found with the given name.
	 */	
	public function get_workspace($workspace)
	{
		if ($this->_workspace)
		{
			return $this->_workspace;
		}

		$response = $this->_send_command(
			'GET',
			'workspace.js',
			str::format(
				"&query=(Name%20%3D%20{0})",
				urlencode('"' . $workspace . '"')
			)
		);

		if (!$response)
		{
			return null;
		}
		
		$this->_workspace = $response[0]->_ref;
		return $this->_workspace;
	}

	/**
	 * Load Type Definitions
	 *
	 * Loads and caches the type definitions for the workspace. The
	 * type definitions are used to read the environments, priorities,
	 * states, etc.
	 */
	private function _load_typedefs($workspace)
	{
		$response = $this->_send_command(
			'GET',
			'typedefinition.js',
			"&workspace=$workspace"
		);
		
		if (!$response)
		{
			return;
		}

		foreach ($response as $typedefinition)
		{
			if ($typedefinition->_refObjectName != "Defect")
			{
				continue;
			}

			$typedefs = $this->_send_command(
				'GET',
				$typedefinition->_ref
			);

			if (!isset($typedefs->TypeDefinition->Attributes))
			{
				continue;
			}

			$attributes = $typedefs->TypeDefinition->Attributes;
			foreach ($attributes as $attribute)
			{
				if ($attribute->_refObjectName != 'Environment' &&
					$attribute->_refObjectName != 'Priority' &&
					$attribute->_refObjectName != 'Schedule State' &&
					$attribute->_refObjectName != 'Severity' &&
					$attribute->_refObjectName != 'State'
					)
				{
					continue;
				}

				$results = array();
				foreach ($attribute->AllowedValues as $value)
				{
					if (!$value->StringValue)
					{
						continue;
					}

					$o = obj::create();
					$o->id = (string) $value->StringValue;
					$o->name = (string) $value->StringValue;
					$results[] = $o;
				}

				switch ($attribute->_refObjectName)
				{
					case 'Environment':
						$this->_environment = $results;
						break;

					case 'Priority':
						$this->_priorities = $results;
						break;

					case 'Schedule State':
						$this->_schedulestates = $results;
						break;

					case 'Severity':
						$this->_severities = $results;
						break;

					case 'State':
						$this->_states = $results;
						break;
				}
			}
		}
	}

	/**
	 * Get Priorities
	 *
	 * Returns a list of priorities. Priorities are returned as array
	 * of objects, each with its ID and name.
	 */
	public function get_priorities($workspace)
	{
		if (!$this->_priorities)
		{
			$this->_load_typedefs($workspace);
		}

		return $this->_priorities;
	}

	/**
	 * Get Severities
	 *
	 * Returns a list of severities. Severities are returned as array
	 * of objects, each with its ID and name.
	 */
	public function get_severities($workspace)
	{
		if (!$this->_severities)
		{
			$this->_load_typedefs($workspace);
		}

		return $this->_severities;
	}

	/**
	 * Get States
	 *
	 * Returns a list of states. States are returned as array of
	 * objects, each with its ID and name.
	 */
	public function get_states($workspace)
	{
		if (!$this->_states)
		{
			$this->_load_typedefs($workspace);
		}

		return $this->_states;
	}

	/**
	 * Get Schedule States
	 *
	 * Returns a list of schedule states. Schedule States are returned
	 * as array of objects, each with its ID and name.
	 */
	public function get_schedulestates($workspace)
	{
		if (!$this->_schedulestates)
		{
			$this->_load_typedefs($workspace);
		}

		return $this->_schedulestates;
	}

	/**
	 * Get Environments
	 *
	 * Returns a list of environments. Environments are returned as
	 * array of objects, each with its ID and name.
	 */
	public function get_environments($workspace)
	{
		if (!$this->_environment)
		{
			$this->_load_typedefs($workspace);
		}

		return $this->_environment;
	}

	/**
	 * Get Releases
	 *
	 * Returns a list of releases. Releases are returned as array
	 * of objects, each with its ID and name.
	 */
	public function get_releases($workspace, $project)
	{
		$response = $this->_send_command(
			'GET',
			'release.js',
			"&workspace=$workspace&project=$project" . 
				'&projectScopeDown=false&projectScopeUp=false'
		);

		if (!$response)
		{
			return array();
		}

		$releases = array();
		foreach ($response as $release)
		{
			$r = obj::create();
			$r->id = (string) $release->_ref;
			$r->name = (string) $release->_refObjectName;
			$releases[] = $r;
		}

		return $releases;
	}

	/**
	 * Get Project Object
	 *
	 * Loads and caches the object for the given project key.
	 */
	private function _get_project_object($project_key)
	{
		if ($this->_project_object)
		{
			return $this->_project_object;
		}

		$this->_project_object = $this->_send_command(
			'GET',
			$project_key
		);

		return $this->_project_object;
	}

	/** 
	 * Get editors
	 * 
	 * Return a list of editors for the given project. Editors are
	 * returned as array of objects, each with its ID and name.
	 */
	public function get_editors($project_key)
	{
		$project_obj = $this->_get_project_object($project_key);
		if (!isset($project_obj->Project->Editors))
		{
			return array();
		}

		$result = array();
		foreach ($project_obj->Project->Editors as $editor)
		{
			$e = obj::create();
			$e->id = (string) $editor->_ref;
			$e->name = (string) $editor->_refObjectName;
			$result[] = $e;
		}
		
		// Sort objects/users in results array by their names.
		usort($result, array($this, '_sort_names'));
		return $result;
	}

	/**
	 * Get Issue
	 *
	 * Gets an existing case from the Rally installation and returns
	 * it. The resulting issue object has various properties such
	 * as the summary, description, project etc.
	 */	 
	public function get_issue($issue_id)
	{
		$issue = $this->_send_command(
			'GET',
			'defect.js',
			"&query=(FormattedId%20%3D%20$issue_id)"
		);

		if (!$issue)
		{
			$this->_throw_error(
				'Issue "{0}" not found.',
				$issue_id
			);
		}

		$response = $this->_send_command(
			'GET',
			$issue[0]->_ref
		);

		if (!isset($response->Defect))
		{
			$this->_throw_error(
				'Issue "{0}" not found.',
				$issue_id
			);
		}

		return $response->Defect;
	}

	/**
	 * Get User
	 *
	 * Querys the workspace for a user by email address and returns
	 * its reference.
	 */	 
	private function _get_user($workspace, $email)
	{
		$response = $this->_send_command(
			'GET',
			'user.js',
			"&workspace=$workspace&query=(EmailAddress%20%3D%20$email)"
		);

		if (!$response)
		{
			return array();
		}

		return $response[0]->_ref;
	}

	private function _send_command($method, $uri, $options = '',
		$data = null)
	{
		if (!str::starts_with($options, '?'))
		{
			$options = '?fetch=false' . $options;
		}

		$r = $this->_send_command_batch(
			$method,
			$uri,
			$options,
			1,
			$data
		);
		
		// If QueryResult->Result is set we need to check if we need
		// to fetch more elements.
		if (isset($r->QueryResult->Results))
		{
			$result = $r->QueryResult->Results;

			$count = $r->QueryResult->TotalResultCount;
			$index = $r->QueryResult->StartIndex;
			$pagesize = $r->QueryResult->PageSize;

			while ($index + $pagesize < $count)
			{
				$r = $this->_send_command_batch(
					$method,
					$uri,
					$options,
					$index + $pagesize,
					$data
				);

				$count = $r->QueryResult->TotalResultCount;
				$index = $r->QueryResult->StartIndex;
				$pagesize = $r->QueryResult->PageSize;

				$result = array_merge(
					$result,
					$r->QueryResult->Results
				);
			}
		}
		else
		{
			$result = $r;
		}

		return $result;
	}

	private function _send_command_batch($method, $uri, $options,
		$start, $data)
	{
		if (str::starts_with($uri, 'https://') ||
			str::starts_with($uri, 'http://'))
		{
			$url = $uri;
		}
		else
		{
			$url = $this->_address .
				'slm/webservice/1.40/' .
				$uri .
				$options .
				"&start=$start&pagesize=100";
		}

		return $this->_send_request($method, $url, $data);
	}
	
	private function _send_request($method, $url, $data = null)
	{
		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Issuing Rally HTTP request');
			logger::debugr(
				'$request',
				array(
					'method' => $method,
					'url' => $url,
					'data' => $data
				)
			);
		}
		
		$response = http::request_ex(
			$this->_curl,
			$method, 
			$url, 
			array(
				'user' => $this->_user,
				'password' => $this->_password,
				'data' => json::encode($data),
				'headers' => array(
					'Content-Type' => 'application/json',
					'X-RallyIntegrationName' => 'TestRail',
					'X-RallyIntegrationVendor' => 'Gurock',
					'X-RallyIntegrationVersion' => '1',
					'X-RallyIntegrationOS' =>
						php_uname('s'),
					'X-RallyIntegrationPlatform' =>
						str::format(
							'PHP {0}',
							phpversion()
						),
					'X-RallyIntegrationLibrary' =>
						'Rally Defect Plugin, REST API'
				)
			)
		);

		// In case debug logging is enabled, we append the data
		// we've sent and the entire request/response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debug('Got the following response');
			logger::debugr('$response', $response);
		}

		$obj = json::decode($response->content);

		if ($response->code != 200)
		{
			$this->_throw_error(
				'Invalid HTTP code ({0}). Please check your user/' .
				'password for Rally.',
				$response->code
			);
		}

		return $obj;
	}

	/**
	 * Add Issue
	 *
	 * Adds a new issue to the Rally installation with the given
	 * parameters (title, project etc.) and returns its ID. The
	 * parameters must be named according to the Rally API format,
	 * e.g.:
	 *
	 * summary:     The summary of the new issue
	 * project:     The ID of the project the issue should be added
	 *              to
	 * priority:    The ID of the priority the issue should be added
	 *              to
	 * description: The description of the new issue
	 */	
	public function add_issue($options)
	{
		$fields = array();

		foreach ($options as $field_name => $field_value)
		{
			if (!$field_value)
			{
				continue;
			}

			$field = $this->_format_field(
				$field_name,
				$field_value);

			if (isset($field['name']) && isset($field['value']))
			{
				$fields[$field['name']] = $field['value'];
			}
		}

		$fields['SubmittedBy'] = $this->_get_user(
			$this->_workspace,
			$this->_user
		);

		$response = $this->_send_command_batch(
			'POST',
			'defect/create.js',
			'?fetch=true',
			1,
			array('Defect' => $fields)
		);

		// Check if the issue/defect was created successfully. The API
		// appears to return 200 in all cases and doesn't use proper
		// HTTP status codes.
		if (isset($response->CreateResult->Errors))
		{
			$errors = $response->CreateResult->Errors;
			if (is_array($errors) && $errors)
			{
				$this->_throw_error($errors[0]);
			}
		}

		if (isset($response->CreateResult->Object))
		{
			return $response->CreateResult->Object->FormattedID;
		}
		else 
		{
			return null;
		}
	}

	private function _format_field($field_name, $field_value)
	{
		$data = array();
		$data['name'] = $field_name;

		switch ($field_name)
		{
			case 'project':
				$data['value'] = array('ref' => $field_value);
				break;

			case 'summary':
				$data['name'] = 'name';
				$data['value'] = $field_value;
				break;

			case 'description':
				$data['name'] = 'description';
				$data['value'] = 
					nl2br(
						html::link_urls(
							$field_value
						)
					);
				break;

			default:
				$data['value'] = $field_value;
				break;
		}

		return $data;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new RallyException($message);
	}
}

class RallyException extends Exception
{
}
