<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

/**
 * Lighthouse Defect Plugin for TestRail
 *
 * Copyright Gurock Software GmbH. All rights reserved.
 *
 * This is the TestRail defect plugin for Lighthouse. Please
 * see http://docs.gurock.com/testrail-integration/defects-plugins
 * for more information about TestRail's defect plugins.
 *
 * http://www.gurock.com/testrail/
 */

class Lighthouse_defect_plugin extends Defect_plugin
{
	private $_api;

	private $_address;
	private $_user;
	private $_password;
	private $_project_id;

	private static $_meta = array(
		'author' => 'Gurock Software',
		'version' => '1.0',
		'description' => 'Lighthouse defect plugin for TestRail',
		'can_push' => true,
		'can_lookup' => true,
		'default_config' => 
			'; Please configure your Lighthouse connection below
[connection]
address=https://<your-instance>.lighthouseapp.com/
user=testrail
password=secret
project_id=<lighthouse-project-id>'
	);

	public function get_meta()
	{
		return self::$_meta;
	}

	// *********************************************************
	// CONFIGURATION
	// *********************************************************

	public function validate_config($config)
	{
		$ini = ini::parse($config);

		if (!isset($ini['connection']))
		{
			throw new ValidationException('Missing [connection] group');
		}

		$keys = array('address', 'user', 'password', 'project_id');

		// Check required values for existence
		foreach ($keys as $key)
		{
			if (!isset($ini['connection'][$key]) ||
				!$ini['connection'][$key])
			{
				throw new ValidationException(
					"Missing configuration for key '$key'"
				);
			}
		}

		$address = $ini['connection']['address'];

		// Check whether the address is a valid url (syntax only)
		if (!check::url($address))
		{
			throw new ValidationException('Address is not a valid url');
		}

		$project_id = $ini['connection']['project_id'];

		// Check whether the project ID is a natural number
		if (!check::natural($project_id))
		{
			throw new ValidationException(
				"The 'project_id' value is needed to relate a ticket to
				the correct project and must be a numeric ID"
			);
		}
	}

	public function configure($config)
	{
		$ini = ini::parse($config);
		$this->_address = str::slash($ini['connection']['address']);
		$this->_user = $ini['connection']['user'];
		$this->_password = $ini['connection']['password'];		
		$this->_project_id = $ini['connection']['project_id'];
	}

	// *********************************************************
	// API / CONNECTION
	// *********************************************************

	private function _get_api()
	{
		if (!$this->_api)
		{
			$this->_api = new Lighthouse_api(
				$this->_address,
				$this->_user,
				$this->_password
			);
		}

		return $this->_api;
	}

	// *********************************************************
	// PUSH
	// *********************************************************

	public function prepare_push($context)
	{
		// Return a form with the following fields/properties
		return array(
			'fields' => array(
				'title' => array(
					'type' => 'string',
					'label' => 'Title',
					'required' => true,
					'size' => 'full'
				),
				'state' => array(
					'type' => 'dropdown',
					'label' => 'Ticket State',
					'required' => true,
					'size' => 'compact'
				),
				'assignee' => array(
					'type' => 'dropdown',
					'label' => "Who's responsible",
					'remember' => true,
					'required' => false,
					'size' => 'compact'
				),
				'milestone' => array(
					'type' => 'dropdown',
					'label' => 'Milestone',
					'remember' => true,
					'required' => false,
					'size' => 'compact'
				),
				'tags' => array(
					'type' => 'string',
					'label' => 'Tags',
					'size' => 'full',
					'description' => 'A comma separated list of tags.'
				),
				'description' => array(
					'type' => 'text',
					'label' => 'Description',
					'rows' => 10
				)
			)
		);
	}

	private function _get_title_default($context)
	{
		$test = current($context['tests']);
		$title = 'Failed test: ' . $test->case->title;
		
		if ($context['test_count'] > 1)
		{
			$title .= ' (+others)';
		}
		
		return $title;
	}
	
	private function _get_description_default($context)
	{
		return $context['test_change']->description;
	}

	public function prepare_field($context, $input, $field)
	{
		$data = array();

		// Process those fields that do not need a connection to the
		// Lighthouse installation.		
		if ($field == 'title' || $field == 'description')
		{
			switch ($field)
			{
				case 'title':
					$data['default'] = $this->_get_title_default(
						$context);
					break;
					
				case 'description':
					$data['default'] = $this->_get_description_default(
						$context);
					break;
			}

			return $data;
		}

		// Take into account the preferences of the user, but only
		// for the initial form rendering (not for dynamic loads).
		if ($context['event'] == 'prepare')
		{
			$prefs = arr::get($context, 'preferences');
		}
		else
		{
			$prefs = null;
		}

		// And then try to connect/login (in case we haven't set up a
		// working connection previously in this request) and process
		// the remaining fields.
		$api = $this->_get_api();

		switch ($field)
		{
			case 'assignee':
				$data['default'] = arr::get($prefs, 'assignee');
				$data['options'] =
					$api->get_project_memberships($this->_project_id);
				break;

			case 'milestone':
				$data['default'] = arr::get($prefs, 'milestone');
				$data['options'] =
					$api->get_milestones($this->_project_id);
				break;

			case 'state':
				$states = $api->get_states($this->_project_id);
				if ($states)
				{
					$data['options'] = $states;				
					$data['default'] = current($states);
				}
				break;
		}

		return $data;
	}

	public function validate_push($context, $input)
	{
	}

	public function push($context, $input)
	{
		$api = $this->_get_api();

		return $api->add_ticket(
			$this->_project_id, 
			$input['title'],
			$input['description'],
			$input['tags'],
			$input['assignee'],
			$input['milestone'],
			$input['state']
		);
	}

	// *********************************************************
	// LOOKUP
	// *********************************************************

	public function lookup($id)
	{
		$api = $this->_get_api();

		$ticket = $api->get_ticket($this->_project_id, $id);
		$project = $api->get_project($this->_project_id);

		// Determine the ticket status based on the open/closed states
		// of the project.
		$status_id = GI_DEFECTS_STATUS_CLOSED;

		if (arr::exists($project->open_states, $ticket->status))
		{
			$status_id = GI_DEFECTS_STATUS_OPEN;
		}

		// Format the description of the ticket (we use a monospace
		// font). Since Lighthouses supports markdown for formatting
		// the description, we format is as such.
		if (isset($ticket->description) && $ticket->description)
		{
			$description = str::format(
				'<div class="monospace">{0}</div>',
				markdown::to_html(
					$ticket->description
				)
			);
		}
		else
		{
			$description = null;
		}

		// Add some important attributes for the ticket such as the
		// project, status etc. Note that the attribute values
		// (and description) support HTML and we thus need to escape
		// possible HTML characters (with 'h') in this plugin.

		$attributes = array();
		$attributes['Ticket State'] = h($ticket->status);

		$attributes['Project'] = str::format(
			'<a target="_blank" href="{0}projects/{1}">{2}</a>',
			a($this->_address),
			a($ticket->project_id),
			h($project->name)					
		);

		if ($ticket->milestone_title)
		{
			$attributes['Milestone'] = str::format(
				'<a target="_blank" href="{0}projects/{1}/milestones/{2}">{3}</a>',
				a($this->_address),
				a($this->_project_id),
				a($ticket->milestone_id),
				h($ticket->milestone_title)
			);
		}
		else
		{
			$attributes['Milestone'] = 'None';
		}

		if ($ticket->assignee)
		{
			$attributes["Who's responsible"] = h($ticket->assignee);
		}
		else
		{
			$attributes["Who's responsible"] = 'None';
		}

		$attributes['Tags'] = h(str::join($ticket->tags, ', '));

		return array(
			'id' => $id,
			'url' => str::format(
				'{0}projects/{1}/tickets/{2}',
				$this->_address,
				$this->_project_id,
				$id
			),
			'title' => $ticket->title,
			'status_id' => $status_id,
			'status' => $ticket->status,
			'description' => $description,
			'attributes' => $attributes
		);
	}
}

class Lighthouse_api
{
	private $_address;
	private $_user;
	private $_password;
	private $_curl;

	/**
	 * Construct
	 *
	 * Initializes a new Lighthouse API object. Expects the address of
	 * the Lighthouse installation including https prefix, the user
	 * name and password.
	 */
	public function __construct($address, $user, $password)
	{
		$this->_address = str::slash($address);
		$this->_user = $user;
		$this->_password = $password;
	}

	private function _throw_error($format, $params = null)
	{
		$args = func_get_args();
		$format = array_shift($args);
		
		if (count($args) > 0)
		{
			$message = str::formatv($format, $args);
		}
		else 
		{
			$message = $format;
		}
		
		throw new LighthouseException($message);
	}

	private function _send_command($method, $uri, $data = null)
	{
		$url = $this->_address . $uri;

		$options = array(
			'headers' => array(
				'Content-Type' => 'application/xml'
			),
			'user' => $this->_user,
			'password' => $this->_password,
			'data' => $data
		);

		if (!$this->_curl)
		{
			// Initialize the cURL handle. We re-use this handle to
			// make use of Keep-Alive, if possible.
			$this->_curl = http::open();
		}

		$response = http::request_ex(
			$this->_curl,
			$method,
			$url,
			$options
		);

		// In case debug logging is enabled, we append the request
		// data and the response to the log.
		if (logger::is_on(GI_LOG_LEVEL_DEBUG))
		{
			logger::debugr(
				'$rest',
				array(
					'url' => $url,
					'options' => $options,
					'response' => $response
				)
			);
		}

		if ($response->code == 404)
		{
			$this->_throw_error('Resource/ticket not found.');
		}

		if ($response->code == 401)
		{
			$this->_throw_error(
				'Access denied. Please check your Lighthouse user and
				password.'
			);
		}

		$content = xml::parse_string($response->content);

		// Check for additional errors and include the received error
		// message in the exception, if any.
		if ($response->code != 200 && $response->code != 201)
		{
			if (isset($content->error))
			{
				$error = '';
				foreach ($content->error as $e)
				{
					if ($error)
					{
						$error .= "\n";
					}

					$error .= $e;
				}
			}
			else
			{
				$error = (string) $content;
			}

			$this->_throw_error(
				'Invalid HTTP code ({0}). {1}',
				$response->code,
				$error
			);
		}

		return $content;
	}

	/**
	 * Add Ticket
	 *
	 * Adds a new ticket to the Lighthouse installation with the given
	 * parameters and returns its ID.
	 *
	 * project_id 		The ID of the project that the new ticket is
	 * 					related to.
	 * title 			The title of the new ticket.
	 * description 		The description of the new ticket.
	 * tags 			Comma-separated list of tags for the ticket.
	 * assignee_id		The ID of a user. The new ticket is assigned
	 * 					to the corresponding user.
	 * milestone_id		The ID of a milestone of. The new ticket is
	 * 					related to the corresponding milestone.
	 * state 			A ticket state string that should be 
	 *					set for the new ticket.
	 */
	public function add_ticket($project_id, $title, $description,
		$tags, $assignee_id, $milestone_id, $state)
	{
		$data = str::format(
			'<?xml version="1.0" encoding="UTF-8"?>
			<ticket>
				<title>{0}</title>
				<body>{1}</body>
				<tag>{2}</tag>
				<assigned-user-id>{3}</assigned-user-id>
				<milestone-id>{4}</milestone-id>
				<state>{5}</state>
			</ticket>',
			xml::encode($title),
			xml::encode($description),
			xml::encode($tags),
			xml::encode($assignee_id),
			xml::encode($milestone_id),
			xml::encode($state)
		);

		$response = $this->_send_command(
			'POST',
			str::format(
				'projects/{0}/tickets.xml',
				$project_id
			),
			$data
		);

		return (int)$response->number;
	}

	/**
	 * Get Ticket
	 *
	 * Gets an existing ticket with the given ID from the Lighthouse
	 * installation.
	 */
	public function get_ticket($project_id, $ticket_id)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				'projects/{0}/tickets/{1}.xml',
				$project_id,
				$ticket_id
			)
		);

		$ticket = obj::create();
		$ticket->id = (int)$response->number;
		$ticket->title = (string)$response->title;
		$ticket->description = (string)$response->{'latest-body'};
		$ticket->status = (string)$response->state;
		$ticket->project_id = (int)$response->{'project-id'};
		$ticket->assignee = (string)$response->{'assigned-user-name'};
		$ticket->milestone_title = (string)$response->{'milestone-title'};
		$ticket->milestone_id = (int)$response->{'milestone-id'};

		// Split up the tags string into an array of tags. We have to
		// respect that Lighthouse allows tags including whitespaces
		// by putting them into double quotes.
		$ticket->tags = array();
		if ($response->tag)
		{
			preg_match_all(
				'/\b[^\s]+\b|"[^"]+"/', 
				$response->tag, 
				$matches
			);

			foreach ($matches[0] as $match)
			{
				$ticket->tags[] = trim((string)$match, '"');
			}
		}

		return $ticket;
	}

	/**
	 * Get Project Memberships
	 *
	 * Returns a list of all members associated with the project
	 * specified by the given project ID. 
	 */
	public function get_project_memberships($project_id)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				'projects/{0}/memberships.xml',
				$project_id
			)
		);
		
		$members = array();

		if (isset($response->membership))
		{
			foreach ($response->membership as $member)
			{
				if (!isset($member->{'user-id'}) ||
					!isset($member->user) ||
					!isset($member->user->name))
				{
					continue;
				}

				$members[(int)$member->{'user-id'}] = 
					(string)$member->user->name;
			}
		}

		return $members;
	}

	/**
	 * Get Milestones
	 *
	 * Returns a list of all milestones that exist for the project
	 * specified by the given project ID.
	 */
	public function get_milestones($project_id)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				'projects/{0}/milestones.xml',
				$project_id
			)
		);

		$milestones = array();

		if (isset($response->milestone))
		{
			foreach ($response->milestone as $milestone)
			{
				if (!isset($milestone->id) ||
					!isset($milestone->title))
				{
					continue;
				}

				$milestones[(int)$milestone->id] =
					(string)$milestone->title;
			}
		}

		return $milestones;
	}

	/**
	 * Get Project
	 *
	 * Gets an existing project with the given ID from the Lighthouse
	 * installation.
	 */
	public function get_project($project_id)
	{
		$response = $this->_send_command(
			'GET',
			str::format(
				'projects/{0}.xml',
				$project_id
			)
		);

		$project = obj::create();
		$project->id = $response->id;
		$project->name = $response->name;
		$project->open_states = str::split(
			$response->{'open-states-list'},
			','
		);
		$project->closed_states = str::split(
			$response->{'closed-states-list'},
			','
		);

		return $project;	
	}

	/**
	 * Get States
	 *
	 * Gets the available states for the project with the given ID
	 * from the Lighthouse installation.
	 */
	public function get_states($project_id)
	{
		$project = $this->get_project($project_id);

		$states = array();
		foreach ($project->open_states as $state)
		{
			$states[$state] = $state;
		}

		foreach ($project->closed_states as $state)
		{
			$states[$state] = $state;
		}

		return $states;
	}
}

class LighthouseException extends Exception
{
}
