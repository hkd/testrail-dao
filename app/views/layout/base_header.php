<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
header('Content-Type: text/html; charset=UTF-8');
header('Expires: Sat, 01 Jan 2000 00:00:00 GMT');
header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');

// Set application style if no other style has been specified
if (!isset($page_css))
{
	$page_css = 'app';
}

// Are we allowed to use optimized/combined versions of our css files?
// Note that these settings are not defined when no config.php is
// present (e.g. during the installation) and we therefore default to
// true.
$optimize_css_include_build = true;

if (defined('DEPLOY_DEVELOP') && DEPLOY_DEVELOP)
{
	$optimize_css = false;
}
else
{
	$optimize_css = !defined('DEPLOY_OPTIMIZE_CSS') || DEPLOY_OPTIMIZE_CSS;
	if (defined('DEPLOY_HOSTED') && DEPLOY_HOSTED)
	{
		$optimize_css_include_build = false;
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo  isset($page_title) ? h($page_title) . ' - ' : '' ?>TestRail</title>

<?php if ($optimize_css): ?>
	<?php if ($optimize_css_include_build): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo  r("css/$page_css-combined.css?3735") ?>" 
			media="all" />
	<?php else: ?>
		<link type="text/css" rel="stylesheet" href="<?php echo  r("css/$page_css-combined.css") ?>" 
			media="all" />
	<?php endif ?>
<?php else: ?>
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/reset.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/base.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/ui.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/tree.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/colorpicker.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/fancybox.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/chosen.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/controls.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/charts.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/forms.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/dialogs.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/markdown.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r('css/dropzone.css') ?>" media="all" />
	<link type="text/css" rel="stylesheet" href="<?php echo  r("css/$page_css.css") ?>" media="all" />
<?php endif ?>

<?php if (isset($page_print_css)): ?>
	<?php if ($optimize_css_include_build): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo  r("css/print/$page_print_css.css?3735") ?>"
			media="print" />
	<?php else: ?>
		<link type="text/css" rel="stylesheet" href="<?php echo  r("css/print/$page_print_css.css") ?>"
			media="print" />
	<?php endif ?>
<?php endif ?>

<link rel="shortcut icon" href="<?php echo  r('images/favicon.ico') ?>"/>

<?php
// Note: The only JS file that is included in the head is jQuery. This
// is for $(document).ready which is used throughout the application
// for inline JS sections (to kick off some actions, depending on the
// individual pages). Additional JS files are loaded right before the
// </body> tag in base_footer.php for performance reasons.
//
// Also see:
// http://developer.yahoo.com/blogs/ydn/posts/2007/07/high_performanc_5/
//
// Note: when updating jQuery to an incompatible version, we also need
// to add the build number to the following include.
?>

<script type="text/javascript" src="<?php echo  r('js/jquery.js') ?>"></script>
</head>
<?php if (isset($page_min_threshold)): ?>
	<?php if (!isset($page_min_width) || $page_min_threshold > $page_min_width): ?>
		<?php $page_min_width = $page_min_threshold ?>
	<?php endif ?>
<?php endif ?>
<?php if (isset($page_min_width)): ?>
<body style="min-width: <?php echo  $page_min_width ?>px">
<?php else: ?>
<body>
<?php endif ?>