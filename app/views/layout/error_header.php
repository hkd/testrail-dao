<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$page_css = 'error';
include APPPATH . 'views/layout/base_header.php';
?>

<div id="header-container">
	<div id="header">
		<div class="logo">
			<img src="<?php echo  r('images/layout/logo.png') ?>" width="160" height="50" alt="" />
		</div>
		<div class="support">
			<div class="support-inner">
				Got any questions or need help?<br />
				<a href="http://www.gurock.com/support/" target="_blank">Contact Gurock Software support</a>
			</div>
		</div>
	</div>
</div>

<div id="content-container">
	<div id="content">
