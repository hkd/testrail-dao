<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php $project = projects::get_current() ?>
<?php
// Execute the deferred query and group the case assocs by the cases
// for easy and fast lookup
$case_assocs = group::by_id($case_assocs->result(), 'case_id');
?>
<?php $section_hierarchies = array() ?>
<?php $section = $sections->peek() ?>
<?php while ($section): ?>
<?php $section_lookup[$section->id] = $section ?>
<?php $sections->next() ?>
<?php $case = $cases->peek() ?>
<?php while ($case && $case->section_id == $section->id): ?>
<?php $cases->next() ?>
<?php if (isset($case_assocs[$case->id])): ?>
<?php $current_case_assocs = group::by_id_scalar(
	$case_assocs[$case->id]->items,
	'name',
	'value') ?>
<?php foreach ($current_case_assocs as $key => $value): ?>
<?php $case->$key = $value ?>
<?php endforeach ?>
<?php endif ?>
<?php $milestone = null ?>
<?php if ($case->milestone_id): ?>
<?php $milestone = arr::get($milestone_lookup, $case->milestone_id) ?>
<?php endif ?>
<?php $values = array() ?>
<?php foreach ($columns as $column): ?>
<?php if (!isset($fields_csv[$column])): ?>
<?php continue ?>
<?php endif ?>
<?php
$value = null;
$is_encoded = false;

switch ($column)
{
	case 'cases:id':
		$value = entities::case_id($case->id);
		break;

	case 'cases:title':
		$value = $case->title;
		break;

	case 'cases:template_id':
		$value = $GI->cache->get_scalar(
			'template',
			$case->template_id,
			'name',
			''
		);
		break;		

	case 'cases:type_id':
		$value = $GI->cache->get_scalar(
			'case_type',
			$case->type_id,
			'name',
			''
		);
		break;

	case 'cases:priority_id':
		$value = $GI->cache->get_scalar(
			'priority',
			$case->priority_id,
			'name',
			''
		);
		break;

	case 'cases:milestone_id':
		if (fields::has_for_cases_and_template($project->id, $case->template_id, 'milestone_id'))
		{
			if ($milestone)
			{
				$value = $milestone->name;
			}
		}
		break;

	case 'cases:estimate':
		if (fields::has_for_cases_and_template($project->id, $case->template_id, 'estimate'))
		{
			if ($case->estimate)
			{
				$value = $case->estimate . 's';
			}
		}
		break;

	case 'cases:estimate_forecast':
		if (fields::has_for_cases_and_template($project->id, $case->template_id, 'estimate'))
		{
			if ($case->estimate_forecast)
			{
				$value = $case->estimate_forecast . 's';
			}
		}
		break;

	case 'cases:refs':
		if (fields::has_for_cases_and_template($project->id, $case->template_id, 'refs'))
		{
			if ($case->refs)
			{
				$value = references::format_nolinks($case->refs);
			}
		}
		break;		

	case 'cases:section_id':
		$value = $section->name;
		break;

	case 'cases:section_full':
		// Compute the full section name (as in Section1 > Section 2)
		// on demand and cache the result afterwards.
		$value = arr::get($section_hierarchies, $section->id);
		if (!$value)
		{
			$value = sections::get_name_hierarchy(
				$section,
				$section_lookup,
				$section_hierarchies
			);
		}
		break;

	case 'cases:section_depth':
		$value = $section->depth;
		break;

	case 'cases:section_desc':
		$value = $section->description;
		break;

	case 'cases:suite_id':
		$value = entities::suite_id($suite->id);
		break;

	case 'cases:suite_name':
		$value = $suite->name;
		break;

	case 'cases:created_by':
		$value = $GI->cache->get_scalar(
			'user',
			$case->user_id,
			'name',
			''
		);
		break;

	case 'cases:created_on':
		$value = date::format_short_datetime($case->created_on);
		break;

	case 'cases:updated_by':
		$value = $GI->cache->get_scalar(
			'user',
			$case->updated_by,
			'name',
			''
		);
		break;

	case 'cases:updated_on':
		$value = date::format_short_datetime($case->updated_on);
		break;

	default:
		// We deal with a custom field (or unknown/unsupported column) if
		// we reach this point. We first check if we deal with a sub-field
		// of a custom field (such as cases:custom_steps_separated.step).
		// We split the column into the system name and sub-property in
		// this case (and format the value differently, see below).
		$sub = null;

		$ix = str::pos($column, '.');
		if ($ix !== false)
		{
			$sub = str::sub($column, $ix + 1);
			$column = str::sub($column, 0, $ix);
		}

		$field = arr::get($fields, $column);
		if ($field && $field->can_template($case->template_id))
		{
			if ($sub)
			{
				$value = fields::export_value_partial(
					$field, 
					$case, 
					$sub,
					$exporter
				);
			}
			else 
			{
				$value = fields::export_value(
					$field, 
					$case, 
					$exporter
				);
			}

			$is_encoded = true; // Custom fields do their own encoding
		}
		break;
}

if ($value !== null)
{
	if (!$is_encoded)
	{
		// System fields are encoded/escaped here (replace " with "")
		$value = csv::encode($value);
	}
}
else 
{
	$value = '';
}

$values[] = $value;
?>
<?php endforeach ?>
<?php echo  csv::join($values, $separator) ?>
<?php $case = $cases->peek() ?>
<?php endwhile ?>
<?php $section = $sections->peek() ?>
<?php endwhile ?>