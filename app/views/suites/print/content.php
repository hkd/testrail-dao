<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php if ($project->suite_mode == TP_PROJECTS_SUITES_SINGLE): ?>
<h1><?php echo  lang('pages_cases') ?></h1>
<?php else: ?>
<h1><?php echo  entities::suite_id($suite->id) ?>: <?php echo h( $suite->name ) ?></h1>
<?php endif ?>
<?php if ($suite->description): ?>
	<div class="markdown" style="margin-bottom: 1em"><?php echo  markdown::to_html($suite->description) ?></div>
<?php endif ?>
<?php $case_assocs = group::by_id($case_assocs->result(), 'case_id') ?>
<?php $index = 1 ?>
<?php $break_sections = $format != 'outline' ?>
<?php $section = $sections->peek() ?>
<?php while ($section): ?>
	<?php $sections->next() ?>
	<?php if ($break_sections): ?>
		<div class="page">
	<?php endif ?>
		<?php
		$temp = array();
		$temp['project'] = $project;
		$temp['section'] = $section;
		$temp['sections'] = $sections;
		$temp['cases'] = $cases;
		$temp['case_assocs'] = $case_assocs;
		$temp['format'] = $format;
		$temp['index'] = $index;
		$temp['chapter'] = '';
		$temp['fields'] = $fields;
		$temp['case_fields'] = $case_fields;
		$temp['columns'] = $columns;
		$temp['columns_for_user'] = $columns_for_user;
		$temp['milestone_lookup'] = $milestone_lookup;
		$GI->load->view('suites/print/section', $temp);
		?>	
		<?php $index++ ?>
	<?php if ($break_sections): ?>		
		</div>
	<?php endif ?>
	<?php $section = $sections->peek() ?>
<?php endwhile ?>