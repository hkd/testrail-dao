<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$temp = array();
$temp['run'] = $run;
$temp['milestone'] = $milestone;
$temp['show_dates'] = !isset($show_dates) || $show_dates;
$GI->load->view('runs/print/start', $temp);
?>
<?php $index = 1 ?>
<?php $section = $sections->peek() ?>
<?php
$root = obj::create();
$root->_is_root = true;
$root->_index = 1;
$root->_chapter = '';
$root->_shown = true; // Hidden root node
?>
<?php $test_assocs = group::by_id($test_assocs->result(), 'test_id') ?>
<?php $case_assocs = group::by_id($case_assocs->result(), 'case_id') ?>
<?php while ($section): ?>
	<?php $sections->next() ?>
		<?php
		$temp = array();
		$temp['project'] = $project;
		$temp['parent'] = $root;
		$temp['section'] = $section;
		$temp['sections'] = $sections;
		$temp['tests'] = $tests;
		$temp['test_changes'] = $test_changes;
		$temp['test_assocs'] = $test_assocs;
		$temp['case_assocs'] = $case_assocs;
		$temp['format'] = $format;
		$temp['case_fields'] = $case_fields;
		$temp['test_fields'] = $test_fields;
		$temp['fields'] = $fields;
		$temp['columns'] = $columns;
		$temp['columns_for_user'] = $columns_for_user;
		$temp['milestone_lookup'] = $milestone_lookup;
		$GI->load->view('runs/print/section', $temp);
		?>	
		<?php $index++ ?>
	<?php $section = $sections->peek() ?>
<?php endwhile ?>
