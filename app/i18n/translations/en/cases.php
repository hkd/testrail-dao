<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$lang['cases_add'] = 'Add Test Case';
$lang['cases_add_and_next'] = 'Add & Next';
$lang['cases_edit'] = 'Edit Test Case';
$lang['cases_view'] = 'View Test Case';
$lang['cases_view_short'] = 'View Case';
$lang['cases_edit_short'] = 'Edit Case';

$lang['cases_actions'] = 'Actions';
$lang['cases_delete'] = 'Delete Test Case';
$lang['cases_delete_link'] = 'Delete this test case';
$lang['cases_delete_descr'] = 'Delete a test case to remove it from its test suite. This also deletes all related running tests.';
$lang['cases_delete_confirm'] = 'Really delete this test case? This also deletes all active tests and results for this case and cannot be undone.';
$lang['cases_save'] = 'Save Test Case';
$lang['cases_save_many'] = 'Save Test Cases';

$lang['cases_edit_many_intro_title'] = 'Steps to update multiple cases';
$lang['cases_edit_many_intro_body_1'] = 'Review the cases to update or remove cases from the selection.';
$lang['cases_edit_many_intro_body_2'] = 'Enable the fields you want to update and select or enter their new values.';
$lang['cases_edit_many_intro_body_3'] = 'Save your changes (a dialog with a summary is shown before your changes are applied).';
$lang['cases_edit_all_intro_body_1'] = 'Select a filter if you want to apply the changes to a subset of cases only.';
$lang['cases_edit_all_intro_body_2'] = 'Enable the fields you want to update and select or enter their new values.';
$lang['cases_edit_all_intro_body_3'] = 'Save your changes (a dialog with a summary is shown before your changes are applied).';
$lang['cases_edit_many_filter_reset'] = 'Remove filter to apply the changes to all test cases.';
$lang['cases_edit_many_filter_empty'] = 'The selected filter does not match any test cases.';
$lang['cases_edit_many_cases_empty'] = 'You need to update at least one test case.';
$lang['cases_edit_many_diff_title'] = 'Review Changes';
$lang['cases_edit_many_diff_intro'] = 'The following changes are applied to all selected test cases.
This cannot be undone so please make sure to review the changes carefully.';
$lang['cases_edit_many_diff_confirm'] = 'Yes, update all {0} {0?{cases}:{case}}';
$lang['cases_edit_many_diff_confirm_dialog'] = 'Really update all included test cases? Note that this change cannot be undone and may affect a lot of test cases.';
$lang['cases_edit_many_nofields'] = 'You need to update at least one field (for example: Type, Priority, etc.).';
$lang['cases_edit_many_diff_empty'] = 'Empty';
$lang['cases_edit_many_scope'] = 'Edit Scope';
$lang['cases_edit_all_filter'] = 'Filter';
$lang['cases_edit_all_filter_cases'] = '<em>{0}</em> {0?{test cases}:{test case}} included.';
$lang['cases_edit_all_filter_cases_group'] = '<em>{0}</em> {0?{test cases}:{test case}} included (of group <em>{1}</em>).';
$lang['cases_edit_all_filter_cases_section'] = '<em>{0}</em> {0?{test cases}:{test case}} included (of section <em>{1}</em>).';
$lang['cases_edit_all_filter_cases_section_sub'] = '<em>{0}</em> {0?{test cases}:{test case}} included (of section <em>{1}</em>, incl. subsections).';

$lang['cases_various'] = 'Various';
$lang['cases_box'] = 'Test Case';
$lang['cases_case'] = 'Test Case';
$lang['cases_ids'] = 'Case IDs';
$lang['cases_id'] = 'ID';
$lang['cases_id_nolink'] = 'ID (no link)';
$lang['cases_included'] = 'Included';
$lang['cases_title'] = 'Title';
$lang['cases_title_nolink'] = 'Title (no link)';
$lang['cases_title_desc'] = 'Ex: <em>Opening a simple log file</em>';
$lang['cases_suite'] = 'Suite';
$lang['cases_suite_id'] = 'Suite ID';
$lang['cases_section'] = 'Section';
$lang['cases_section_full'] = 'Section Hierarchy';
$lang['cases_section_depth'] = 'Section Depth';
$lang['cases_section_desc'] = 'Section Description';
$lang['cases_title_and_link'] = 'Case Details (ID, Title and Link)';
$lang['cases_link'] = 'Case Link';
$lang['cases_offset'] = 'Offset';

$lang['cases_template'] = 'Template';
$lang['cases_type'] = 'Type';
$lang['cases_created_by'] = 'Created By';
$lang['cases_created'] = 'Created';
$lang['cases_created_on'] = 'Created On';
$lang['cases_updated_by'] = 'Updated By';
$lang['cases_updated'] = 'Updated';
$lang['cases_updated_on'] = 'Updated On';
$lang['cases_priority'] = 'Priority';
$lang['cases_estimate'] = 'Estimate';
$lang['cases_forecast'] = 'Forecast';
$lang['cases_references'] = 'References';
$lang['cases_milestone'] = 'Milestone';
$lang['cases_attachments'] = 'Attachments';
$lang['cases_in_section'] = 'In section <a href="{0}">{1}</a>.';
$lang['cases_description'] = 'Description';
$lang['cases_description_empty'] = 'No additional details available.';
$lang['cases_not_from_same_suite'] = 'Some test cases no longer exist or are from different test suites.';
$lang['cases_columns'] = 'Columns';
$lang['cases_filter'] = 'Filter';
$lang['cases_filter_save'] = 'Save Filter';
$lang['cases_people_dates'] = 'People &amp; Dates';

$lang['cases_softlock'] = 'Not saved: this test case has been changed by other users';
$lang['cases_softlock_desc'] = 'This case has been modified since you opened it
(by <em>{0}</em> on <em>{1}</em>, and possibly others). You can <a target="_blank" href="{2}">review the changes</a>
and still save the case. Please note that this will override all changes made by other users.';
$lang['cases_softlock_force'] = 'Yes, override all made changes and save my version';

$lang['cases_steps'] = 'Steps';
$lang['cases_steps_step_placeholder'] = 'Step Description';
$lang['cases_steps_id'] = 'Step ID';
$lang['cases_steps_name'] = 'Step Name';
$lang['cases_steps_loading'] = 'Loading steps ... ';
$lang['cases_steps_invalid'] = 'Steps field has an invalid format.';
$lang['cases_steps_add'] = 'Add Step';
$lang['cases_steps_content_image'] = 'Add an image to the Step field.';
$lang['cases_steps_expected'] = 'Expected Result';
$lang['cases_steps_expected_placeholder'] = 'Expected Result';
$lang['cases_steps_expected_image'] = 'Add an image to the Expected Result field.';
$lang['cases_steps_has_expected'] = 'Has Expected';
$lang['cases_steps_rows'] = 'Rows';
$lang['cases_steps_actual'] = 'Actual Result';
$lang['cases_steps_actual_enter'] = 'Enter actual result';
$lang['cases_steps_actual_image'] = 'Add an image to the Actual Result field.';
$lang['cases_steps_step'] = 'Step';
$lang['cases_steps_desc'] = 'The steps and expected results of this test case.';
$lang['cases_steps_empty_title'] = 'Add steps to this test case';
$lang['cases_steps_empty_title_many'] = 'Add steps to the test cases';
$lang['cases_steps_empty_body'] = 'For every test step, enter a short description and the
expected results. <a {0}>Add the first step</a>.';
$lang['cases_steps_explanation_title'] = 'What are steps?';
$lang['cases_steps_explanation_body'] = 'Enter all test steps needed to verify this test case.';
$lang['cases_steps_explanation_body_many'] = 'Enter all test steps needed to verify the test cases.';
$lang['cases_steps_confirm'] = 'Really delete this step? This operation cannot be undone.';

$lang['cases_steps_field_invalid'] = 'Invalid custom field: the custom field may have been deleted.';
$lang['cases_steps_no'] = 'No test steps available.';
$lang['cases_steps_set_status'] = 'Set all steps to "{0}".';
$lang['cases_steps_step_status'] = 'This step is marked as "{0}".';
$lang['cases_steps_results'] = '{0} passed, {1} failed and {2} untested steps. <a {3}>Show details</a>.';
$lang['cases_steps_unavailable'] = 'No test steps available because you are adding multiple test results.';

$lang['cases_steps_hint_title'] = 'Did you know?';
$lang['cases_steps_hint_info'] = 'You can also configure TestRail to enter test steps separately:';
$lang['cases_steps_hint_more'] = 'Learn more';

$lang['cases_success_add'] = 'Successfully added the new test case. <a href="{0}">Add another</a>';
$lang['cases_success_view'] = 'Successfully added the new test case. <a href="{0}">View test case</a>';

$lang['cases_success_delete'] = 'Successfully deleted the test case.';
$lang['cases_success_update'] = 'Successfully updated the test case.';
$lang['cases_success_update_many'] = 'Successfully updated the test cases.';
$lang['cases_error_add'] = 'An error occurred while adding the new test case.';
$lang['cases_error_exists'] = 'The specified test case does not exist or you do not have the permission to access it.';
$lang['cases_error_delete'] = 'An error occurred while deleting the test case. Maybe the test case didn\'t exist anymore?';
$lang['cases_error_update'] = 'An error occurred while saving the test case.';

$lang['cases_denied_add'] = 'You are not allowed to add test cases (insufficient permissions).';
$lang['cases_denied_edit'] = 'You are not allowed to edit test cases (insufficient permissions).';
$lang['cases_denied_copy'] = 'You are not allowed to copy test cases (insufficient permissions).';
$lang['cases_denied_move'] = 'You are not allowed to move test cases (insufficient permissions).';
$lang['cases_denied_delete'] = 'You are not allowed to delete test cases (insufficient permissions).';
$lang['cases_denied_readonly'] = 'This operation is not allowed. The test case is read-only.';

$lang['cases_dnd_copy'] = 'Copy here';
$lang['cases_dnd_copy_hint'] = '(shift)';
$lang['cases_dnd_move'] = 'Move here';
$lang['cases_dnd_move_hint'] = '(ctrl/cmd)';
$lang['cases_dnd_cancel'] = 'Cancel';

$lang['cases_auto_section'] = 'Test Cases';
$lang['cases_no_attachments'] = 'No attachments.';
$lang['cases_related'] = 'Related Test Cases';
$lang['cases_no_related'] = 'None';
$lang['cases_linking_here'] = 'Test cases linking here';
$lang['cases_links_to_others'] = 'Links to other test cases';

$lang['cases_grid_case'] = 'Test Case';
$lang['cases_grid_add'] = 'Add Test Case';
$lang['cases_grid_nocases'] = 'No test cases.';

$lang['cases_invalid_milestone'] = 'The specified milestone is invalid or belongs to a different project.';
$lang['cases_invalid_section'] = 'The specified section is invalid or belongs to a different suite/project.';
$lang['cases_invalid_javascript'] = 
'The JavaScript of your browser is not working properly and the test case wasn\'t saved as a safety measure.
This can be caused by malfunctioning browser plugins or invalid UI scripts that were added to your TestRail installation.
Please contact your TestRail administrator to look into this.';
$lang['cases_error_move'] = 'Moving the case to the new section failed.';

$lang['cases_activity_empty'] = 'No activity yet.';
$lang['cases_tests'] = 'Tests &amp; Results';

$lang['cases_print_hint'] = 'Print Case';
$lang['cases_print_hint_desc'] = 'Opens a print view of this test case.';
$lang['cases_edit_title'] = 'Edit Case Title';
$lang['cases_edit_title_save'] = 'Save Title';
$lang['cases_edit_title_required'] = 'The Case Title field is required.';
$lang['cases_edit_title_field'] = 'Case Title';
$lang['cases_edit_title_desc'] = 'Edit the title of the test case. ';

$lang['cases_sidebar_case'] = 'Details';
$lang['cases_sidebar_results'] = 'Tests &amp; Results';
$lang['cases_sidebar_history'] = 'History';
$lang['cases_sidebar_defects'] = 'Defects';

$lang['cases_results_runs'] = 'Runs';
$lang['cases_results_changes'] = 'Results &amp; Comments';
$lang['cases_results_empty'] = 'No test results so far.';
$lang['cases_results_empty_desc'] = 'Results can be added in test runs on the Test Runs &amp; Results tab.';

$lang['cases_history_created'] = 'Created';
$lang['cases_history_created_desc'] = 'This test case was created.
Changes to this test case are displayed above, separately for each update.';
$lang['cases_history_updated'] = 'Updated';
$lang['cases_history_unknown'] = 'Unknown Date';

$lang['cases_defects'] = 'Defects';
$lang['cases_defects_empty'] = 'No defects so far.';
$lang['cases_defects_empty_desc'] = 'Defects can be linked on the Add Result dialog when adding results for this case (on the Test Runs &amp; Results tab).';

$lang['cases_no_default_priority'] = 'There is no default value for the test case priority.';
$lang['cases_no_default_case_type'] = 'There is no default value for the test case type.';
$lang['cases_no_default_template'] = 'There is no default value for the test case template.';

$lang['cases_directions_prev_title'] = 'Go to the previous test case in this suite.';
$lang['cases_directions_next_title'] = 'Go to the next test case in this suite.';
$lang['cases_directions_no_next'] = 'There are no more test cases after the current case.';
$lang['cases_directions_no_prev'] = 'There are no more test cases before the current case.';
