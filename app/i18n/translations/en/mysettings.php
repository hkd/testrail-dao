<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$lang['mysettings_save'] = 'Save Settings';
$lang['mysettings_error_emailinuse'] = 'The Email Address is already in use by another user.';
$lang['mysettings_success_update'] = 'Successfully saved your settings.';

$lang['mysettings_sidebar_width'] = 'Sidebar Width';
$lang['mysettings_qpane_context'] = 'Quick Pane Context';
$lang['mysettings_qpane_active'] = 'Quick Pane Active';
$lang['mysettings_qpane_width'] = 'Quick Pane Width';
$lang['mysettings_goal'] = 'Goal';
$lang['mysettings_goal_checked'] = 'Checked';

$lang['mysettings_invalid_javascript'] = 
'The JavaScript of your browser is not working properly and the settings weren\'t saved as a safety measure.
This can be caused by malfunctioning browser plugins or invalid UI scripts that were added to your TestRail installation.
Please contact your TestRail administrator to look into this.';
