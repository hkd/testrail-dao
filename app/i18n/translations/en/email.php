<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$lang['email_no_server'] = 'There is no SMTP server configured for sending email.';

$lang['email_forgotpassword_subject'] = '[%{installation_name}] Resetting your TestRail password';
$lang['email_forgotpassword_body'] = 'Dear %{name},

You (or somebody else) requested to reset the password for your TestRail
user account. If you don\'t want to reset the password, you can simply
ignore this email.

To reset your TestRail password, please use this link:

%{url}

Sincerely,
%{installation_name}
Powered by TestRail';
$lang['email_reset_password_subject'] = '[%{installation_name}] Resetting your TestRail password';
$lang['email_reset_password_body'] = 'Dear %{name},

The administrator has reset the password for this account. Please follow the link below to create a new one.

%{url}

Sincerely,
%{installation_name}
Powered by TestRail';

$lang['email_invite_user_subject'] = '[%{installation_name}] A new user account was created for you';
$lang['email_invite_user_body'] = 'Dear %{name},

A new TestRail (test management) user account was created for you.
You can set a new password for your user account on the following
page (and then log in):

%{url}

Enjoy,
%{installation_name}
Powered by TestRail';

$lang['email_notify_test_assignedto_subject'] = '[%{installation_name}] Test %{id} was assigned to you';
$lang['email_notify_test_assignedto_many_subject'] = '[%{installation_name}] %{test_count} tests were assigned to you';

$lang['email_notify_test_status_subject'] = '[%{installation_name}] Test %{id} was set to %{status}';
$lang['email_notify_test_status_many_subject'] = '[%{installation_name}] %{test_count} tests were set to %{status}';

$lang['email_notify_test_unassigned_subject'] = '[%{installation_name}] Test %{id} was unassigned';
$lang['email_notify_test_unassigned_many_subject'] = '[%{installation_name}] %{test_count} tests were unassigned';

$lang['email_notify_test_comment_subject'] = '[%{installation_name}] A comment was added to test %{id}';
$lang['email_notify_test_comment_many_subject'] = '[%{installation_name}] A comment was added to %{test_count} tests';

$lang['email_notify_test_assignedto_header'] = 'Dear %{name},

The test was assigned to you by %{user}.

Test Run:    %{run}
Project:     %{project}
Assigned To: You

';

$lang['email_notify_test_assignedto_many_header'] = 'Dear %{name},

The tests were assigned to you by %{user}.

Test Run:    %{run}
Project:     %{project}
Assigned To: You

';

$lang['email_notify_test_status_header'] = 'Dear %{name},

The test result was added by %{user}.

Test Run:    %{run}
Project:     %{project}
Status:      %{status}

';

$lang['email_notify_test_status_many_header'] = 'Dear %{name},

The test results were added by %{user}.

Test Run:    %{run}
Project:     %{project}
Status:      %{status}

';

$lang['email_notify_test_unassigned_header'] = 'Dear %{name},

The test was set to unassigned by %{user}.

Test Run:    %{run}
Project:     %{project}
Assigned To: 

';

$lang['email_notify_test_unassigned_many_header'] = 'Dear %{name},

The tests were set to unassigned by %{user}.

Test Run:    %{run}
Project:     %{project}
Assigned To: 

';

$lang['email_notify_test_comment_header'] = 'Dear %{name},

The following comment was added by %{user}:

"%{comment}"

Test Run:    %{run}
Project:     %{project}

';

$lang['email_notify_test_body_intro'] = "Please see below for the affected tests:\n\n";
$lang['email_notify_test_body'] = "%{id}: %{title}\n%{url}\n\n";
$lang['email_notify_test_footer'] = 'You can disable email notifications for your account under My Settings:

%{unsubscribe}

Sincerely,
%{installation_name}
Powered by TestRail';

$lang['email_notify_run_subject'] = '[%{installation_name}] Test run %{id} was assigned to you';
$lang['email_notify_run_body'] = 'Dear %{name},

The following test run was assigned to you by %{user}:

Test Run: %{run}
Project:  %{project}

The test run and its tests can be viewed at:

%{url}

Sincerely,
%{installation_name}
Powered by TestRail';

$lang['email_test_subject'] = '[%{installation_name}] Test Email';
$lang['email_test_body'] = 'Hello,

This is an email sent to your email address to test the email settings
of a TestRail installation. Since you are currently reading this email,
it seems everything is working fine with the email settings.

The TestRail installation is available at:

%{url}

Sincerely,
%{installation_name}
Powered by TestRail';

$lang['email_notify_report_link_subject'] = '[%{installation_name}] A new report is available: %{report_name}';
$lang['email_notify_report_link_body'] = 'Hello,

The following report is now available:

%{report_name}
%{report_url}

Sincerely,
%{installation_name}
Powered by TestRail';

$lang['email_notify_report_attachment_subject'] = '[%{installation_name}] A new report is available: %{report_name}';
$lang['email_notify_report_attachment_body'] = 'Hello,

The following TestRail report was sent to you:

%{report_name}

The report is attached to this email, either as a ZIP file, a PDF file, or both. 
If the email contains a ZIP file, then after downloading and extracting it, 
you can view the report in your web browser by opening the index.html file.

If the email contains a PDF, after downloading it you can open it in your preferred PDF viewer.

Sincerely,
%{installation_name}
Powered by TestRail';
