<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$lang['attachments_attachment'] = 'Attachment';
$lang['attachments_title'] = 'Attachments';
$lang['attachments_noattachments'] = 'No attachments.';
$lang['attachments_file'] = 'File';
$lang['attachments_file_desc'] = 'Choose the file to upload.';
$lang['attachments_image'] = 'Image';
$lang['attachments_image_desc'] = 'Choose the image to upload.';
$lang['attachments_add'] = 'Add Attachment';
$lang['attachments_addimage'] = 'Add Images';
$lang['attachments_confirm_delete'] = 'Really remove this attachment? This change cannot be undone.';
$lang['attachments_error_exists'] = 'The attachment does not exist or you do not have the permission to access it.';
$lang['attachments_error_required'] = 'The File field is required.';
$lang['attachments_error_partial'] = 'Attachment was only partially uploaded.';
$lang['attachments_error_nodir'] = 'No attachments directory configured.';
$lang['attachments_error_noaccess'] = 'Attachments directory is not writable.';
$lang['attachments_error_rename'] = 'An error occurred while renaming the attachment.';
$lang['attachments_error_in_use'] = 'Cannot delete an attachment that is still in use.';
$lang['attachments_denied_add'] = 'You are not allowed to add attachments (insufficient permissions).';
$lang['attachments_denied_edit'] = 'You are not allowed to edit attachments (insufficient permissions).';
$lang['attachments_denied_delete'] = 'You are not allowed to delete attachments (insufficient permissions).';
$lang['attachments_element'] = 'Element';

$lang['attachments_drop'] = 'Drop files here to attach, or click to browse.';
$lang['attachments_drop_image'] = 'Drop images here to embed, or click to browse.';
$lang['attachments_drop_image_nobrowse'] = 'Drop images here to embed.';
$lang['attachments_drop_notype'] = 'You can only add images to this text field (example: PNG or JPG files).';
$lang['attachments_drop_notype_canattach'] = 'You can only add images to this text field (example: PNG or JPG files). You can attach other file types to a case or result from the sidebar or result dialogs.';

$lang['attachments_screenshot_take_mac'] = 'How to take a screenshot on your Mac:';
$lang['attachments_screenshot_take_win'] = 'How to take a screenshot on Windows:';
$lang['attachments_screenshot_paste'] = 'Then paste it:';
