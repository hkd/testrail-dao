<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php
$lang['settings_tabs_application'] = 'Application';
$lang['settings_tabs_email'] = 'Email';
$lang['settings_tabs_updates'] = 'Updates';
$lang['settings_tabs_sessions'] = 'Sessions';
$lang['settings_tabs_integration'] = 'Integration';
$lang['settings_tabs_interface'] = 'User Interface';
$lang['settings_tabs_auth'] = 'Login';
$lang['settings_tabs_security'] = 'Security';
$lang['settings_tabs_api'] = 'API';
$lang['settings_tabs_index'] = 'Tab Index';

$lang['settings_save'] = 'Save Settings';
$lang['settings_success_update'] = 'Successfully updated site settings.';
$lang['settings_error_update'] = 'An error occurred while updating site settings.';
$lang['settings_error_attachment_dir'] = 'Attachments Directory is not a valid (writable) directory.';
$lang['settings_error_log_dir'] = 'Log Directory is not a valid (writable) directory.';
$lang['settings_error_defect_id_url'] = 'Defect View Url does not contain the %id% placeholder.';
$lang['settings_error_reference_id_url'] = 'Reference View Url does not contain the %id% placeholder.';
$lang['settings_error_edit_mode'] = 'The Editing Test Results field does not contain a valid value.';
$lang['settings_error_defect_plugin'] = 'The Defect Plugin field does not contain a valid value.';
$lang['settings_error_license_key'] = '<strong>{0}</strong>
Please enter/paste the complete license key or contact the
<a href="http://www.gurock.com/support/" target="_blank">Gurock Software support</a> if this problem persists.';

$lang['settings_installation_name'] = 'Installation Name';
$lang['settings_installation_name_field'] = 'Installation Name';
$lang['settings_installation_name_desc'] = 'The name of this TestRail installation. The installation name is displayed on the login screen and the dashboard.';
$lang['settings_installation_url'] = 'Web Address';
$lang['settings_installation_url_field'] = 'Web Address';
$lang['settings_installation_url_desc'] = 'The web address of your TestRail installation.
Used, among other things, for links in email notifications.';
$lang['settings_attachment_dir'] = 'Attachment Directory';
$lang['settings_attachment_dir_field'] = 'Attachment Directory';
$lang['settings_attachment_dir_desc'] = 'The directory on the web server where uploaded attachments are stored.
Must be writable for TestRail and shouldn\'t be directly accessible with a web browser.';

$lang['settings_report_dir'] = 'Report Directory';
$lang['settings_report_dir_field'] = 'Report Directory';
$lang['settings_report_dir_desc'] = 'The directory on the web server where generated reports are stored.
Must be writable for TestRail and shouldn\'t be directly accessible with a web browser.';

$lang['settings_log_dir'] = 'Log Directory';
$lang['settings_log_dir_field'] = 'Log Directory';
$lang['settings_log_dir_desc'] = 'The directory on the web server where TestRail log files are stored.
Must be writable for TestRail. Please create this directory if it doesn\'t already exist.';

$lang['settings_integration_jira_title'] = 'Using JIRA?';
$lang['settings_integration_jira_intro'] = 'Click the button below to set up the integration between JIRA and TestRail.
The integration enables you to view JIRA issues and add new issues directly from TestRail.';
$lang['settings_integration_jira_quick'] = 'Quick links:';
$lang['settings_integration_jira_quick_overview'] = 'JIRA overview';
$lang['settings_integration_jira_quick_docs'] = 'JIRA configuration';
$lang['settings_integration_jira_configure'] = 'Configure JIRA Integration';
$lang['settings_integration_jira_hide'] = 'or hide this message';
$lang['settings_integration_jira_enable'] = 'Enable JIRA Integration';
$lang['settings_jira_title'] = 'Configure JIRA Integration';
$lang['settings_jira_address'] = 'JIRA Address';
$lang['settings_jira_address_hint'] = 'https://example.atlassian.net/';
$lang['settings_jira_address_desc'] = 'The full web address of your JIRA installation (as you access it with your web browser).';
$lang['settings_jira_version'] = 'JIRA Version';
$lang['settings_jira_version_4x'] = '3.x, 4.x (legacy SOAP integration)';
$lang['settings_jira_version_5x'] = '5.x, 6.x, 7.x or later / JIRA Cloud';
$lang['settings_jira_user'] = 'JIRA User';
$lang['settings_jira_user_noemail'] = 'JIRA requires the actual JIRA username instead of an email address.
So please enter your JIRA username for the integration instead.';
$lang['settings_jira_user_desc'] = 'The default JIRA user used for the integration.
New issues will appear in the name of this user (reporter). Can be overridden per user in
TestRail under My Settings.';
$lang['settings_jira_password'] = 'JIRA Password';
$lang['settings_jira_password_desc'] = 'The matching password for the default JIRA integration user.';
$lang['settings_jira_defects'] = 'Defect integration (to link results to and push/lookup JIRA issues)';
$lang['settings_jira_defects_field'] = 'Enable Defects';
$lang['settings_jira_refs'] = 'Reference integration (to link cases to JIRA requirements/user stories)';
$lang['settings_jira_refs_field'] = 'Enable References';
$lang['settings_jira_empty'] = 'Please enable at least one integration option (test results and/or test cases).';
$lang['settings_jira_variable_user'] = 'JIRA User';
$lang['settings_jira_variable_user_desc'] = 'Your JIRA user name (not email address) used for the TestRail and JIRA integration.';
$lang['settings_jira_variable_password'] = 'JIRA Password';
$lang['settings_jira_variable_password_desc'] = 'The matching password for your JIRA user account.';

$lang['settings_integration_update'] = 'Successfully updated integration settings.';
$lang['settings_integration_user_field'] = 'User Variable';
$lang['settings_integration_user_field_type'] = 'Type';
$lang['settings_integration_user_fields_none'] = 'No user variables configured.';
$lang['settings_integration_user_fields_add'] = 'Add User Variable';
$lang['settings_integration_tabs_defects'] = 'Defects';
$lang['settings_integration_tabs_references'] = 'References';
$lang['settings_integration_tabs_fields'] = 'User Variables';

$lang['settings_default_language'] = 'Default Language';
$lang['settings_default_language_field'] = 'Default Language';
$lang['settings_default_language_desc'] = 'Determines the default language of the user interface.
Users can override their language under My Settings.';
$lang['settings_default_locale'] = 'Default Locale';
$lang['settings_default_locale_field'] = 'Default Locale';
$lang['settings_default_locale_desc'] = 'Determines how dates and numbers are formatted by default.
Users can override their locale under My Settings.';
$lang['settings_default_timezone'] = 'Default Time Zone';
$lang['settings_default_timezone_field'] = 'Default Time Zone';
$lang['settings_default_timezone_desc'] = 'Determines the default time zone for dates and times.
Users can override their time zone under My Settings.';
$lang['settings_default_timezone_empty'] = 'Use server time zone';

$lang['settings_no_task'] = 'The background task is not installed';
$lang['settings_no_task_desc'] = 'The background task is responsible for email
notifications, generating reports, adding custom fields and various other tasks.
Please enable the background task as follows:<br /><br />
<a target="_blank" href="http://docs.gurock.com/testrail-admin/howto-background-task">Activating the background task</a>';

$lang['settings_email_server'] = 'Server';
$lang['settings_email_server_field'] = 'Email Server';
$lang['settings_email_server_desc'] = 'The host name and port of the machine that is used for sending out
emails. If the port differs from 25, append it like this: \'mail.example.com:50\'.';
$lang['settings_email_from'] = 'From';
$lang['settings_email_from_field'] = 'Email From';
$lang['settings_email_from_desc'] = 'The email address that is used for sending out emails.';
$lang['settings_email_user'] = 'User';
$lang['settings_email_user_field'] = 'Email User';
$lang['settings_email_user_desc'] = 'Leave empty if the email server does not require authentication.';
$lang['settings_email_pass'] = 'Password';
$lang['settings_email_pass_field'] = 'Email Password';
$lang['settings_email_pass_desc'] = 'Leave empty if the email server does not require authentication.';
$lang['settings_email_notifications'] = 'Enable email notifications';
$lang['settings_email_notifications_short'] = 'Notifications';
$lang['settings_email_notifications_field'] = 'Email Notifications';
$lang['settings_email_notifications_desc'] = 'Email notifications are sent for test changes and test results.
Can also be disabled on a per-user basis via My Settings.';
$lang['settings_email_ssl'] = 'Use a secure connection for sending emails (SSL)';
$lang['settings_email_ssl_field'] = 'Email SSL';
$lang['settings_email_ssl_desc'] = 'Enable this option if your email smtp server uses and requires a secure
socket connection (SSL).';

$lang['settings_email_test'] = 'Send Test Email';
$lang['settings_email_test_link'] = 'You can <a tabindex="-1" {0}>send a test email</a> to check if your settings are working.';
$lang['settings_email_test_intro'] = 'Note that this test uses the stored email settings.
Please save your settings first in case you made any changes.';
$lang['settings_email_test_email'] = 'Email Address';
$lang['settings_email_test_email_descr'] = 'The email address of the person who should receive the test email.';
$lang['settings_email_test_email_required'] = 'The Email Address field is required.';
$lang['settings_email_test_email_invalid'] = 'Please specify a valid email address.';
$lang['settings_email_test_failure'] = 'Sending the test email failed. Please see the following messages and server output for details:';
$lang['settings_email_test_success'] = 'Successfully sent the email. Please check the inbox.';

$lang['settings_defect_id_url'] = 'Defect View Url';
$lang['settings_defect_id_url_field'] = 'Defect View Url';
$lang['settings_defect_id_url_desc'] = 'The web address of a case of your defect tracker.
Use %id% as the placeholder for the actual case ID.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_defect_add_url'] = 'Defect Add Url';
$lang['settings_defect_add_url_field'] = 'Defect Add Url';
$lang['settings_defect_add_url_desc'] = 'The web address for adding a new case to your defect tracker.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';

$lang['settings_defect_plugin'] = 'Defect Plugin';
$lang['settings_defect_plugin_field'] = 'Defect Plugin';
$lang['settings_defect_plugin_desc'] = 'The plugin for integrating TestRail with your defect tracker.
The plugin can be configured below. <a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_defect_config'] = 'Configuration';
$lang['settings_defect_config_field'] = 'Defect Plugin Configuration';
$lang['settings_defect_config_desc'] = 'Make sure to use HTTPS for a secure connection to your defect tracker.
User variables are recommended to store the user &amp; password securely
(can also be used to customize the login per user).
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_defect_template_field'] = 'Defect Plugin Template';
$lang['settings_defect_template_expand'] = '<a {0}>Enter a template</a> for the description field of the defect dialog.';
$lang['settings_defect_template_desc'] = 'The template for the description field of the defect dialog.
You can add various placeholder variables via the Add Field link below.';
$lang['settings_defect_template_add_field'] = 'Add Field';
$lang['settings_defect_template_placeholder'] = 'Status: %tests:status_id% .. ';
$lang['settings_defect_template_default'] = 
'%tests:comment%

%tests:details%';

$lang['settings_defect_template_dialog_title'] = 'Add Field';
$lang['settings_defect_template_dialog_field'] = 'Field';
$lang['settings_defect_template_dialog_field_desc'] = 'The field to add to the description template.';
$lang['settings_defect_template_dialog_add'] = 'Add Field';

$lang['settings_reference_id_url'] = 'Reference View Url';
$lang['settings_reference_id_url_field'] = 'Reference View Url';
$lang['settings_reference_id_url_desc'] = 'The web address for your case references (requirements or
user stories, e.g.). Use %id% as the placeholder for the actual reference ID.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_reference_add_url'] = 'Reference Add Url';
$lang['settings_reference_add_url_field'] = 'Reference Add Url';
$lang['settings_reference_add_url_desc'] = 'The web address for adding a new reference.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_reference_plugin'] = 'Reference Plugin';
$lang['settings_reference_plugin_field'] = 'Reference Plugin';
$lang['settings_reference_plugin_desc'] = 'The plugin for integrating TestRail with your requirement, issue
or bug tracker. The plugin can be configured below.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';
$lang['settings_reference_config_field'] = 'Defect Plugin Configuration';
$lang['settings_reference_config_desc'] = 'Make sure to use HTTPS for a secure connection.
User variables are recommended to store the user &amp; password securely and can be configured
on the <em>Defects</em> tab (can also be used to customize the login per user).
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-integration/">Learn more</a>';

$lang['settings_check_for_updates'] = 'Check for TestRail updates';
$lang['settings_check_for_updates_field'] = 'Check for Updates';
$lang['settings_check_for_updates_desc'] = 'Specifies if TestRail should check for new versions once a day and notifies you
when an update is available. Your current TestRail version, license key ID and a hash of the
server\'s hostname is transmitted during an update check.';

$lang['settings_apiv2_enabled'] = 'Enable API';
$lang['settings_apiv2_enabled_field'] = 'API Enabled';
$lang['settings_apiv2_enabled_desc'] = 'TestRail\'s API can be used to integrate with test automation tools, for UI customizations or for the initial import of projects, test cases, etc.
<a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-api2/">Learn more</a>';
$lang['settings_apiv2_session_enabled'] = 'Enable session authentication for API';
$lang['settings_apiv2_session_enabled_field'] = 'API Session Auth';
$lang['settings_apiv2_session_enabled_desc'] = 'Session authentication works by using the session cookie for authenticating API requests.
This is useful when calling API methods from <a class="link" tabindex="-1" target="_blank" href="http://docs.gurock.com/testrail-custom/uiscripts-introduction">UI scripts</a> (in the context of the current user).';

$lang['settings_edit_mode'] = 'Editing Test Results';
$lang['settings_edit_mode_field'] = 'Editing Test Results';
$lang['settings_edit_mode_desc'] = 'Choose whether users are allowed to edit their own test results.
You can specify the maximum time a user can do so after submitting the result. You can choose specific time frames,
allow unlimited editing or completely disable this feature.';

$lang['settings_name_format'] = 'Name Formatting';
$lang['settings_name_format_field'] = 'Name Formatting';
$lang['settings_name_format_desc'] = 'Specifies how (abbreviated) user names are formatted and displayed in the user interface.';
$lang['settings_name_format_last_name'] = 'Shorten Last Name';
$lang['settings_name_format_first_name'] = 'Shorten First Name';

$lang['settings_partial_count'] = 'Pagination Limit';
$lang['settings_partial_count_slow'] = '(may be slow)';
$lang['settings_partial_count_desc'] = 'Specifies how many items are displayed by default on some pages (e.g. tests or test cases). Please note that it\'s recommended to use the compact view mode for larger test suites &amp; test runs for best performance.';

$lang['settings_sess_idle_policy'] = 'Idle Session Timeout Policy';
$lang['settings_sess_custom_idle'] = 'Custom Idle Session Timeout (mins)';
$lang['settings_sess_abs_timeout'] = 'Absolute Session Timeout Policy';
$lang['settings_sess_custom_abs_timeout'] = 'Custom Absolute Session Timeout (hours)';
$lang['settings_sess_disable_tpolicy'] = 'Disable Session Timeout Policy';
$lang['settings_sess_disable_rememberme'] = 'Disable Remember Me Checkbox';

$lang['settings_sess_idle_policy_desc'] = 'Logs the user out after a certain period of inactivity.';
$lang['settings_sess_custom_idle_desc'] = 'You can specify the idle timeout threshold in minutes.';
$lang['settings_sess_abs_timeout_desc'] = 'Logs the user out after a certain period, irrespective of whether they are active or not';
$lang['settings_sess_custom_abs_timeout_desc'] = 'You can specify the absolute timeout threshold in hours.';
$lang['settings_sess_disable_tpolicy_desc'] = 'Completely disables the session timeout feature. Users will be able to set the Remember Me checkbox on the Login page, and remain logged in unless they clear their browser cookies or click the Logout button. This is the default behaviour.';
$lang['settings_sess_disable_rememberme_desc'] = 'Hides the Remember Me checkbox on the login, so when users close their browser after using TestRail, their session is closed and they have to login again. Can be useful if you want to limit your user session lengths without enforcing an idle or absolute session. (Remember Me must be disabled if you choose to implement a session timeout policy.)';


$lang['settings_database_driver'] = 'Driver';
$lang['settings_database_driver_desc'] = 'The driver for your database.
Usually MS SQL Server if you install on Windows and MySQL if you install on Linux/Unix.';
$lang['settings_database_driver_field'] = 'Database Driver';
$lang['settings_database_server'] = 'Server';
$lang['settings_database_server_field'] = 'Database Server';
$lang['settings_database_server_desc'] = 'The host name of the database server.';
$lang['settings_database_database'] = 'Database';
$lang['settings_database_database_field'] = 'Database';
$lang['settings_database_database_desc'] = 'The name of the empty database you have created. This installation 
wizard will automatically create all needed tables in this database.';
$lang['settings_database_user'] = 'User';
$lang['settings_database_user_field'] = 'Database User';
$lang['settings_database_user_desc'] = 'The TestRail database user. This user needs permissions to alter the 
database schema (ex: to create or drop tables).';
$lang['settings_database_password'] = 'Password';
$lang['settings_database_password_field'] = 'Database Password';

$lang['settings_database_driver_error'] = 'Unknown database driver selected.';
$lang['settings_database_connection_error'] = 'Could not connect to the specified database: {0}';

$lang['settings_database_driver_sqlsrv'] = 'MS SQL Server (2008, 2012, 2014 or 2016)';
$lang['settings_database_driver_mysql'] = 'MySQL (5.x and higher)';

$lang['settings_license_key'] = 'License Key';
$lang['settings_license_key_field'] = 'License Key';
$lang['settings_license_key_desc'] = 'Your trial or full TestRail license key.';
$lang['settings_license_key_type'] = 'License Type';
$lang['settings_license_key_to'] = 'Licensed To';
$lang['settings_license_key_expiration'] = 'Expiration Date';
$lang['settings_license_key_support'] = 'Support Until';
$lang['settings_license_key_support_expired'] = '(expired)';
$lang['settings_license_key_seats'] = 'Named Users';
$lang['settings_license_key_unlimited'] = 'Unlimited';

$lang['settings_license_update'] = 'Update License';
$lang['settings_license_updated'] = 'Successfully updated your TestRail license key.';

$lang['settings_license_error_decode'] = 'Invalid license key format.';
$lang['settings_license_error_expired'] = 'License has expired (only valid until {0}).';
$lang['settings_license_error_notfound'] = 'No license key found in the database.';
$lang['settings_license_error_toomanyusers'] = 'There are more active users than allowed by the license ({0}/{1}).';
$lang['settings_license_error_nonewuser'] = 'Cannot add a new user ({0} of {1} allowed named users are already in use).
Please add additional named users to your <a href="{2}">TestRail license</a>
or deactivate another user.';
$lang['settings_license_error_user_suffix'] = '{0} Please contact your TestRail administrator to resolve this issue.';
$lang['settings_license_admin_warning'] = '<strong>There is a problem with your current TestRail license:</strong> {0}
Please update your license key below or contact the <a href="http://www.gurock.com/support/" target="_blank">Gurock Software support</a>.';

$lang['settings_subscription_error_user'] = 'Your TestRail Cloud account is disabled. Please contact your TestRail administrator to learn more.';
$lang['settings_subscription_error_expired'] = 'Your TestRail trial has expired (only valid until {0}).';
$lang['settings_subscription_error_admin'] = 'Your TestRail Cloud account is disabled. Please go to Administration, then Subscription to learn more.';
$lang['settings_subscription_manage'] = 'Manage Subscription';
$lang['settings_subscription_goto'] = 'Go to subscription page';
$lang['settings_subscription_admin_warning'] = '<strong>There is a problem with your TestRail subscription:</strong> {0}
Please update your subscription below or contact the <a href="http://www.gurock.com/support/" target="_blank">Gurock Software support</a>.';
$lang['settings_subscription_account'] = 'Account';
$lang['settings_subscription_expiration'] = 'Expiration Date';
$lang['settings_subscription_intro_trial'] = 'Please click below to
create a subscription via the Gurock Software customer portal and upgrade
your trial account to the full edition.';
$lang['settings_subscription_intro_trial_expires'] = 'Your TestRail trial account 
expires on <strong style="color: #606060">{0}</strong>.
Please click below to create a subscription and upgrade
your trial account to the full edition.';
$lang['settings_subscription_intro_at'] = '(at <em>{0}</em>)';
$lang['settings_subscription_intro_trial_expired'] = 'Your TestRail trial account 
expired on <strong style="color: #606060">{0}</strong>.
Please click below to create a subscription and upgrade
your trial account to the full edition.';
$lang['settings_subscription_intro_full'] = 'Please click below to
manage your subscription via the Gurock Software customer portal.
Managing your subscription allows you to change your payment and
contact details.';

$lang['settings_export'] = 'Export';
$lang['settings_exports'] = 'Export &amp; Backup';
$lang['settings_exports_empty'] = 'No exports available.';
$lang['settings_exports_intro'] = 'Export your TestRail database and uploaded files for a local installation or for backup purposes below. Created exports are downloadable for 10 days and are removed afterwards.';
$lang['settings_exports_scheduled'] = 'A new export is currently being created and will be available within the next few minutes.
<br /><a href="{0}">Refresh this page</a>';
$lang['settings_exports_schedule'] = 'Schedule Export';
$lang['settings_exports_open_error'] = 'The export does not exist or you do not have the permission to access it.';
$lang['settings_exports_schedule_success'] = 'Successfully scheduled the export. The export will be available within the next few minutes.';

$lang['settings_hosted_trial'] = 'Please note that some settings cannot be modified in the hosted trial of TestRail and are disabled.';

$lang['settings_auth_login_text'] = 'Login Text';
$lang['settings_auth_login_text_desc'] = 'You can post a custom text to the login page.
This could include links to Wiki pages on how to get a TestRail user account, for example.';
$lang['settings_auth_external_auth'] = 'External Auth';
$lang['settings_auth_external_status'] = 'Status';
$lang['settings_auth_external_path'] = 'Path';
$lang['settings_auth_external_inactive'] = 'Inactive';
$lang['settings_auth_external_active'] = 'Active';
$lang['settings_auth_external_desc'] = 'You can use an external authentication script to authenticate users and integrate TestRail with Active Directory or LDAP servers.
<a class="link" tabindex="-1" target="_blank" href="http://code.gurock.com/p/testrail-auth/">Learn more</a>';
$lang['settings_auth_forgot_password'] = 'Disable Forgot Password functionality';
$lang['settings_auth_forgot_password_field'] = 'Forgot Password';
$lang['settings_auth_forgot_password_desc'] = 'The Forgot Password feature sends a password reset request to users via email (not the actual password).
It can be useful to disable this feature if you use external authentication or do not want to allow this functionality.';
$lang['settings_auth_invite_users'] = 'Disable Invite User functionality';
$lang['settings_auth_invite_users_field'] = 'Invite Users';
$lang['settings_auth_invite_users_desc'] = 'When adding new users via the Invite User feature, TestRail sends an email to the new user to set her/his password.
It can be useful to disable this feature if you do not want to allow this functionality.';

$lang['settings_auth_password_policy'] = 'Password Policy';
$lang['settings_auth_password_policy_unknown'] = 'Field Password Policy uses an unknown password policy.';
$lang['settings_auth_password_policy_invalid'] = 'Field Password Policy uses an invalid custom password policy. Please check your regular expressions for errors and try again.';
$lang['settings_auth_password_policy_desc'] = 'Enforces the selected password policy (not used for existing passwords or passwords automatically generated by TestRail).';
$lang['settings_auth_password_policy_custom'] = 'Custom';
$lang['settings_auth_password_policy_custom_default'] = '
.{15,}
[a-z]
[A-Z]
[0-9]
[`~!@#$%^&*()\-_=+[\]{}|;:\'",<>./?]';
$lang['settings_auth_password_policy_custom_field'] = 'Password Policy Custom';
$lang['settings_auth_password_policy_custom_desc'] = 'You can create a custom password policy with regular expressions. Simply enter one regular expression per line and all expressions must match in order to accept a password.';
$lang['settings_auth_password_policy_description'] = 'Description';
$lang['settings_auth_password_policy_description_default'] = 
'Minimum of 15 characters, at least one lower & upper case character, a number and a special character.';
$lang['settings_auth_password_policy_description_field'] = 'Password Policy Description';
$lang['settings_auth_password_policy_description_desc'] = 'You can optionally enter a short description that is shown to users who enter a password that does not match your password policy.';

$lang['settings_auth_ip_restrictions'] = 'Allow access to TestRail from the following IPs only';
$lang['settings_auth_ip_restrictions_doc'] = 
'; You can use simple IP addresses:
; 192.168.1.1
; Or entire networks:
; 192.168.1.0/24';
$lang['settings_auth_ip_check_nomatch'] = 'Your IP address ({0}) does not match the given IP/network address list. This would lock you out of the account and prevent you from making any further changes. Please allow your current IP address and try again.';
$lang['settings_auth_ip_check_invalid'] = 'Your list of IP/network addresses is empty or uses an invalid format. Please check the addresses for errors and try again.';
$lang['settings_auth_ip_restrictions_current'] = '<a {0}>Add my IP address</a>';
$lang['settings_auth_ip_restrictions_desc'] = 'Restricting access to certain IPs can be used to prevent requests from unauthorized locations. Simply enter one IP or network address per line.';

$lang['settings_projects_with_integration_title'] = 'Some projects override the global integration settings';
$lang['settings_projects_with_integration'] = 'The following projects have their own integration options configured and override some of the global settings defined on this page:';
$lang['settings_projects_with_integration_hint'] = 'Some projects override the global integration settings. See the yellow box at the top of the page for details.';
$lang['settings_projects_override_integration'] = '<strong>Please note:</strong> Any integration settings defined for this project will override the <a href="{0}">global integration settings</a>.';
