<?php if (!defined('ROOTPATH')) exit('No direct script access allowed'); ?>
<?php

$lang['charts_timeframe_title'] = 'Select Time Frame';
$lang['charts_timeframe_field'] = 'Time Frame';
$lang['charts_timeframe_desc'] = 'Select a different time frame for the chart.';
$lang['charts_timeframe_days_7'] = '7 days';
$lang['charts_timeframe_days_14'] = '14 days';
$lang['charts_timeframe_days_30'] = '30 days';
$lang['charts_timeframe_days_60'] = '60 days';
$lang['charts_timeframe_days_90'] = '90 days';
