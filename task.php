<?php

if (php_sapi_name() != 'cli' || isset($_SERVER['REMOTE_ADDR']))
{
	// The background task is not allowed to be run via browser. It
	// should only be started via command line.
	die('Access denied.');
}

@set_time_limit(0);
define('STARTED_WITH_CLI', true);
$_GET['tasks'] = true; // Set the controller to invoke

// Check for environment variables/options that can influence the
// runtime behavior of the task and set them as defines, if found.
$envs = array(
	'DEPLOY_INSTANCE_ID',
	'DEPLOY_HOSTNAME',
	'DEPLOY_INSTALLATION_URL',
	'DEPLOY_ATTACHMENT_PATH',
	'DEPLOY_BACKUP_PATH',
	'DEPLOY_CUSTOM_PATH',
	'DEPLOY_EXPORT_PATH',
	'DEPLOY_LOG_PATH',
	'DEPLOY_REQUEST_HOSTED',
	'DEPLOY_REPORT_PATH',
	'DEPLOY_DEBUG',
	'DEPLOY_DEBUG_TASK',
	'DEPLOY_PROXY_HOST',
	'DEPLOY_PROXY_PORT',
	'DB_DRIVER',
	'DB_HOSTNAME',
	'DB_DATABASE',
	'DB_USERNAME',
	'DB_PASSWORD'
);

foreach ($envs as $env)
{
	$value = getenv($env);

	if ($value !== false)
	{
		define($env, $value);
	}
}

// And finally include the standard index.php of TestRail
require_once dirname(__FILE__) . '/' . 'index.php';
