-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Lun 06 Août 2018 à 02:52
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `testrail`
--

-- --------------------------------------------------------

--
-- Structure de la table `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `view` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL,
  `test_change_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_attachments_case_id` (`case_id`),
  KEY `ix_attachments_test_change_id` (`test_change_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=22 ;

--
-- Contenu de la table `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `filename`, `size`, `created_on`, `project_id`, `case_id`, `test_change_id`, `user_id`) VALUES
(1, 'test de responsive.PNG', '1.test_de_responsive.png', 61964, 1531922133, 1, NULL, 5, NULL),
(2, 'test de responsive2.PNG', '2.test_de_responsive2.png', 117336, 1531922212, 1, NULL, 5, NULL),
(3, 'test de responsive1.PNG', '3.test_de_responsive1.png', 34916, 1531922212, 1, NULL, 5, NULL),
(4, 'somme decimal.PNG', '4.somme_decimal.png', 40069, 1531922773, 1, NULL, 6, NULL),
(5, 'nombre negatif.PNG', '5.nombre_negatif.png', 37796, 1531923359, 1, NULL, 7, NULL),
(6, 'signe speciaux.PNG', '6.signe_speciaux.png', 36250, 1531924082, 1, NULL, 8, NULL),
(7, '1.PNG', '7.1.png', 36682, 1531964563, 2, NULL, 9, NULL),
(8, '2.PNG', '8.2.png', 49696, 1531964613, 2, NULL, 10, NULL),
(9, '3.PNG', '9.3.png', 48156, 1531964939, 2, NULL, 11, NULL),
(10, '3.PNG', '10.3.png', 22247, 1533512262, 4, NULL, 27, NULL),
(11, '4.PNG', '11.4.png', 17236, 1533512454, 4, NULL, NULL, NULL),
(12, '5.PNG', '12.5.png', 17180, 1533512500, 4, NULL, NULL, NULL),
(13, '6.PNG', '13.6.png', 17212, 1533512557, 4, NULL, NULL, NULL),
(14, '7.PNG', '14.7.png', 22551, 1533513038, 4, NULL, 33, NULL),
(15, '8.PNG', '15.8.png', 16767, 1533513299, 4, NULL, NULL, NULL),
(16, '9.PNG', '16.9.png', 16767, 1533513747, 4, NULL, NULL, NULL),
(17, '10.PNG', '17.10.png', 17017, 1533513812, 4, NULL, NULL, NULL),
(18, '11.PNG', '18.11.png', 22373, 1533513860, 4, NULL, 37, NULL),
(19, '4.PNG', '19.4.png', 17236, 1533515849, 4, NULL, 38, NULL),
(20, '6.PNG', '20.6.png', 17212, 1533515849, 4, NULL, 38, NULL),
(21, '5.PNG', '21.5.png', 17180, 1533515849, 4, NULL, 38, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cases`
--

CREATE TABLE IF NOT EXISTS `cases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `estimate` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `custom_preconds` longtext COLLATE utf8_unicode_ci,
  `custom_steps` longtext COLLATE utf8_unicode_ci,
  `custom_expected` longtext COLLATE utf8_unicode_ci,
  `custom_steps_separated` longtext COLLATE utf8_unicode_ci,
  `custom_mission` longtext COLLATE utf8_unicode_ci,
  `custom_goals` longtext COLLATE utf8_unicode_ci,
  `custom_automation_type` int(11) DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `estimate_forecast` int(11) DEFAULT NULL,
  `refs` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suite_id` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_cases_section_id` (`section_id`),
  KEY `ix_cases_suite_id` (`suite_id`),
  KEY `ix_cases_copyof_id` (`copyof_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=19 ;

--
-- Contenu de la table `cases`
--

INSERT INTO `cases` (`id`, `section_id`, `title`, `display_order`, `priority_id`, `estimate`, `milestone_id`, `custom_preconds`, `custom_steps`, `custom_expected`, `custom_steps_separated`, `custom_mission`, `custom_goals`, `custom_automation_type`, `type_id`, `is_copy`, `copyof_id`, `created_on`, `user_id`, `estimate_forecast`, `refs`, `suite_id`, `updated_on`, `updated_by`, `template_id`) VALUES
(1, 1, 'somme simple', 1, 1, NULL, NULL, 'se connecter sur http://localhost/somme/\r\net faire 1+1', 'valeur 1 = 1\r\nvaleur 2 = 2\r\n\r\n', 'Running ''somme nombre entier''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 80,36... OK\r\n3.type on id=a with value 1... OK\r\n4.type on id=b with value 2... OK\r\n5.clickAt on css=button.btn.btn-secondary with value -373,-301... OK\r\n6.assertValue on id=resultat with value 3... OK\r\n7.doubleClickAt on id=a with value 64,37... OK\r\n8.type on id=a with value 3... OK\r\n9.type on id=b with value 10... OK\r\n10.clickAt on css=button.btn.btn-secondary with value 53,37... OK\r\n11.assertValue on id=resultat with value 13... OK\r\n12.mouseDownAt on id=a with value 4,26... OK\r\n13.mouseMoveAt on id=a with value 4,26... OK\r\n14.mouseUpAt on id=a with value 4,26... OK\r\n15.clickAt on id=a with value 4,26... OK\r\n16.type on id=a with value 32... OK\r\n17.type on id=b with value 4000... OK\r\n18.clickAt on css=button.btn.btn-secondary with value 38,23... OK\r\n19.assertValue on id=resultat with value 4032... OK\r\n''somme nombre entier'' completed successfully', NULL, NULL, NULL, 0, 2, 0, NULL, 1531855977, 1, NULL, NULL, 1, 1531919640, 1, 1),
(2, 1, 'somme nombre décimal', 2, 3, NULL, NULL, 'se connecter sur localhost/somme/\r\n', 'valeur 1 = 12,5\r\nvaleur 2 = 15,103', 'Running ''test nombre décimal''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 95,24... OK\r\n3.type on id=a with value 12.5... OK\r\n4.clickAt on id=b with value 72,32... OK\r\n5.type on id=b with value 14.6... OK\r\n6.clickAt on css=button.btn.btn-secondary with value 49,32... OK\r\n7.clickAt on id=a with value 55,40... OK\r\n8.type on id=a with value 12.7... OK\r\n9.clickAt on id=b with value 35,25... OK\r\n10.type on id=b with value 31.04... OK\r\n11.clickAt on css=button.btn.btn-secondary with value 41,26... OK\r\n12.assertValue on id=resultat with value 43.74... Failed:\r\nActual value ''11'' did not match ''43.74''\r\n''test nombre décimal'' was aborted', NULL, NULL, NULL, 0, 7, 0, NULL, 1531917690, 1, NULL, NULL, 1, 1531922677, 1, 1),
(3, 1, 'modification du signe addition', 3, 4, NULL, NULL, 'modifier le code source du fichier index.html\r\nligne 102 :var somme = parseInt($(''#a'').val())+parseInt($(''#b'').val());\r\nmodification en \r\nligne 102 :var somme = parseInt($(''#a'').val())+parseInt($(''#b'').val());\r\nse connecter sur localhost/somme/\r\n', 'clic sur val1 et mettre 1\r\nclic sur valeur2 et mettre 2\r\nclic sur bouton calculer', 'Running ''somme nombre entier''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 80,36... OK\r\n3.type on id=a with value 1... OK\r\n4.type on id=b with value 2... OK\r\n5.clickAt on css=button.btn.btn-secondary with value -373,-301... OK\r\n6.assertValue on id=resultat with value 3... Failed:\r\nActual value ''-1'' did not match ''3''', NULL, NULL, NULL, 0, 7, 0, NULL, 1531919794, 1, NULL, NULL, 1, 1531920075, 1, 1),
(4, 1, 'somme de caractere', 4, 2, NULL, NULL, 'se connecter sur localhost/somme', 'clic sur val1 et taper a\r\nclic sur valeur2 et taper b', 'Running ''somme de lettre''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 60,25... OK\r\n3.clickAt on id=b with value 45,23... OK\r\n4.clickAt on css=button.btn.btn-secondary with value 26,34... OK\r\n''somme de lettre'' completed successfully', NULL, NULL, NULL, 0, 7, 0, NULL, 1531920299, 1, NULL, NULL, 1, 1531920349, 1, 1),
(5, 1, 'test de responsive', 5, 2, NULL, NULL, 'se connecter sur localhost/somme avec un smartphone ensuite une tablette et enfin un ordinateur', 'pour une tablette et smartphone, faire pivoter l''écran', NULL, NULL, NULL, NULL, 0, 2, 0, NULL, 1531921688, 1, NULL, NULL, 1, 1531922858, 1, 1),
(6, 1, 'somme nombre négatif', 6, 2, NULL, NULL, 'se connecter sur localhost/somme/\r\n', 'cliquer sur val1 et entrer 20\r\ncliquer sur valeur2 et entrer -12\r\ncliquer sur calculer\r\n', 'Running ''nombre négatif''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 25,19... OK\r\n3.type on id=a with value -12... OK\r\n4.clickAt on id=b with value 52,31... OK\r\n5.type on id=b with value 20... OK\r\n6.clickAt on css=button.btn.btn-secondary with value 47,29... OK\r\n7.assertValue on id=resultat with value 8... Failed:\r\nActual value ''32'' did not match ''8''\r\n''nombre négatif'' was aborted', NULL, NULL, NULL, 0, 7, 0, NULL, 1531922896, 1, NULL, NULL, 1, 1531923309, 1, 1),
(7, 1, 'test signes spéciaux', 7, 2, NULL, NULL, 'se connecter sur localhost/somme/', 'dans val1 on met -12,,5\r\ndans valeur2 on met 14', '''signes spéciaux'' completed successfully\r\nRunning ''signes spéciaux''\r\n1.open on /somme/... OK\r\n2.clickAt on id=a with value 55,37... OK\r\n3.clickAt on id=b with value 24,30... OK\r\n4.type on id=b with value 12.3... OK\r\n5.clickAt on css=button.btn.btn-secondary with value 30,31... OK\r\n6.assertAlert on id=resultat with value 0... Failed:\r\nNo response!!!!\r\n''signes spéciaux'' was aborted', NULL, NULL, NULL, 0, 7, 0, NULL, 1531923561, 1, NULL, NULL, 1, 1531923982, 1, 1),
(8, 2, 'somme entier simple', 1, 2, NULL, NULL, 'lancer l''application', 'faisons 1+2', '@Test Somme(): 3 = 3\r\nBUILD SUCCESSFUL (total time: 0 seconds)\r\n', NULL, NULL, NULL, 0, 7, 0, NULL, 1531964068, 1, NULL, NULL, 2, 1531964068, 1, 1),
(9, 2, 'Somme des entiers tailles max', 2, 3, NULL, NULL, 'lancer le test', 'mettre la taille maximum dans les deux valeurs', '@Test Somme(): -2.147483648E9 = 2.147483648E9\r\nException in thread "main" java.lang.AssertionError: Use assertEquals(expected, actual, delta) to compare floating-point numbers\r\n	at org.junit.Assert.fail(Assert.java:92)\r\n	at org.junit.Assert.assertEquals(Assert.java:535)\r\n	at org.junit.Assert.assertEquals(Assert.java:524)\r\n	at calculate.CalculateTest.testSum(CalculateTest.java:15)\r\n	at calculate.CalculateTest.main(CalculateTest.java:18)\r\nC:\\Users\\koodateur\\AppData\\Local\\NetBeans\\Cache\\8.2\\executor-snippets\\run.xml:53: Java returned: 1\r\nBUILD FAILED (total time: 0 seconds)', NULL, NULL, NULL, 0, 8, 0, NULL, 1531964475, 1, NULL, NULL, 2, 1531964475, 1, 1),
(10, 2, 'modification du code', 3, 2, NULL, NULL, 'nous allons modifier le code, au lieu de mettre l''addition, nous allons mettre la soustraction', 'valeur 1+2', '@Test Somme(): -1.0 = 3.0\r\nException in thread "main" java.lang.AssertionError: Use assertEquals(expected, actual, delta) to compare floating-point numbers\r\n	at org.junit.Assert.fail(Assert.java:92)\r\n	at org.junit.Assert.assertEquals(Assert.java:535)\r\n	at org.junit.Assert.assertEquals(Assert.java:524)\r\n	at calculate.CalculateTest.testSum(CalculateTest.java:15)\r\n	at calculate.CalculateTest.main(CalculateTest.java:18)\r\nC:\\Users\\koodateur\\AppData\\Local\\NetBeans\\Cache\\8.2\\executor-snippets\\run.xml:53: Java returned: 1\r\nBUILD FAILED (total time: 0 seconds)', NULL, NULL, NULL, 0, 10, 0, NULL, 1531964687, 1, NULL, NULL, 2, 1531964912, 1, 1),
(14, 5, 'Test avec des champs vide', 1, 2, 3, NULL, 'Nous lançons l''application et nous cliquons sur le bouton de resolution\nLes valeurs des champs restent vide ', 'cliquer sur clearButton\ncliquer sur firstOp\ncliquer sur secondOp\ncliquer sur thirdOp\ncliquer sur submitButton\n', 'Resultat attendu vide \nSuccess', NULL, NULL, NULL, 0, 6, 0, NULL, 1533477967, 1, 3, NULL, 4, 1533513572, 1, 1),
(15, 5, 'Test avec des valeurs entieres positif et negatif en tenant compte de la langue française', 2, 2, 6, NULL, 'Nous lançons l''application et nous cliquons sur le bouton de resolution\nLes valeurs des champs restent vide ', NULL, NULL, '[{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = {-1.0}\\nsuccess"},{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 2\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : Le systeme n''admet pas de solutions dans R\\nsuccess"},{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 5\\nclick sur thirdOp\\necrire 6\\nclick sur submitButton\\n","expected":"Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess"}]', NULL, NULL, 0, 6, 0, NULL, 1533491565, 1, 6, NULL, 4, 1533513579, 1, 2),
(16, 5, 'Test avec des valeurs entieres positif et negatif en tenant compte de la langue Anglaise', 3, 2, 6, NULL, 'Nous lançons l''application et verifions la langue\nLes valeurs des champs restent vide ', NULL, NULL, '[{"content":"clic sur le bouton langue si bouton n''est pas ENG\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\nsuccess"},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 2\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : There is no solutions in R\\nsuccess"},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 5\\nclick sur thirdOp\\necrire 6\\nclick sur submitButton\\n","expected":"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess"}]', NULL, NULL, 0, 6, 0, NULL, 1533497610, 1, 6, NULL, 4, 1533513587, 1, 2),
(17, 5, 'Test avec des carateres n''importe quel langue', 4, 2, 3, NULL, 'Nous lançons l''application et saisissons des caracteres au niveau de champs', 'cliquer sur clearButton\ncliquer sur firstOp\nEcrire a\ncliquer sur secondOp\necrire b\ncliquer sur thirdOp\necrire c\ncliquer sur submitButton\n', 'Resultat attendu vide \nSuccess', NULL, NULL, NULL, 0, 7, 0, NULL, 1533500955, 1, 3, NULL, 4, 1533513591, 1, 1),
(18, 5, 'Test avec des decimaux n''importe quel langue', 5, 2, 2, NULL, 'Nous lançons l''application et saisissons des nombres a virgule au niveau de champs', 'cliquer sur clearButton\ncliquer sur firstOp\nEcrire 0.5\ncliquer sur secondOp\necrire 1\ncliquer sur thirdOp\necrire 0.5\ncliquer sur submitButton\n', 'Resultat attendu vide \nSuccess', NULL, NULL, NULL, 0, 7, 0, NULL, 1533500977, 1, 2, NULL, 4, 1533513594, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `case_assocs`
--

CREATE TABLE IF NOT EXISTS `case_assocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_case_assocs_case_id` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `case_changes`
--

CREATE TABLE IF NOT EXISTS `case_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `case_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `changes` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `ix_case_changes_case_id` (`case_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=51 ;

--
-- Contenu de la table `case_changes`
--

INSERT INTO `case_changes` (`id`, `case_id`, `type_id`, `created_on`, `user_id`, `changes`) VALUES
(1, 1, 6, 1531857718, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":"il faut avoir de grosse valeur pour tester","new_value":"se connecter sur http:\\/\\/localhost\\/somme\\/\\r\\net faire 7777777777777777777 + 2222222222222222223"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 98,32... OK\\r\\n3.type on id=a with value 1000... OK\\r\\n4.clickAt on id=b with value 87,31... OK\\r\\n5.type on id=b with value 2000... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 40,38... OK\\r\\n7.clickAt on id=resultat with value 231,36... OK"}]'),
(2, 1, 6, 1531859030, 1, '[{"type_id":1,"old_text":"Other","new_text":"Accessibility","field":"type_id","old_value":7,"new_value":2},{"type_id":1,"old_text":"Medium","new_text":"Low","field":"priority_id","old_value":2,"new_value":1},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":"se connecter sur http:\\/\\/localhost\\/somme\\/\\r\\net faire 7777777777777777777 + 2222222222222222223","new_value":"se connecter sur http:\\/\\/localhost\\/somme\\/\\r\\net faire 1+1"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":"1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 98,32... OK\\r\\n3.type on id=a with value 1000... OK\\r\\n4.clickAt on id=b with value 87,31... OK\\r\\n5.type on id=b with value 2000... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 40,38... OK\\r\\n7.clickAt on id=resultat with value 231,36... OK","new_value":"valeur 1 = 1\\r\\nvaleur 2 = 1\\r\\nr\\u00e9sultat = 2\\r\\n"}]'),
(3, 1, 6, 1531859074, 1, '[{"type_id":1,"field":"title","old_value":"grosse somme","new_value":"somme simple"}]'),
(4, 2, 6, 1531918044, 1, '[{"type_id":1,"old_text":"Medium","new_text":"High","field":"priority_id","old_value":2,"new_value":3},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"se connecter sur localhost\\/somme\\/\\r\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"valeur 1 = 12,5\\r\\nvaleur 2 = 15,103"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Running ''nombre nombre d\\u00e9cimal''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 64,23... OK\\r\\n3.type on id=a with value 12.5... OK\\r\\n4.clickAt on id=b with value 51,30... OK\\r\\n5.type on id=b with value 15.103... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 62,20... OK\\r\\n7.clickAt on id=resultat with value 320,23... OK\\r\\n8.type on id=a... OK"}]'),
(5, 1, 6, 1531919640, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":"valeur 1 = 1\\r\\nvaleur 2 = 1\\r\\nr\\u00e9sultat = 2\\r\\n","new_value":"valeur 1 = 1\\r\\nvaleur 2 = 2\\r\\n\\r\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Running ''somme nombre entier''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 80,36... OK\\r\\n3.type on id=a with value 1... OK\\r\\n4.type on id=b with value 2... OK\\r\\n5.clickAt on css=button.btn.btn-secondary with value -373,-301... OK\\r\\n6.assertValue on id=resultat with value 3... OK\\r\\n7.doubleClickAt on id=a with value 64,37... OK\\r\\n8.type on id=a with value 3... OK\\r\\n9.type on id=b with value 10... OK\\r\\n10.clickAt on css=button.btn.btn-secondary with value 53,37... OK\\r\\n11.assertValue on id=resultat with value 13... OK\\r\\n12.mouseDownAt on id=a with value 4,26... OK\\r\\n13.mouseMoveAt on id=a with value 4,26... OK\\r\\n14.mouseUpAt on id=a with value 4,26... OK\\r\\n15.clickAt on id=a with value 4,26... OK\\r\\n16.type on id=a with value 32... OK\\r\\n17.type on id=b with value 4000... OK\\r\\n18.clickAt on css=button.btn.btn-secondary with value 38,23... OK\\r\\n19.assertValue on id=resultat with value 4032... OK\\r\\n''somme nombre entier'' completed successfully"}]'),
(6, 3, 6, 1531920075, 1, '[{"type_id":1,"old_text":"Medium","new_text":"Critical","field":"priority_id","old_value":2,"new_value":4},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"modifier le code source du fichier index.html\\r\\nligne 102 :var somme = parseInt($(''#a'').val())+parseInt($(''#b'').val());\\r\\nmodification en \\r\\nligne 102 :var somme = parseInt($(''#a'').val())+parseInt($(''#b'').val());\\r\\nse connecter sur localhost\\/somme\\/\\r\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"clic sur val1 et mettre 1\\r\\nclic sur valeur2 et mettre 2\\r\\nclic sur bouton calculer"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Running ''somme nombre entier''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 80,36... OK\\r\\n3.type on id=a with value 1... OK\\r\\n4.type on id=b with value 2... OK\\r\\n5.clickAt on css=button.btn.btn-secondary with value -373,-301... OK\\r\\n6.assertValue on id=resultat with value 3... Failed:\\r\\nActual value ''-1'' did not match ''3''"}]'),
(7, 4, 6, 1531920349, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"se connecter sur localhost\\/somme"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"clic sur val1 et taper a\\r\\nclic sur valeur2 et taper b"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Running ''somme de lettre''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 60,25... OK\\r\\n3.clickAt on id=b with value 45,23... OK\\r\\n4.clickAt on css=button.btn.btn-secondary with value 26,34... OK\\r\\n''somme de lettre'' completed successfully"}]'),
(8, 5, 6, 1531921871, 1, '[{"type_id":1,"old_text":"Other","new_text":"Accessibility","field":"type_id","old_value":7,"new_value":2},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"se connecter sur localhost\\/somme avec un smartphone ensuite une tablette et enfin un ordinateur"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"pour une tablette et smartphone, faire pivoter l''\\u00e9cran"}]'),
(9, 2, 6, 1531922677, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"Running ''nombre nombre d\\u00e9cimal''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 64,23... OK\\r\\n3.type on id=a with value 12.5... OK\\r\\n4.clickAt on id=b with value 51,30... OK\\r\\n5.type on id=b with value 15.103... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 62,20... OK\\r\\n7.clickAt on id=resultat with value 320,23... OK\\r\\n8.type on id=a... OK","new_value":"Running ''test nombre d\\u00e9cimal''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 95,24... OK\\r\\n3.type on id=a with value 12.5... OK\\r\\n4.clickAt on id=b with value 72,32... OK\\r\\n5.type on id=b with value 14.6... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 49,32... OK\\r\\n7.clickAt on id=a with value 55,40... OK\\r\\n8.type on id=a with value 12.7... OK\\r\\n9.clickAt on id=b with value 35,25... OK\\r\\n10.type on id=b with value 31.04... OK\\r\\n11.clickAt on css=button.btn.btn-secondary with value 41,26... OK\\r\\n12.assertValue on id=resultat with value 43.74... Failed:\\r\\nActual value ''11'' did not match ''43.74''\\r\\n''test nombre d\\u00e9cimal'' was aborted"}]'),
(10, 5, 6, 1531922814, 1, '[{"type_id":1,"field":"title","old_value":"test de responsibilit\\u00e9","new_value":"test de responsivit\\u00e9"}]'),
(11, 5, 6, 1531922858, 1, '[{"type_id":1,"field":"title","old_value":"test de responsivit\\u00e9","new_value":"test de responsive"}]'),
(12, 6, 6, 1531923309, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"se connecter sur localhost\\/somme\\/\\r\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"cliquer sur val1 et entrer 20\\r\\ncliquer sur valeur2 et entrer -12\\r\\ncliquer sur calculer\\r\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Running ''nombre n\\u00e9gatif''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 25,19... OK\\r\\n3.type on id=a with value -12... OK\\r\\n4.clickAt on id=b with value 52,31... OK\\r\\n5.type on id=b with value 20... OK\\r\\n6.clickAt on css=button.btn.btn-secondary with value 47,29... OK\\r\\n7.assertValue on id=resultat with value 8... Failed:\\r\\nActual value ''32'' did not match ''8''\\r\\n''nombre n\\u00e9gatif'' was aborted"}]'),
(13, 7, 6, 1531923982, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"se connecter sur localhost\\/somme\\/"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"dans val1 on met -12,,5\\r\\ndans valeur2 on met 14"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"''signes sp\\u00e9ciaux'' completed successfully\\r\\nRunning ''signes sp\\u00e9ciaux''\\r\\n1.open on \\/somme\\/... OK\\r\\n2.clickAt on id=a with value 55,37... OK\\r\\n3.clickAt on id=b with value 24,30... OK\\r\\n4.type on id=b with value 12.3... OK\\r\\n5.clickAt on css=button.btn.btn-secondary with value 30,31... OK\\r\\n6.assertAlert on id=resultat with value 0... Failed:\\r\\nNo response!!!!\\r\\n''signes sp\\u00e9ciaux'' was aborted"}]'),
(14, 10, 6, 1531964912, 1, '[{"type_id":1,"old_text":"Other","new_text":"Security","field":"type_id","old_value":7,"new_value":10},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"nous allons modifier le code, au lieu de mettre l''addition, nous allons mettre la soustraction"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"valeur 1+2"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"@Test Somme(): -1.0 = 3.0\\r\\nException in thread \\"main\\" java.lang.AssertionError: Use assertEquals(expected, actual, delta) to compare floating-point numbers\\r\\n\\tat org.junit.Assert.fail(Assert.java:92)\\r\\n\\tat org.junit.Assert.assertEquals(Assert.java:535)\\r\\n\\tat org.junit.Assert.assertEquals(Assert.java:524)\\r\\n\\tat calculate.CalculateTest.testSum(CalculateTest.java:15)\\r\\n\\tat calculate.CalculateTest.main(CalculateTest.java:18)\\r\\nC:\\\\Users\\\\koodateur\\\\AppData\\\\Local\\\\NetBeans\\\\Cache\\\\8.2\\\\executor-snippets\\\\run.xml:53: Java returned: 1\\r\\nBUILD FAILED (total time: 0 seconds)"}]'),
(17, 14, 6, 1533480295, 1, '[{"type_id":1,"old_text":"Test Case (Text)","new_text":"Test Case (Steps)","field":"template_id","old_value":1,"new_value":2}]'),
(18, 14, 6, 1533481520, 1, '[{"type_id":1,"old_text":"Security","new_text":"Functional","field":"type_id","old_value":10,"new_value":6},{"type_id":5,"field":"estimate","old_value":null,"new_value":1},{"type_id":1,"old_text":"Test Case (Steps)","new_text":"Test Case (Text)","field":"template_id","old_value":2,"new_value":1},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"click 1\\r\\nclick2"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"error"}]'),
(19, 14, 6, 1533484089, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":"click 1\\r\\nclick2","new_value":"#clearButton\\n#firstOp\\n#secondOp\\n#thirdOp\\n#submitButton\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"error","new_value":"success"}]'),
(20, 14, 6, 1533484884, 1, '[{"type_id":5,"field":"estimate","old_value":1,"new_value":0},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":"#clearButton\\n#firstOp\\n#secondOp\\n#thirdOp\\n#submitButton\\n","new_value":"cliquer sur clearButton\\ncliquer sur firstOp\\ncliquer sur secondOp\\ncliquer sur thirdOp\\ncliquer sur submitButton\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"success","new_value":"Resultat attendu vide \\nSuccess"}]'),
(21, 14, 6, 1533485251, 1, '[{"type_id":5,"field":"estimate","old_value":0,"new_value":3438}]'),
(22, 14, 6, 1533485353, 1, '[{"type_id":5,"field":"estimate","old_value":3438,"new_value":180}]'),
(23, 14, 6, 1533485399, 1, '[{"type_id":5,"field":"estimate","old_value":180,"new_value":0}]'),
(24, 14, 6, 1533485455, 1, '[{"type_id":5,"field":"estimate","old_value":0,"new_value":3}]'),
(25, 14, 6, 1533485848, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"Resultat attendu vide \\nSuccess","new_value":"Resultat attendu vide\\norg.junit.ComparisonFailure: expected:<[]> but was:<[m]>"}]'),
(26, 14, 6, 1533485948, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"Resultat attendu vide\\norg.junit.ComparisonFailure: expected:<[]> but was:<[m]>","new_value":"Resultat attendu vide\\nexpected:<[]> but was:<[m]>"}]'),
(27, 14, 6, 1533486188, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"Resultat attendu vide\\nexpected:<[]> but was:<[m]>","new_value":"Echec Resultat attendu vide\\nexpected:<[]> but was:<[m]>"}]'),
(28, 14, 6, 1533488028, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":"Echec Resultat attendu vide\\nexpected:<[]> but was:<[m]>","new_value":"Resultat attendu vide \\nSuccess"}]'),
(29, 15, 6, 1533492356, 1, '[{"type_id":5,"field":"estimate","old_value":null,"new_value":11}]'),
(30, 15, 6, 1533492442, 1, '[{"type_id":5,"field":"estimate","old_value":11,"new_value":6}]'),
(31, 15, 6, 1533495478, 1, '[{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":null,"new_value":"[{\\"content\\":\\"un \\",\\"expected\\":\\"donne rien\\"},{\\"content\\":\\"\\",\\"expected\\":\\"\\"}]"}]'),
(32, 15, 6, 1533495932, 1, '[{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"un \\",\\"expected\\":\\"donne rien\\"},{\\"content\\":\\"\\",\\"expected\\":\\"\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]"}]'),
(33, 15, 6, 1533496772, 1, '[{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]"}]'),
(34, 15, 6, 1533496889, 1, '[{"type_id":5,"field":"estimate","old_value":6,"new_value":7},{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]"}]'),
(35, 15, 6, 1533496983, 1, '[{"type_id":5,"field":"estimate","old_value":7,"new_value":6},{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\nclic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = -3.0 , -2.0\\\\nsuccess\\"}]"}]'),
(36, 15, 6, 1533497198, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"Nous lan\\u00e7ons l''application et nous cliquons sur le bouton de resolution\\nLes valeurs des champs restent vide "}]'),
(37, 16, 6, 1533500128, 1, '[{"type_id":5,"field":"estimate","old_value":null,"new_value":8},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"Nous lan\\u00e7ons l''application et verifions la langue\\nLes valeurs des champs restent vide "},{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":null,"new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas ENG\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : There is no solutions in R\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\\\nsuccess\\"}]"}]'),
(38, 14, 6, 1533500836, 1, '[{"type_id":1,"field":"title","old_value":"Paramettre en entr\\u00e9 vide","new_value":"Test avec des champs vide"}]'),
(39, 15, 6, 1533500884, 1, '[{"type_id":1,"field":"title","old_value":"paramettres corrects tout entier fran\\u00e7ais","new_value":"Test avec des valeurs entieres positif et negatif en tenant compte de la langue fran\\u00e7ais"}]'),
(40, 16, 6, 1533500895, 1, '[{"type_id":1,"field":"title","old_value":"paramettres corrects tout entier Anglais","new_value":"Test avec des valeurs entieres positif et negatif en tenant compte de la langue Anglaise"}]'),
(41, 15, 6, 1533500899, 1, '[{"type_id":1,"field":"title","old_value":"Test avec des valeurs entieres positif et negatif en tenant compte de la langue fran\\u00e7ais","new_value":"Test avec des valeurs entieres positif et negatif en tenant compte de la langue fran\\u00e7aise"}]'),
(42, 14, 6, 1533512648, 1, '[{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":"Nous lan\\u00e7ons l''application et nous cliquons sur le bouton de resolution\\r\\nLes valeurs des champs restent vide","new_value":"Nous lan\\u00e7ons l''application et nous cliquons sur le bouton de resolution\\nLes valeurs des champs restent vide "}]'),
(43, 17, 6, 1533512659, 1, '[{"type_id":5,"field":"estimate","old_value":null,"new_value":2},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"Nous lan\\u00e7ons l''application et saisissons des caracteres au niveau de champs"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"cliquer sur clearButton\\ncliquer sur firstOp\\nEcrire a\\ncliquer sur secondOp\\necrire b\\ncliquer sur thirdOp\\necrire c\\ncliquer sur submitButton\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Resultat attendu vide \\nSuccess"}]'),
(44, 18, 6, 1533512663, 1, '[{"type_id":5,"field":"estimate","old_value":null,"new_value":2},{"type_id":6,"old_text":null,"new_text":null,"label":"Preconditions","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_preconds","old_value":null,"new_value":"Nous lan\\u00e7ons l''application et saisissons des nombres a virgule au niveau de champs"},{"type_id":6,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_steps","old_value":null,"new_value":"cliquer sur clearButton\\ncliquer sur firstOp\\nEcrire 0.5\\ncliquer sur secondOp\\necrire 1\\ncliquer sur thirdOp\\necrire 0.5\\ncliquer sur submitButton\\n"},{"type_id":6,"old_text":null,"new_text":null,"label":"Expected Result","options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"field":"custom_expected","old_value":null,"new_value":"Resultat attendu vide \\nSuccess"}]'),
(45, 16, 6, 1533512757, 1, '[{"type_id":5,"field":"estimate","old_value":8,"new_value":7}]'),
(46, 17, 6, 1533512761, 1, '[{"type_id":5,"field":"estimate","old_value":2,"new_value":3}]'),
(47, 18, 6, 1533512918, 1, '[{"type_id":5,"field":"estimate","old_value":2,"new_value":3}]'),
(48, 15, 6, 1533513425, 1, '[{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = -3.0 , -2.0\\\\nsuccess\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = {-1.0}\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Le systeme n''admet pas de solutions dans R\\\\nsuccess\\"},{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas FR\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\\\nsuccess\\"}]"}]'),
(49, 16, 6, 1533513432, 1, '[{"type_id":5,"field":"estimate","old_value":7,"new_value":6},{"type_id":8,"old_text":null,"new_text":null,"label":"Steps","options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"field":"custom_steps_separated","old_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas ENG\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 3\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : There is no solutions in R\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\\\nsuccess\\"}]","new_value":"[{\\"content\\":\\"clic sur le bouton langue si bouton n''est pas ENG\\\\ncliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 2\\\\nclick sur secondOp\\\\necrire 2\\\\nclick sur thirdOp\\\\necrire 1\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : There is no solutions in R\\\\nsuccess\\"},{\\"content\\":\\"cliquer sur clearButton\\\\nclick sur firstOp\\\\necrire 1\\\\nclick sur secondOp\\\\necrire 5\\\\nclick sur thirdOp\\\\necrire 6\\\\nclick sur submitButton\\\\n\\",\\"expected\\":\\"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\\\nsuccess\\"}]"}]'),
(50, 18, 6, 1533513440, 1, '[{"type_id":5,"field":"estimate","old_value":3,"new_value":2}]');

-- --------------------------------------------------------

--
-- Structure de la table `case_types`
--

CREATE TABLE IF NOT EXISTS `case_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Contenu de la table `case_types`
--

INSERT INTO `case_types` (`id`, `name`, `is_default`, `is_deleted`) VALUES
(1, 'Acceptance', 0, 0),
(2, 'Accessibility', 0, 0),
(3, 'Automated', 0, 0),
(4, 'Compatibility', 0, 0),
(5, 'Destructive', 0, 0),
(6, 'Functional', 0, 0),
(7, 'Other', 1, 0),
(8, 'Performance', 0, 0),
(9, 'Regression', 0, 0),
(10, 'Security', 0, 0),
(11, 'Smoke & Sanity', 0, 0),
(12, 'Usability', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `configs`
--

CREATE TABLE IF NOT EXISTS `configs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_configs_group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `config_groups`
--

CREATE TABLE IF NOT EXISTS `config_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_config_groups_project_id` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `defects`
--

CREATE TABLE IF NOT EXISTS `defects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `defect_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `test_change_id` int(11) NOT NULL,
  `case_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_defects_defect_id` (`defect_id`),
  KEY `ix_defects_test_change_id` (`test_change_id`),
  KEY `ix_defects_case_id` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `exports`
--

CREATE TABLE IF NOT EXISTS `exports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_exports_created_on` (`created_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` int(11) NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `type_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `configs` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_multi` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `is_system` tinyint(1) NOT NULL,
  `include_all` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_fields_name` (`entity_id`,`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=15 ;

--
-- Contenu de la table `fields`
--

INSERT INTO `fields` (`id`, `name`, `system_name`, `entity_id`, `label`, `description`, `type_id`, `location_id`, `display_order`, `configs`, `is_multi`, `is_active`, `status_id`, `is_system`, `include_all`) VALUES
(1, 'preconds', 'custom_preconds', 1, 'Preconditions', 'The preconditions of this test case. Reference other test cases with [C#] (e.g. [C17]).', 3, 2, 1, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"id":"4be1344d55d11"}]', 0, 1, 1, 0, 0),
(2, 'steps', 'custom_steps', 1, 'Steps', 'The required steps to execute the test case.', 3, 2, 2, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"id":"4be97c65ea2fd"}]', 0, 1, 1, 0, 0),
(3, 'expected', 'custom_expected', 1, 'Expected Result', 'The expected result after executing the test case.', 3, 2, 3, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"id":"4be1345cafd07"}]', 0, 1, 1, 0, 0),
(4, 'dc330d77', 'estimate', 1, 'Estimate', NULL, 1, 1, 1, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 1, 1, 1, 1),
(5, 'ddfe71c8', 'milestone_id', 1, 'Milestone', NULL, 9, 1, 2, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 0, 1, 1, 1),
(6, 'c4bd4336', 'refs', 1, 'References', NULL, 1, 1, 3, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 1, 1, 1, 1),
(7, 'd4d1e651', 'version', 2, 'Version', NULL, 1, 4, 2, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 1, 1, 1, 1),
(8, 'e7c13ac2', 'elapsed', 2, 'Elapsed', NULL, 1, 4, 3, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 1, 1, 1, 1),
(9, 'a6637b4f', 'defects', 2, 'Defects', NULL, 1, 4, 4, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false},"id":"4be97c65ea2fd"}]', 0, 1, 1, 1, 1),
(10, 'steps_separated', 'custom_steps_separated', 1, 'Steps', NULL, 10, 2, 4, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"format":"markdown","has_expected":true,"rows":"5"},"id":"4be97c65ea2fd"}]', 0, 1, 1, 0, 0),
(11, 'step_results', 'custom_step_results', 2, 'Steps', NULL, 11, 3, 1, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"format":"markdown","has_expected":true,"has_actual":true,"rows":"5"},"id":"4be97c65ea2fd"}]', 0, 1, 1, 0, 0),
(12, 'mission', 'custom_mission', 1, 'Mission', 'A high-level overview of what to test and which areas to cover, usually just 1-2 sentences.', 3, 2, 5, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"id":"4be1345cafd07"}]', 0, 1, 1, 0, 0),
(13, 'goals', 'custom_goals', 1, 'Goals', 'A detailed list of goals to cover as part of the exploratory sessions.', 3, 2, 6, '[{"context":{"is_global":true,"project_ids":null},"options":{"is_required":false,"default_value":"","format":"markdown","rows":"7"},"id":"4be1345cafd07"}]', 0, 1, 1, 0, 0),
(14, 'automation_type', 'custom_automation_type', 1, 'Automation Type', NULL, 6, 1, 5, '[{"context":{"is_global":true,"project_ids":[]},"options":{"is_required":false,"default_value":"0","items":"0, None\\n1, Ranorex"},"id":"7a34a519-f458-40bb-af43-ed63baf874ee"}]', 0, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `field_templates`
--

CREATE TABLE IF NOT EXISTS `field_templates` (
  `field_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL,
  PRIMARY KEY (`field_id`,`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `field_templates`
--

INSERT INTO `field_templates` (`field_id`, `template_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(10, 2),
(11, 2),
(12, 3),
(13, 3);

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `ix_group_users_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE IF NOT EXISTS `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) NOT NULL,
  `is_done` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_jobs_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

--
-- Contenu de la table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `created_on`, `is_locked`, `heartbeat`, `is_done`) VALUES
(1, 'check_for_update', 1533514997, 0, 1533515003, 0),
(2, 'defect_gc', 1533515003, 0, 1533515003, 0),
(3, 'fields', 1533515003, 0, 1533516110, 0),
(4, 'forecasts', 1533515004, 0, 1533515004, 0),
(5, 'migration_160', 1533515004, 0, 1533515005, 1),
(6, 'migration_161', 1533515005, 0, 1533515006, 1),
(7, 'notifications', 1533515006, 0, 1533516110, 0),
(8, 'progress', 1533515006, 0, 1533515007, 0),
(9, 'reference_gc', 1533515007, 0, 1533515007, 0),
(10, 'report_jobs', 1533515008, 0, 1533516111, 0),
(11, 'reports', 1533515008, 0, 0, 0),
(12, 'session_gc', 1533515050, 0, 1533515050, 0),
(13, 'token_gc', 1533515050, 0, 1533515050, 0);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `message_recps`
--

CREATE TABLE IF NOT EXISTS `message_recps` (
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  PRIMARY KEY (`message_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `milestones`
--

CREATE TABLE IF NOT EXISTS `milestones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `due_on` int(11) DEFAULT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `start_on` int(11) DEFAULT NULL,
  `started_on` int(11) DEFAULT NULL,
  `is_started` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_milestones_project_id` (`project_id`),
  KEY `ix_milestones_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `milestones`
--

INSERT INTO `milestones` (`id`, `project_id`, `name`, `due_on`, `completed_on`, `is_completed`, `description`, `start_on`, `started_on`, `is_started`, `parent_id`) VALUES
(1, 1, 'somme', NULL, NULL, 0, NULL, NULL, 1531852674, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `preferences`
--

CREATE TABLE IF NOT EXISTS `preferences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_preferences_name` (`user_id`,`name`),
  KEY `ix_preferences_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=23 ;

--
-- Contenu de la table `preferences`
--

INSERT INTO `preferences` (`id`, `user_id`, `name`, `value`) VALUES
(1, 1, 'goals', '{"1":true,"3":true,"2":true,"4":true,"5":true}'),
(3, 1, 'todos_overview_user_ids', '1'),
(4, 1, 'todos_overview_status_ids', '3,4,5'),
(7, 1, 'tests_qpane', '1'),
(8, 1, 'cases_qpane', '0'),
(10, 1, 'admin_users_show', 'active'),
(13, 1, 'sidebar_width', '250'),
(14, 2, 'tests_qpane', '1'),
(15, 2, 'runs_overview_display', 'large'),
(18, 2, 'cases_qpane', '1'),
(21, 2, 'todos_overview_user_ids', '2'),
(22, 2, 'todos_overview_status_ids', '3,4,5');

-- --------------------------------------------------------

--
-- Structure de la table `priorities`
--

CREATE TABLE IF NOT EXISTS `priorities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priority` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `priorities`
--

INSERT INTO `priorities` (`id`, `priority`, `name`, `short_name`, `is_default`, `is_deleted`) VALUES
(1, 1, 'Low', 'Low', 0, 0),
(2, 2, 'Medium', 'Medium', 1, 0),
(3, 3, 'High', 'High', 0, 0),
(4, 4, 'Critical', 'Critical', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `announcement` longtext COLLATE utf8_unicode_ci,
  `show_announcement` tinyint(1) NOT NULL,
  `defect_id_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_add_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_access` int(11) NOT NULL,
  `default_role_id` int(11) DEFAULT NULL,
  `reference_id_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_add_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_plugin` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_config` longtext COLLATE utf8_unicode_ci,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `defect_template` longtext COLLATE utf8_unicode_ci,
  `suite_mode` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `reference_plugin` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_config` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `projects`
--

INSERT INTO `projects` (`id`, `name`, `announcement`, `show_announcement`, `defect_id_url`, `defect_add_url`, `default_access`, `default_role_id`, `reference_id_url`, `reference_add_url`, `defect_plugin`, `defect_config`, `is_completed`, `completed_on`, `defect_template`, `suite_mode`, `master_id`, `reference_plugin`, `reference_config`) VALUES
(1, 'somme', NULL, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1533477445, NULL, 1, 1, NULL, NULL),
(2, 'Somme java', NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1533477488, NULL, 1, 2, NULL, NULL),
(4, '2ndApp', 'Une application qui permet de résoudre des équations du second degré dans R\r\nElle prend 3 valeurs en paramètre et fait ressortir la solution si elle existe ou informe si la résolution n''est pas possible dans R', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `project_access`
--

CREATE TABLE IF NOT EXISTS `project_access` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_favs`
--

CREATE TABLE IF NOT EXISTS `project_favs` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_groups`
--

CREATE TABLE IF NOT EXISTS `project_groups` (
  `project_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`project_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_history`
--

CREATE TABLE IF NOT EXISTS `project_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `suite_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `run_id` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_project_history_project_order` (`project_id`,`created_on`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Contenu de la table `project_history`
--

INSERT INTO `project_history` (`id`, `project_id`, `action`, `created_on`, `user_id`, `suite_id`, `milestone_id`, `run_id`, `name`, `is_deleted`, `plan_id`) VALUES
(1, 1, 1, 1531852510, 1, NULL, NULL, 1, NULL, 0, NULL),
(2, 1, 1, 1531852674, 1, NULL, 1, NULL, NULL, 0, NULL),
(3, 2, 1, 1531964535, 1, NULL, NULL, 2, NULL, 0, NULL),
(4, 1, 1, 1533229219, 1, NULL, NULL, 3, NULL, 0, NULL),
(5, 4, 1, 1533511749, 1, NULL, NULL, 4, NULL, 0, NULL),
(6, 4, 1, 1533513171, 2, NULL, NULL, 5, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `refs`
--

CREATE TABLE IF NOT EXISTS `refs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `case_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_refs_reference_id` (`reference_id`),
  KEY `ix_refs_case_id` (`case_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `access` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `executed_on` int(11) DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  `dir` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formats` longtext COLLATE utf8_unicode_ci NOT NULL,
  `system_options` longtext COLLATE utf8_unicode_ci,
  `custom_options` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `status_message` longtext COLLATE utf8_unicode_ci,
  `status_trace` longtext COLLATE utf8_unicode_ci,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_reports_project_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Contenu de la table `reports`
--

INSERT INTO `reports` (`id`, `plugin`, `project_id`, `name`, `description`, `access`, `created_by`, `created_on`, `executed_on`, `execution_time`, `dir`, `formats`, `system_options`, `custom_options`, `status`, `status_message`, `status_trace`, `is_locked`, `heartbeat`) VALUES
(1, 'cases_result_coverage', 1, 'Comparison for Cases (Results) 18/07/2018', NULL, 1, 1, 1531921527, 1533515028, 20306, '2018/07/report-1-3bcf5661-f299-4c93-939d-f91f84e6681a', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Comparison for Cases (Results) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"runs_suites_id":1,"runs_filters":null,"runs_include":"1","runs_ids":null,"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":10,"cases_columns":{"cases:id":75,"cases:title":0},"cases_filters":null,"cases_limit":1000,"content_hide_links":false,"cases_include_comparison":true,"cases_include_coverage":true}', 2, NULL, NULL, 0, 1533515028),
(4, 'cases_status_tops', 1, 'Status Tops (Cases) 19/07/2018', NULL, 1, 1, 1531967075, 1533515034, 5160, '2018/07/report-4-1bbda7cc-3a1d-4744-8768-b048b75354b9', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Status Tops (Cases) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"statuses_include":"1","statuses_ids":null,"results_include":"1","runs_suites_include":"1","runs_suites_ids":null,"runs_filters":null,"runs_include":"1","runs_ids":null,"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":10,"cases_columns":{"cases:id":75,"cases:title":0},"cases_limit":100,"content_hide_links":false}', 2, NULL, NULL, 0, 1533515034),
(6, 'projects_summary', 2, 'Project (Summary) 02/08/2018', NULL, 1, 1, 1533229045, 1533515040, 5860, '2018/08/report-6-5771b920-6749-4b94-bb98-2d5613590e46', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Project (Summary) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"milestones_completed_limit":10,"runs_completed_limit":10,"history_daterange":"5","history_daterange_from":null,"history_daterange_to":null,"history_limit":100,"activities_daterange":"5","activities_daterange_from":null,"activities_daterange_to":null,"activities_statuses_include":"1","activities_statuses_ids":null,"activities_limit":100,"content_hide_links":false,"milestones_active_include":true,"milestones_completed_include":false,"runs_active_include":true,"runs_completed_include":false,"activities_include":true,"history_include":true}', 2, NULL, NULL, 0, 1533515040),
(7, 'cases_defect_summary', 4, 'Summary for Cases (Defects) 06/08/2018', NULL, 1, 2, 1533514007, 1533515045, 5001, '2018/08/report-7-ae21e4c4-a1f5-4eba-aebc-b0ad25a692a3', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Summary for Cases (Defects) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"runs_suites_id":4,"runs_filters":null,"runs_include":"1","runs_ids":null,"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":10,"cases_columns":{"cases:id":75,"cases:title":0},"cases_filters":null,"cases_limit":1000,"content_hide_links":false,"cases_include_comparison":true,"cases_include_summary":true}', 2, NULL, NULL, 0, 1533515045),
(8, 'defects_summary', 2, 'Summary (Defects) 06/08/2018', NULL, 1, 1, 1533515008, 1533515049, 4295, '2018/08/report-8-280d9450-f6f0-4876-8ff0-c9497c141f51', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Summary (Defects) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":true,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"defects_include":"1","defects_ids":null,"runs_suites_include":"1","runs_suites_ids":null,"runs_filters":null,"runs_include":"3","runs_ids":[2],"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":25,"tests_columns":{"tests:id":75,"cases:title":0},"tests_filters":null,"tests_limit":1000,"content_hide_links":false}', 2, NULL, NULL, 0, 1533515049),
(10, 'cases_result_coverage', 4, 'Comparison for Cases (Results) 06/08/2018', NULL, 1, 2, 1533515423, 1533515518, 5228, '2018/08/report-10-10f0f977-e108-4219-8957-c1469e5b4673', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Comparison for Cases (Results) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"runs_suites_id":4,"runs_filters":null,"runs_include":"1","runs_ids":null,"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":100,"cases_columns":{"cases:id":75,"cases:title":0},"cases_filters":null,"cases_limit":1000,"content_hide_links":false,"cases_include_comparison":true,"cases_include_coverage":true}', 2, NULL, NULL, 0, 1533515518),
(11, 'runs_summary', 4, 'Runs (Summary) 06/08/2018', NULL, 1, 2, 1533515896, 1533515910, 6615, '2018/08/report-11-301aac58-6d26-483e-9b51-751e03b5dcb7', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Runs (Summary) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"runs_suites_include":"1","runs_suites_ids":null,"runs_filters":null,"runs_include":"3","runs_ids":[5],"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":25,"activities_daterange":"5","activities_daterange_from":null,"activities_daterange_to":null,"activities_statuses_include":"1","activities_statuses_ids":null,"activities_limit":100,"tests_filters":null,"tests_columns":{"tests:id":75,"cases:title":0},"tests_limit":100,"content_hide_links":false,"status_include":true,"activities_include":true,"progress_include":true,"tests_include":true}', 2, NULL, NULL, 0, 1533515910),
(12, 'tests_property_groups', 4, 'Property Distribution (Results) 06/08/2018', NULL, 1, 2, 1533516093, 1533516116, 5059, '2018/08/report-12-d3a1fa16-ed29-4d5e-be0f-db363916d3e4', '["raw_zip","inline","standalone_zip","pdf"]', '{"name":"Property Distribution (Results) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":false,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"tests_groupby":"tests:status_id","runs_suites_include":"1","runs_suites_ids":null,"runs_filters":null,"runs_include":"1","runs_ids":null,"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":10,"tests_columns":{"tests:id":75,"cases:title":0},"tests_filters":null,"tests_limit":25,"content_hide_links":false,"tests_include_summary":true,"tests_include_details":true}', 2, NULL, NULL, 0, 1533516116);

-- --------------------------------------------------------

--
-- Structure de la table `report_jobs`
--

CREATE TABLE IF NOT EXISTS `report_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plugin` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `executed_on` int(11) DEFAULT NULL,
  `system_options` longtext COLLATE utf8_unicode_ci,
  `custom_options` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `ix_report_jobs_project_id` (`project_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `report_jobs`
--

INSERT INTO `report_jobs` (`id`, `plugin`, `project_id`, `created_by`, `created_on`, `executed_on`, `system_options`, `custom_options`) VALUES
(1, 'defects_summary', 2, 1, 1531965591, 1533515008, '{"name":"Summary (Defects) %date%","description":null,"access":"1","schedule_now":true,"schedule_later":true,"schedule_interval":"1","schedule_weekday":1,"schedule_day":1,"schedule_hour":8,"notify_user":false,"notify_link":false,"notify_link_recipients":null,"notify_attachment":false,"notify_attachment_recipients":"person1@example.com\\r\\nperson2@example.com","notify_attachment_html_format":false,"notify_attachment_pdf_format":false}', '{"defects_include":"1","defects_ids":null,"runs_suites_include":"1","runs_suites_ids":null,"runs_filters":null,"runs_include":"3","runs_ids":[2],"runs_sections_include":"1","runs_sections_ids":null,"runs_limit":25,"tests_columns":{"tests:id":75,"cases:title":0},"tests_filters":null,"tests_limit":1000,"content_hide_links":false}');

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` int(11) NOT NULL,
  `is_default` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `is_default`, `display_order`) VALUES
(1, 'Lead', 262143, 1, 18),
(2, 'Designer', 258636, 0, 10),
(3, 'Tester', 258624, 0, 8),
(4, 'Read-only', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `runs`
--

CREATE TABLE IF NOT EXISTS `runs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suite_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `include_all` tinyint(1) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `passed_count` int(11) NOT NULL DEFAULT '0',
  `retest_count` int(11) NOT NULL DEFAULT '0',
  `failed_count` int(11) NOT NULL DEFAULT '0',
  `untested_count` int(11) NOT NULL DEFAULT '0',
  `assignedto_id` int(11) DEFAULT NULL,
  `is_plan` tinyint(1) NOT NULL DEFAULT '0',
  `plan_id` int(11) DEFAULT NULL,
  `entry_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entries` longtext COLLATE utf8_unicode_ci,
  `config` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config_ids` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_index` int(11) DEFAULT NULL,
  `blocked_count` int(11) NOT NULL DEFAULT '0',
  `is_editable` tinyint(1) NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `custom_status1_count` int(11) NOT NULL DEFAULT '0',
  `custom_status2_count` int(11) NOT NULL DEFAULT '0',
  `custom_status3_count` int(11) NOT NULL DEFAULT '0',
  `custom_status4_count` int(11) NOT NULL DEFAULT '0',
  `custom_status5_count` int(11) NOT NULL DEFAULT '0',
  `custom_status6_count` int(11) NOT NULL DEFAULT '0',
  `custom_status7_count` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_runs_project_id` (`project_id`),
  KEY `ix_runs_plan_id` (`plan_id`),
  KEY `ix_runs_milestone_id` (`milestone_id`),
  KEY `ix_runs_suite_id` (`suite_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `runs`
--

INSERT INTO `runs` (`id`, `suite_id`, `milestone_id`, `created_on`, `user_id`, `project_id`, `is_completed`, `completed_on`, `include_all`, `name`, `description`, `passed_count`, `retest_count`, `failed_count`, `untested_count`, `assignedto_id`, `is_plan`, `plan_id`, `entry_id`, `entries`, `config`, `config_ids`, `entry_index`, `blocked_count`, `is_editable`, `content_id`, `custom_status1_count`, `custom_status2_count`, `custom_status3_count`, `custom_status4_count`, `custom_status5_count`, `custom_status6_count`, `custom_status7_count`, `updated_by`, `updated_on`) VALUES
(1, 1, NULL, 1531852510, 1, 1, 0, NULL, 1, 'Test Run 17/07/2018', NULL, 3, 0, 3, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1531852510),
(2, 2, NULL, 1531964535, 1, 2, 0, NULL, 1, 'Test Run 19/07/2018', NULL, 2, 0, 1, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, 1531964535),
(3, 1, 1, 1533229219, 1, 1, 0, NULL, 1, 'Test Run 02/08/2018', NULL, 5, 1, 1, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1533229219),
(4, 4, NULL, 1533511749, 1, 4, 0, NULL, 0, 'Test Run 06/08/2018', NULL, 3, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 1, 1533511749),
(5, 4, NULL, 1533513171, 2, 4, 0, NULL, 0, 'Test Run 06/08/2018', NULL, 2, 0, 0, 0, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 2, 1533513171);

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `suite_id` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `ix_sections_suite_id` (`suite_id`),
  KEY `ix_sections_copyof_id` (`copyof_id`),
  KEY `ix_sections_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Contenu de la table `sections`
--

INSERT INTO `sections` (`id`, `suite_id`, `name`, `display_order`, `is_copy`, `copyof_id`, `parent_id`, `depth`, `description`) VALUES
(1, 1, 'Test Cases', 1, 0, NULL, NULL, 0, NULL),
(2, 2, 'Test Cases', 1, 0, NULL, NULL, 0, NULL),
(5, 4, 'Test Cases', 1, 0, NULL, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) NOT NULL,
  `user_data` longtext COLLATE utf8_unicode_ci,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_sessions_session_id` (`session_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Contenu de la table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`, `id`) VALUES
('811f2192-308c-44b4-875b-6eb0e76dea04', '', '', 1533235948, '{"last_login":1533228487,"user_id":1,"rememberme":true,"csrf":"CWZra..ETJEKp6esBr9h"}', 8),
('82064465-d7ed-49e5-a250-a0eefb7d0ed8', '', '', 1533481756, '{"rememberme":true,"user_id":1,"csrf":"MuArzZC8YrfkSXcpnpaJ"}', 9),
('9a870a41-2347-4375-a265-fc1492adf5f5', '', '', 1533476672, NULL, 10),
('654a0ea1-17fd-406a-97d4-cc6cea9bdcb1', '', '', 1533476672, NULL, 11),
('ef18b61b-fe55-47a9-9894-56c5bb58d58f', '', '', 1533488035, '{"rememberme":true,"user_id":1,"csrf":"2bncfHDSH69RCWqiBaaY"}', 12),
('93c5fe6f-ef6d-4a25-8b54-843cebf9f9dd', '', '', 1533497610, '{"rememberme":true,"user_id":1,"csrf":"hCd1rBSDFfDEgtbHTRRp"}', 13),
('e3a8d27e-84d3-4f9a-8a8f-10959180f502', '', '', 1533500059, '{"rememberme":true,"user_id":1,"csrf":"WB\\/vNcBWVlmgy9t.Zeav"}', 14),
('bef332f8-0d12-4d74-adbe-55ab5f37a841', '', '', 1533516552, '{"last_login":1533513114,"user_id":2,"rememberme":true,"csrf":"P6q9aLrPAeATjndSeWN8"}', 16);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_settings_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=46 ;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'session_policy', ''),
(2, 'session_absolute_policy', ''),
(3, 'session_remember_me_disabled', '0'),
(4, 'database_version', '188'),
(5, 'installation_name', 'TestRail QA'),
(6, 'installation_url', 'http://localhost/testrail-dao/'),
(7, 'attachment_dir', 'C:\\wamp\\www\\testrail-dao\\Attachment'),
(8, 'report_dir', 'C:\\wamp\\www\\testrail-dao\\Report'),
(9, 'default_language', 'en'),
(10, 'default_locale', 'fr-fr'),
(11, 'default_timezone', NULL),
(12, 'email_server', 'mail.google.com'),
(13, 'email_ssl', '1'),
(14, 'email_from', 'daohamadou@gmail.com'),
(15, 'email_user', 'daohamadou@gmail.com'),
(16, 'email_pass', 'Malade78848370'),
(17, 'email_notifications', '1'),
(18, 'license_key', 'pjUDdM7ZSTlz8jqPdKstT4Kn3A4Ored3e7zPlvrDwiRx85l9DGnV0CrZaspt\r\nWsLtepa2FZftEfRzMdXHeZTYfkGVKLsZtU2/XlTsq8ZW/Hk8rBYzbhUunvK4\r\nr8keDiL9eAJyYjGbIZx1FZULOqDNL/kyudhL72VYZJTB30C1V65KEjiNenea\r\nydIc8r5fPwfvdgQDljj4aPtO9aMZhkth1ipbrV+SrCKdMrVH2d/YuDtCvaHu\r\nhQbJa6KxPjiTiR0l'),
(31, 'login_text', NULL),
(32, 'password_policy', NULL),
(33, 'password_policy_custom', '.{15,}\r\n[a-z]\r\n[A-Z]\r\n[0-9]\r\n[`~!@#$%^&*()\\-_=+[\\]|;:''",<>./?]'),
(34, 'password_policy_desc', 'Minimum of 15 characters, at least one lower & upper case character, a number and a special character.'),
(35, 'forgot_password', '1'),
(36, 'invite_users', '1'),
(37, 'ip_check', '0'),
(38, 'ip_policy', '; You can use simple IP addresses:\r\n; 192.168.1.1\r\n; Or entire networks:\r\n; 192.168.1.0/24'),
(39, 'check_for_updates', '1'),
(40, 'edit_mode', '86400'),
(41, 'name_format', '0'),
(42, 'partial_count', '500'),
(43, 'apiv2_enabled', '1'),
(44, 'apiv2_session_enabled', '1'),
(45, 'latest_version', '5.5.1.3746');

-- --------------------------------------------------------

--
-- Structure de la table `statuses`
--

CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `color_dark` int(11) NOT NULL,
  `color_medium` int(11) NOT NULL,
  `color_bright` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `is_system` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_untested` tinyint(1) NOT NULL,
  `is_final` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_statuses_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Contenu de la table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `system_name`, `label`, `color_dark`, `color_medium`, `color_bright`, `display_order`, `is_system`, `is_active`, `is_untested`, `is_final`) VALUES
(1, 'passed', 'passed', 'Passed', 6667107, 9820525, 12709313, 1, 1, 1, 0, 1),
(2, 'blocked', 'blocked', 'Blocked', 9474192, 13684944, 14737632, 2, 1, 1, 0, 1),
(3, 'untested', 'untested', 'Untested', 11579568, 15395562, 15790320, 3, 1, 1, 1, 0),
(4, 'retest', 'retest', 'Retest', 13026868, 15593088, 16448182, 4, 1, 1, 0, 0),
(5, 'failed', 'failed', 'Failed', 14250867, 15829135, 16631751, 5, 1, 1, 0, 1),
(6, 'custom_status1', 'custom_status1', 'Unnamed 1', 0, 10526880, 13684944, 6, 0, 0, 0, 0),
(7, 'custom_status2', 'custom_status2', 'Unnamed 2', 0, 10526880, 13684944, 7, 0, 0, 0, 0),
(8, 'custom_status3', 'custom_status3', 'Unnamed 3', 0, 10526880, 13684944, 8, 0, 0, 0, 0),
(9, 'custom_status4', 'custom_status4', 'Unnamed 4', 0, 10526880, 13684944, 9, 0, 0, 0, 0),
(10, 'custom_status5', 'custom_status5', 'Unnamed 5', 0, 10526880, 13684944, 10, 0, 0, 0, 0),
(11, 'custom_status6', 'custom_status6', 'Unnamed 6', 0, 10526880, 13684944, 11, 0, 0, 0, 0),
(12, 'custom_status7', 'custom_status7', 'Unnamed 7', 0, 10526880, 13684944, 12, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `subscriptions`
--

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `is_subscribed` tinyint(1) NOT NULL,
  `test_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_subscriptions_run_test` (`run_id`,`test_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `suites`
--

CREATE TABLE IF NOT EXISTS `suites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `is_master` tinyint(1) NOT NULL,
  `is_baseline` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_suites_project_id` (`project_id`),
  KEY `ix_suites_copyof_id` (`copyof_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `suites`
--

INSERT INTO `suites` (`id`, `name`, `project_id`, `description`, `created_on`, `created_by`, `is_copy`, `copyof_id`, `is_master`, `is_baseline`, `parent_id`, `is_completed`, `completed_on`) VALUES
(1, 'Master', 1, NULL, 1531852467, 1, 0, NULL, 1, 0, NULL, 0, NULL),
(2, 'Master', 2, NULL, 1531962558, 1, 0, NULL, 1, 0, NULL, 0, NULL),
(4, 'Master', 4, NULL, 1533477658, 1, 0, NULL, 1, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `is_locked`, `heartbeat`) VALUES
(1, 0, 1533516116);

-- --------------------------------------------------------

--
-- Structure de la table `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `include_all` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `templates`
--

INSERT INTO `templates` (`id`, `name`, `is_default`, `is_deleted`, `include_all`) VALUES
(1, 'Test Case (Text)', 1, 0, 1),
(2, 'Test Case (Steps)', 0, 0, 1),
(3, 'Exploratory Session', 0, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `template_projects`
--

CREATE TABLE IF NOT EXISTS `template_projects` (
  `template_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`template_id`,`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `run_id` int(11) NOT NULL,
  `case_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `assignedto_id` int(11) DEFAULT NULL,
  `is_selected` tinyint(1) NOT NULL,
  `last_status_change_id` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `in_progress` int(11) NOT NULL,
  `in_progress_by` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `tested_by` int(11) DEFAULT NULL,
  `tested_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_tests_run_id` (`run_id`),
  KEY `ix_tests_case_id` (`case_id`,`is_selected`),
  KEY `ix_tests_content_id` (`content_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=25 ;

--
-- Contenu de la table `tests`
--

INSERT INTO `tests` (`id`, `run_id`, `case_id`, `status_id`, `assignedto_id`, `is_selected`, `last_status_change_id`, `is_completed`, `in_progress`, `in_progress_by`, `content_id`, `tested_by`, `tested_on`) VALUES
(1, 1, 1, 1, NULL, 1, 12, 0, 0, NULL, 1, 1, 1531984289),
(2, 1, 2, 5, NULL, 1, 6, 0, 0, NULL, 2, 1, 1531922776),
(3, 1, 3, 1, NULL, 1, 3, 0, 0, NULL, 3, 1, 1531920173),
(4, 1, 4, 1, NULL, 1, 4, 0, 0, NULL, 4, 1, 1531920588),
(5, 1, 5, 5, NULL, 1, 5, 0, 0, NULL, 5, 1, 1531922214),
(6, 1, 6, 5, NULL, 1, 7, 0, 0, NULL, 6, 1, 1531923361),
(7, 1, 7, 2, NULL, 1, 8, 0, 0, NULL, 7, 1, 1531924096),
(8, 2, 8, 1, NULL, 1, 9, 0, 0, NULL, 8, 1, 1531964581),
(9, 2, 9, 5, NULL, 1, 10, 0, 0, NULL, 9, 1, 1531964630),
(11, 2, 10, 1, NULL, 1, 11, 0, 0, NULL, 10, 1, 1531964944),
(12, 3, 1, 1, 1, 1, 13, 0, 0, NULL, 1, 1, 1533229240),
(13, 3, 2, 4, 1, 1, 14, 0, 0, NULL, 2, 1, 1533229252),
(14, 3, 3, 5, 1, 1, 15, 0, 0, NULL, 3, 1, 1533229260),
(15, 3, 4, 1, 1, 1, 16, 0, 0, NULL, 4, 1, 1533229265),
(16, 3, 5, 1, 1, 1, 17, 0, 0, NULL, 5, 1, 1533229270),
(17, 3, 6, 1, 1, 1, 18, 0, 0, NULL, 6, 1, 1533229275),
(18, 3, 7, 1, 1, 1, 26, 0, 0, NULL, 7, 1, 1533229330),
(19, 4, 14, 1, 1, 1, 31, 0, 0, NULL, 14, 1, 1533512975),
(20, 4, 15, 1, 1, 1, 32, 0, 0, NULL, 15, 1, 1533512993),
(21, 4, 18, 1, 1, 1, 34, 0, 0, NULL, 18, 1, 1533513062),
(22, 5, 16, 1, 2, 1, 39, 0, 0, NULL, 16, 2, 1533515872),
(23, 5, 17, 1, 2, 1, 37, 0, 0, NULL, 17, 2, 1533513863);

-- --------------------------------------------------------

--
-- Structure de la table `test_activities`
--

CREATE TABLE IF NOT EXISTS `test_activities` (
  `date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `passed_count` int(11) NOT NULL,
  `retest_count` int(11) NOT NULL,
  `failed_count` int(11) NOT NULL,
  `untested_count` int(11) NOT NULL,
  `blocked_count` int(11) NOT NULL,
  `custom_status1_count` int(11) NOT NULL,
  `custom_status2_count` int(11) NOT NULL,
  `custom_status3_count` int(11) NOT NULL,
  `custom_status4_count` int(11) NOT NULL,
  `custom_status5_count` int(11) NOT NULL,
  `custom_status6_count` int(11) NOT NULL,
  `custom_status7_count` int(11) NOT NULL,
  PRIMARY KEY (`date`,`project_id`,`run_id`),
  KEY `ix_test_activities_run_id` (`run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `test_activities`
--

INSERT INTO `test_activities` (`date`, `project_id`, `run_id`, `passed_count`, `retest_count`, `failed_count`, `untested_count`, `blocked_count`, `custom_status1_count`, `custom_status2_count`, `custom_status3_count`, `custom_status4_count`, `custom_status5_count`, `custom_status6_count`, `custom_status7_count`) VALUES
(20180717, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180718, 1, 1, 2, 0, 3, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(20180719, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180719, 2, 2, 2, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180802, 1, 3, 12, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180806, 4, 4, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180806, 4, 5, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `test_assocs`
--

CREATE TABLE IF NOT EXISTS `test_assocs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_change_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_test_assocs_test_change_id` (`test_change_id`),
  KEY `ix_test_assocs_test_id` (`test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `test_changes`
--

CREATE TABLE IF NOT EXISTS `test_changes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `version` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elapsed` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defects` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `assignedto_id` int(11) DEFAULT NULL,
  `unassigned` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `is_selected` tinyint(1) NOT NULL,
  `caching` int(11) NOT NULL,
  `custom_step_results` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `ix_test_changes_test_id` (`test_id`),
  KEY `ix_test_changes_project_order` (`project_id`,`is_selected`,`created_on`),
  KEY `ix_test_changes_run_order` (`run_id`,`is_selected`,`created_on`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=40 ;

--
-- Contenu de la table `test_changes`
--

INSERT INTO `test_changes` (`id`, `test_id`, `user_id`, `status_id`, `comment`, `version`, `elapsed`, `defects`, `created_on`, `assignedto_id`, `unassigned`, `project_id`, `run_id`, `is_selected`, `caching`, `custom_step_results`) VALUES
(1, 1, 1, 1, NULL, NULL, NULL, NULL, 1531857758, NULL, 0, 1, 1, 1, 1, NULL),
(2, 1, 1, 1, NULL, NULL, NULL, NULL, 1531859060, NULL, 0, 1, 1, 1, 1, NULL),
(3, 3, 1, 1, NULL, NULL, NULL, NULL, 1531920173, NULL, 0, 1, 1, 1, 1, NULL),
(4, 4, 1, 1, 'ne retourne pas un feed-back mais gère l''erreur', NULL, NULL, NULL, 1531920588, NULL, 0, 1, 1, 1, 1, NULL),
(5, 5, 1, 5, NULL, NULL, NULL, NULL, 1531922214, NULL, 0, 1, 1, 1, 1, NULL),
(6, 2, 1, 5, NULL, NULL, NULL, NULL, 1531922776, NULL, 0, 1, 1, 1, 1, NULL),
(7, 6, 1, 5, NULL, NULL, NULL, NULL, 1531923361, NULL, 0, 1, 1, 1, 1, NULL),
(8, 7, 1, 2, NULL, NULL, NULL, NULL, 1531924096, NULL, 0, 1, 1, 1, 1, NULL),
(9, 8, 1, 1, NULL, NULL, NULL, NULL, 1531964581, NULL, 0, 2, 2, 1, 1, NULL),
(10, 9, 1, 5, NULL, NULL, NULL, NULL, 1531964630, NULL, 0, 2, 2, 1, 1, NULL),
(11, 11, 1, 1, NULL, NULL, NULL, NULL, 1531964944, NULL, 0, 2, 2, 1, 1, NULL),
(12, 1, 1, 1, NULL, NULL, '4', NULL, 1531984289, NULL, 0, 1, 1, 1, 1, NULL),
(13, 12, 1, 1, NULL, NULL, NULL, NULL, 1533229240, NULL, 0, 1, 3, 1, 1, NULL),
(14, 13, 1, 4, NULL, NULL, NULL, NULL, 1533229252, NULL, 0, 1, 3, 1, 1, NULL),
(15, 14, 1, 5, NULL, NULL, NULL, NULL, 1533229260, NULL, 0, 1, 3, 1, 1, NULL),
(16, 15, 1, 1, NULL, NULL, NULL, NULL, 1533229265, NULL, 0, 1, 3, 1, 1, NULL),
(17, 16, 1, 1, NULL, NULL, NULL, NULL, 1533229270, NULL, 0, 1, 3, 1, 1, NULL),
(18, 17, 1, 1, NULL, NULL, NULL, NULL, 1533229275, NULL, 0, 1, 3, 1, 1, NULL),
(19, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229279, NULL, 0, 1, 3, 1, 1, NULL),
(20, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229285, NULL, 0, 1, 3, 1, 1, NULL),
(21, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229289, NULL, 0, 1, 3, 1, 1, NULL),
(22, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229292, NULL, 0, 1, 3, 1, 1, NULL),
(23, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229296, NULL, 0, 1, 3, 1, 1, NULL),
(24, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229303, NULL, 0, 1, 3, 1, 1, NULL),
(25, 18, 1, 1, NULL, NULL, NULL, NULL, 1533229313, NULL, 0, 1, 3, 1, 1, NULL),
(26, 18, 1, 1, NULL, NULL, '4', NULL, 1533229330, NULL, 0, 1, 3, 1, 1, NULL),
(27, 19, 1, 1, NULL, NULL, NULL, NULL, 1533512284, 1, 0, 4, 4, 1, 1, NULL),
(28, 19, 1, 1, NULL, NULL, NULL, NULL, 1533512322, NULL, 0, 4, 4, 1, 1, NULL),
(29, 20, 1, 1, NULL, NULL, NULL, NULL, 1533512574, NULL, 0, 4, 4, 1, 1, '[{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 3\\nclick sur submitButton\\n","expected":"Resultat attendu : Le systeme admet une solution double x0 :-1.0 S = -1.0\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/11)","status_id":1},{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 2\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : Le systeme n''admet pas de solutions dans R\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/12)","status_id":1},{"content":"clic sur le bouton langue si bouton n''est pas FR\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 5\\nclick sur thirdOp\\necrire 6\\nclick sur submitButton\\n","expected":"Resultat attendu : Les solutions sont x1: -3.0 x2: -2.0 S = -3.0 , -2.0\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/13)","status_id":1}]'),
(30, 20, 1, 1, NULL, NULL, NULL, NULL, 1533512590, NULL, 0, 4, 4, 1, 1, NULL),
(31, 19, 1, 1, NULL, NULL, NULL, NULL, 1533512975, NULL, 0, 4, 4, 1, 1, NULL),
(32, 20, 1, 1, NULL, NULL, NULL, NULL, 1533512993, NULL, 0, 4, 4, 1, 1, NULL),
(33, 21, 1, 1, NULL, NULL, NULL, NULL, 1533513041, NULL, 0, 4, 4, 1, 1, NULL),
(34, 21, 1, 1, NULL, NULL, NULL, NULL, 1533513062, NULL, 0, 4, 4, 1, 1, NULL),
(35, 22, 2, 1, NULL, NULL, NULL, NULL, 1533513815, NULL, 0, 4, 5, 1, 1, '[{"content":"clic sur le bouton langue si bouton n''est pas ENG\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 3\\nclick sur submitButton\\n","expected":"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/15)","status_id":1},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 2\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : There is no solutions in R\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/16)","status_id":1},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 5\\nclick sur thirdOp\\necrire 6\\nclick sur submitButton\\n","expected":"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess","actual":"![](index.php?\\/attachments\\/get\\/17)","status_id":1}]'),
(36, 22, 2, 1, NULL, NULL, NULL, NULL, 1533513823, NULL, 0, 4, 5, 1, 1, NULL),
(37, 23, 2, 1, NULL, NULL, NULL, NULL, 1533513863, NULL, 0, 4, 5, 1, 1, NULL),
(38, 22, 2, 1, NULL, NULL, NULL, NULL, 1533515851, NULL, 0, 4, 5, 1, 0, '[{"content":"clic sur le bouton langue si bouton n''est pas ENG\\ncliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : Double solution x0 :-1.0S = {-1.0}\\nsuccess","actual":"","status_id":3},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 2\\nclick sur secondOp\\necrire 2\\nclick sur thirdOp\\necrire 1\\nclick sur submitButton\\n","expected":"Resultat attendu : There is no solutions in R\\nsuccess","actual":"","status_id":3},{"content":"cliquer sur clearButton\\nclick sur firstOp\\necrire 1\\nclick sur secondOp\\necrire 5\\nclick sur thirdOp\\necrire 6\\nclick sur submitButton\\n","expected":"Resultat attendu : Solutions are x1: -3.0 x2: -2.0 S = {-3.0 , -2.0}\\nsuccess","actual":"","status_id":3}]'),
(39, 22, 2, 1, NULL, NULL, NULL, NULL, 1533515872, NULL, 0, 4, 5, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `test_progress`
--

CREATE TABLE IF NOT EXISTS `test_progress` (
  `date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `tests` int(11) NOT NULL,
  `forecasts` int(11) NOT NULL,
  PRIMARY KEY (`date`,`project_id`,`run_id`),
  KEY `ix_test_progress_run_id` (`run_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `test_progress`
--

INSERT INTO `test_progress` (`date`, `project_id`, `run_id`, `tests`, `forecasts`) VALUES
(20180717, 1, 1, -1, 0),
(20180718, 1, 1, -6, 0),
(20180719, 2, 2, -3, 0),
(20180802, 1, 3, -6, 0),
(20180806, 4, 4, -3, -11),
(20180806, 4, 5, -2, -9);

-- --------------------------------------------------------

--
-- Structure de la table `test_timers`
--

CREATE TABLE IF NOT EXISTS `test_timers` (
  `test_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `started_on` int(11) NOT NULL,
  `elapsed` int(11) NOT NULL,
  `is_paused` tinyint(1) NOT NULL,
  PRIMARY KEY (`test_id`,`user_id`),
  KEY `ix_test_timers_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `uiscripts`
--

CREATE TABLE IF NOT EXISTS `uiscripts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `includes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta` longtext COLLATE utf8_unicode_ci,
  `js` longtext COLLATE utf8_unicode_ci,
  `css` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `salt` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `rememberme` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notifications` tinyint(1) NOT NULL,
  `csrf` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `login_token` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_token_until` int(11) DEFAULT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `is_reset_password_forced` tinyint(1) NOT NULL DEFAULT '0',
  `data_processing_agreement` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_users_email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `salt`, `hash`, `is_active`, `rememberme`, `locale`, `language`, `notifications`, `csrf`, `role_id`, `login_token`, `timezone`, `login_token_until`, `last_activity`, `is_reset_password_forced`, `data_processing_agreement`) VALUES
(1, 'Hamadou DAO', 'daohamadou@gmail.com', 1, '', '1:$2y$10$x467rFAyUjoVT/sptintyOGITT64fVr9bO80Q7nFxwcv6w/AtcJ6C', 1, '', NULL, NULL, 1, '', 1, NULL, NULL, NULL, 1533513062, 0, NULL),
(2, 'Bouba Cissé', 'boubacisse1@gmail.com', 0, '', '1:$2y$10$TiCRhlX6DneCIgBjvHezeea1eFKRkKYwt5eAhzEFIAqPnEzeI2nbO', 1, '', NULL, NULL, 1, '', 3, NULL, NULL, NULL, 1533516547, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_columns`
--

CREATE TABLE IF NOT EXISTS `user_columns` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `columns` longtext COLLATE utf8_unicode_ci NOT NULL,
  `group_by` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `group_order` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`,`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_columns`
--

INSERT INTO `user_columns` (`user_id`, `project_id`, `area_id`, `columns`, `group_by`, `group_order`) VALUES
(1, 1, 1, '{"cases:id":65,"cases:title":0,"cases:custom_automation_type":100}', 'cases:section_id', 'asc');

-- --------------------------------------------------------

--
-- Structure de la table `user_exports`
--

CREATE TABLE IF NOT EXISTS `user_exports` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `format` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `options` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`,`area_id`,`format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_exports`
--

INSERT INTO `user_exports` (`user_id`, `project_id`, `area_id`, `format`, `options`) VALUES
(1, 2, 2, 'csv', '{"columns":["tests:id","cases:title","tests:assignedto_id","tests:original_case_id","tests:comment","tests:defects","tests:elapsed","cases:estimate","cases:estimate_forecast","tests:in_progress_by","tests:plan_name","tests:plan_id","cases:priority_id","cases:refs","tests:run_name","tests:run_config","tests:run_id","cases:section_id","cases:section_depth","cases:section_desc","tests:status_id","tests:tested_by","tests:tested_on","cases:type_id","tests:version"],"layout":"tests","separator_hint":true}');

-- --------------------------------------------------------

--
-- Structure de la table `user_fields`
--

CREATE TABLE IF NOT EXISTS `user_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `type_id` int(11) NOT NULL,
  `fallback` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_fields_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `user_filters`
--

CREATE TABLE IF NOT EXISTS `user_filters` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `filters` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`,`area_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_logins`
--

CREATE TABLE IF NOT EXISTS `user_logins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `attempts` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_logins_name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=88 ;

--
-- Contenu de la table `user_logins`
--

INSERT INTO `user_logins` (`id`, `name`, `created_on`, `updated_on`, `attempts`) VALUES
(1, 'daohamadou@gmail.com', 1531852201, 1533513594, 0),
(2, 'testrail', 1533476440, 1533476689, 2),
(41, 'boubacisse@gmail.com', 1533513094, 1533513094, 1),
(42, 'boubacisse1@gmail.com', 1533513114, 1533513114, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user_settings`
--

CREATE TABLE IF NOT EXISTS `user_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_user_settings_name` (`user_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `user_tokens`
--

CREATE TABLE IF NOT EXISTS `user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `expires_on` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_user_tokens_user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Contenu de la table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `type_id`, `name`, `series`, `hash`, `created_on`, `expires_on`) VALUES
(15, 2, 2, NULL, 'q5aEnqFz921gOexRcFX6', '1:$2y$10$tWOpxdAf8/RscbLXO45.He.hpPD.k26yE/wXd/hpq1ymdaNTXxWBO', 1533513114, 1536105114);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
